/*
 * WhoisTableRetriever.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: PSokolo
 */

#include <WebService/Inc/WhoisTableRetriever.h>
#include <iosfwd>
#include <WhoisTable.hpp>
#include <sstream>
#include <cstdlib>

WhoisTableRetriever::~WhoisTableRetriever()
{
    // TODO Auto-generated destructor stub
}

std::string WhoisTableRetriever::GetEntriesJson()
{
    std::string response;
    response += "\n[\n";
    for (int entryIndex = 0; entryIndex < table.GetNumberOfEntries(); ++entryIndex)
    {
        if(table.GetEntry(entryIndex) == NULL)
        {
            continue;
        }
        WhoisEntry& entry = *table.GetEntry(entryIndex);

        std::stringstream singleEntry;
        if(entryIndex > 0)
        {
        	singleEntry << ",";
        }
        singleEntry <<"\n{ "<<"\n"\
        			<<"  	\"id\": \""<<SSTR(entryIndex)<<"\","<<"\n"\
                    <<"     \"deviceName\": \""<<entry.getDeviceName()<<"\","<<"\n"\
                    <<"     \"deviceAddress\": \"" << entry.getDeviceIpContainer().GetAddressAsString() <<"\","<<"\n"\
                    <<"     \"deviceCommunicationPort\": \"" << entry.getPathAsString() << "\"," <<"\n"\
                    <<"     \"status\": \"" << SSTR(1) <<"\","<<"\n"\
                    <<"		\"apps\": [";
        			const char* appTable = entry.getAppList();
        			for(int i = 0; i<entry.getAppListSize();++i)
        			{
        				const uint32_t t = appTable[i] & 0xFF;
        				if(appTable[i] == 0)
        				{
        					continue;
        				}
        				if(i!=0 && i+1<entry.getAppListSize())
        				{
        					singleEntry<<",";
        				}

        				singleEntry << t;
        			}

        singleEntry <<"]\n}";

        response += singleEntry.str();
    }
    response += "]";
    return response;
}
const WhoisEntry* WhoisTableRetriever::GetEntry(uint32_t id)
{
	return table.GetEntry(id);
}
std::vector<WhoisEntry> WhoisTableRetriever::GetEntriesSet()
{
    std::vector<WhoisEntry> returnVector(table.GetNumberOfEntries());
    for (int entryIndex = 0; entryIndex < table.GetNumberOfEntries(); ++entryIndex)
    {
        if(table.GetEntry(entryIndex) == NULL)
        {
            continue;
        }
        returnVector.push_back(*table.GetEntry(entryIndex));
    }
    return returnVector;
}
