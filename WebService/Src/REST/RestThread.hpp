/*
 * RestThread.hpp
 *
 *  Created on: Apr 29, 2015
 *      Author: psokolo
 */

#ifndef RESTTHREAD_HPP_
#define RESTTHREAD_HPP_
#include "Thread.h"

#include <string>
#include <vector>
class Whois;
class WhoisTableRetriever;
namespace Communication
{
class CommunicationBase;
}

class RestThread :public Thread{
public:
	RestThread(Whois& pWhois);
	virtual ~RestThread();
	virtual void run();
private:
	std::vector<std::string> ParseUri(const std::string) const;
	bool Validate(const std::vector<std::string>&);
	WhoisTableRetriever* m_Table;
	Communication::CommunicationBase* pCommport;
};

#endif /* RESTTHREAD_HPP_ */
