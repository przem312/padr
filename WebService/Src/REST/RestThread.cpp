/*
 * RestThread.cpp
 *
 *  Created on: Apr 29, 2015
 *      Author: psokolo
 */

#include "RestThread.hpp"
#include "fcgio.h"
#include "Whois/Whois.hpp"
#include "../../Inc/WhoisTableRetriever.h"
#include <sstream>      // std::stringstream
#include "../../../Stack/Utils/Console/Console.h"
#include <stdio.h>
#include "../../../Stack/Application/AppMngr.h"
#include "../../../Stack/Utils/DeviceStatus.h"
#include "../../../Stack/Comms/CommPortBase.h"

RestThread::RestThread(Whois& pWhois)
:Thread("RestThread",2048,1),
 pCommport(pWhois.getCommport())
{
	m_Table = new WhoisTableRetriever(pWhois.getTable());
	std::cout<<m_Table->GetEntriesJson();
}

RestThread::~RestThread() {
	delete m_Table;
}

void RestThread::run()
{
	  // Backup the stdio streambufs
	    std::streambuf * cin_streambuf  = std::cin.rdbuf();
	    std::streambuf * cout_streambuf = std::cout.rdbuf();
	    std::streambuf * cerr_streambuf = std::cerr.rdbuf();

	    FCGX_Request request;

	    FCGX_Init();
	    FCGX_InitRequest(&request, 0, 0);

	    while (FCGX_Accept_r(&request) == 0) {
	        fcgi_streambuf cin_fcgi_streambuf(request.in);
	        fcgi_streambuf cout_fcgi_streambuf(request.out);
	        fcgi_streambuf cerr_fcgi_streambuf(request.err);

	        std::cin.rdbuf(&cin_fcgi_streambuf);
	        std::cout.rdbuf(&cout_fcgi_streambuf);
	        std::cerr.rdbuf(&cerr_fcgi_streambuf);
	        std::string uri = FCGX_GetParam("REQUEST_URI", request.envp);
	        const std::string requestMethod = FCGX_GetParam("REQUEST_METHOD",request.envp);
	        const std::vector<std::string>& params = ParseUri(uri);


	        if(Validate(params))
	        {

	        	std::stringstream responseStream("");
	        	responseStream<< "Content-type: application/json\r\n\r\n";
	        	if(requestMethod == "SUBSCRIBE")
	        	{
	        		const uint32_t TYPE_INT = 1;
	        		const uint32_t TYPE_STR = 2;
	        		const std::string content = FCGX_GetParam("QUERY_STRING",request.envp);
	        		uint32_t deviceId=0xff;
	        		uint32_t appId=0;
	        		uint32_t attrId=0;
	        		uint32_t timeout=0;
	        		uint32_t attrType=0;
	        		sscanf(content.c_str(),"id=%d&appId=%d&attrId=%d&type=%d&time=%d",&deviceId,&appId,&attrId,&attrType,&timeout);
	        		DEBUG("SUBSCRIBE %s %d + %d + %d + %d", content.c_str() ,deviceId,appId,attrId,timeout);
	        		//TODO

        			char buffer[16];
	        			std::memset(buffer,0,16);
	        		if(deviceId == 0)
	        		{
	        			responseStream<<"[{\"deviceId\":\""<<deviceId<<"\",\"appId\":\""<<appId<<"\",\"response\":\"";
	        			if(DeviceStatus::GEN_OK == AppMngr::GetInstance()->ExecuteRequest(appId,attrId,buffer,16))
	        			{

	        				for(int i = 0; i<sizeof(buffer);++i)
	        				{
	        					if(buffer[i]==0)continue;
	        					if(attrType==TYPE_STR)
	        					{
	        						DEBUG("STR");
	        					responseStream<<buffer[i];
	        					}
	        					else if(attrType == TYPE_INT)
	        					{
	        						DEBUG("INT");
	        						responseStream<<(uint32_t)buffer[i];
	        					}
	        					DEBUG("%d",buffer[i]);
	        				}
	        			}
	        			else
	        			{
	        				responseStream<<"BAD DATA";
	        			}
	        			responseStream<<"\"}]";
	        		}
	        		else
	        		{
	        			buffer[0]=WEBSERVER_APP;
	        			buffer[1]=8;
	        			buffer[2]=appId;
	        			buffer[3]=attrId;
	        			const TargetAddressContainer* container =&m_Table->GetEntry(deviceId)->getDeviceIpContainer();
	        			pCommport->SendCommand(container,GET_CMD,buffer,16);
	        		}
	        	}
	        	else
	        	{
		        const std::string& jSon = m_Table->GetEntriesJson();
		        responseStream<< jSon;
	        	}
	        	DEBUG("RESPONSE: %s ",responseStream.str().c_str());
	        	std::cout<<responseStream.str();
	        }
	        else
	        {
	        	std::cout << "BAD";
	        }
	        // Note: the fcgi_streambuf destructor will auto flush
	    }

	    // restore stdio streambufs
	    std::cin.rdbuf(cin_streambuf);
	    std::cout.rdbuf(cout_streambuf);
	    std::cerr.rdbuf(cerr_streambuf);

}

std::vector<std::string> RestThread::ParseUri(const std::string uri) const
{
	std::stringstream test;
	test << uri;
	std::string segment;
	std::vector<std::string> seglist;

	while(std::getline(test, segment, '/'))
	{
	   seglist.push_back(segment);
	}
	return seglist;
}
bool RestThread::Validate(const std::vector<std::string>& uriTokens)
{
	return uriTokens.size() > 2 &&
			uriTokens.at(1) == "REST"&&
		   uriTokens.at(2) == "STACK";
}
