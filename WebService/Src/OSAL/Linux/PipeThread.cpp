/*
 * PipeThread.cpp
 *
 *  Created on: Mar 26, 2015
 *      Author: psokolo
 */

#include "PipeThread.h"
#include "AssertMacro.h"
#include <stdio.h>
#include <iostream>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
PipeThread::PipeThread(const char* pipeName, const char* sendPipeName,const char* name, PipeType type,WhoisTable& rTable)
:Thread(name,2048,1),
 TableRetriever(rTable),
 readPipe(pipeName),
 sendPipe(sendPipeName)
{

	// TODO Auto-generated constructor stub

}

PipeThread::~PipeThread() {

	// TODO Auto-generated destructor stub
}

void PipeThread::run()
{
	static char buffer[1024];
	FILE* fp;
	while(1)
	{
		fp = fopen(readPipe,"r");
		int bytesRcvd;
		fgets(buffer,1024,fp);
		fclose(fp);
		if(strcmp(buffer,"WHOIS") == 0)
		{
			FILE* writeF = fopen(sendPipe,"w");
			std::string response = TableRetriever.GetEntriesJson();
			DEBUG("Pipe content: \n %s\n\n",response.c_str());
			fputs(response.c_str(),writeF);
			fclose(writeF);
		}
		std::memset(buffer,NULL,1024);
		
	}
}
