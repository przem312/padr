/*
 * NamedPipe.cpp
 *
 *  Created on: Mar 26, 2015
 *      Author: psokolo
 */

#include "NamedPipe.h"
#include "PipeThread.h"
#include "AssertMacro.h"
#include <sys/types.h>  // mkfifo
#include <sys/stat.h>   // mkfifo
#include <fcntl.h>
#include <unistd.h>


NamedPipe::NamedPipe(WhoisTable& whoisTable) {//TODO: mkfifo only if not exists
	int tmp = open("/tmp/PadrRead",O_RDWR);
	if(0> tmp)
	{
		recvFifo = mkfifo("/tmp/PadrRead",S_IRUSR);
		//ASSERT(sendFifo>=0);
	}
		close(tmp);
	tmp = open("/tmp/PadrWrite",O_RDWR);
	if(tmp<0)
	{
		sendFifo = mkfifo("/tmp/PadrWrite",S_IWUSR);
		//ASSERT(sendFifo>=0);
	}
		close(tmp);
//	recvFifo = mkfifo("/tmp/ToPadr",S_IRGRP);
//	ASSERT(recvFifo>=0);
//	m_RcvThread = new PipeThread(recvFifo,"ReceiveFifo",READ);
	m_SendThread = new PipeThread("/tmp/PadrRead","/tmp/PadrWrite","SendFifo",WRITE,whoisTable);
}

NamedPipe::~NamedPipe() {
	//delete m_RcvThread;
	delete m_SendThread;
}
