/*
 * PipeThread.h
 *
 *  Created on: Mar 26, 2015
 *      Author: psokolo
 */

#ifndef PIPETHREAD_H_
#define PIPETHREAD_H_
#include <Thread.h>
#include "../../../Inc/WhoisTableRetriever.h"
enum PipeType
{
	READ,
	WRITE
};
class WhoisTable;
class PipeThread :public Thread
{
public:
	PipeThread(const char*/*pipeName*/, const char*,const char* /*name*/, PipeType type,WhoisTable& rTable);
	virtual ~PipeThread();
	virtual void run();
private:
	void ReceiveThread();
	void SendThread();
	const char* readPipe;
	const char* sendPipe;
	WhoisTableRetriever TableRetriever;
};

#endif /* PIPETHREAD_H_ */
