/*
 * NamedPipe.h
 *
 *  Created on: Mar 26, 2015
 *      Author: psokolo
 */

#ifndef NAMEDPIPE_H_
#define NAMEDPIPE_H_
class PipeThread;
class WhoisTable;
class NamedPipe {
public:

	NamedPipe(WhoisTable& whoisTable);
	virtual ~NamedPipe();
private:
	int sendFifo;
	int recvFifo;
	PipeThread* m_SendThread;
};

#endif /* NAMEDPIPE_H_ */
