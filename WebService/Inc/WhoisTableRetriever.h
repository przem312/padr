/*
 * WhoisTableRetriever.h
 *
 *  Created on: Mar 10, 2015
 *      Author: PSokolo
 */

#ifndef WEBSERVICE_INC_WHOISTABLERETRIEVER_H_
#define WEBSERVICE_INC_WHOISTABLERETRIEVER_H_
#include <iosfwd>
#include <vector>
#include <WhoisEntry.h>
/*
 *
 */
#define SSTR(X) dynamic_cast< std::ostringstream& >(\
		std::ostringstream()<< std::dec << X).str()

class WhoisTable;
class WhoisTableRetriever
{
public:
    WhoisTableRetriever(WhoisTable& whoisTable)
    :table(whoisTable)
    {

    }
    virtual ~WhoisTableRetriever();
    const WhoisEntry* GetEntry(uint32_t id);
    std::string GetEntriesJson();
    std::vector<WhoisEntry> GetEntriesSet();
private:
    WhoisTable& table;
};

#endif /* WEBSERVICE_INC_WHOISTABLERETRIEVER_H_ */
