/*
 * Rest.hpp
 *
 *  Created on: Apr 29, 2015
 *      Author: psokolo
 */

#ifndef REST_HPP_
#define REST_HPP_

class RestThread;
class Whois;
class Rest {
public:
	Rest(Whois&);
	virtual ~Rest();
	void Init();
private:
	Whois* m_pWhois;
	RestThread* m_Thread;
};

#endif /* REST_HPP_ */
