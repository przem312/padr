#include "Stack.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include "Stack/Utils/Console/Console.h"
#include "Stack/Command/CommandDispatcher.h"
#include "Stack/Command/CommandNumbers.h"
#include "Stack/OS/Thread.h"
class Test : public Thread
{
public:
	Test()
	:Thread("Test",0,0){

	}
	virtual void run()
	{
		  char ip[] = {127,0,0,1};
		    char data[18] = {};
		    data[0] = 0;
		    data[1] = 0;
		    data[2] = 255;
		    data[3] = 2;
		    TargetAddressContainer container(ip);
		    DeviceStatus status;
		    status.SetStatus(DeviceStatus::GEN_OK);
		    Thread::SleepMs(1000);
		    DEBUG("START TEST");
		    CommandDispatcher::GetInstance().SendCommand(GET_CMD,status,&container,&data[0],(size_t)18);
		    DEBUG("STATUS GOT: %d" ,(int)status.GetStatus());
		    if(status.GetStatus() == DeviceStatus::COMM_MSG_PENDING)
		    {
		    	DEBUG("GOT MSG!\n");
		    	for(int i = 0; i<32;++i)
		    	{
		    		printf("%d",data[i]);
		    	}
		    }
		    DEBUG("END");
	}
};
int main()
{
    Stack stack;

    stack.Start();

	while (1);
	//delete test;
	return 0;
}

