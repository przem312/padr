#ifndef __DELAY_H__
#define __DELAY_H__
#ifdef __cplusplus
extern "C" {
#endif
//#include "stm32f4xx.h"
#include "Types.h"
extern uint32_t TimingDelay;
extern uint32_t MillisCounter;

void TimingDelay_Decrement(void);

void Delay_Ms(volatile uint32_t nTime);
void Delay_us(volatile uint32_t nTime);

void IncreaseMillis(void);
uint32_t Millis(void);
#ifdef __cplusplus
}
#endif
#endif /* __DELAY_H__ */
