#if !defined( BSPTYPES_H )
#define BSPTYPES_H

// SYSTEM INCLUDES

#include <stdint.h>
#include <stddef.h>

// C PROJECT INCLUDES
// (none)

// C++ PROJECT INCLUDES
// (none)

// FORWARD REFERENCES
// (none)

#if defined( __cplusplus )
extern "C"
{
#endif

typedef uint8_t     UINT8;
typedef uint16_t    UINT16;
typedef uint32_t    UINT32;
typedef uint64_t    UINT64;
typedef int32_t     INT32;
typedef int16_t     INT16;
typedef int8_t      INT8;
typedef int64_t     INT64;
typedef uint8_t     u_char;
typedef uint16_t    u_short;
typedef int         BOOL;

#if defined( __cplusplus )	
}
#endif

#endif // #if !defined( BSPTYPES_H )

/// @}