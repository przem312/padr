/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx.h"
#include "Delay.h"

//__IO uint32_t TimingDelay;
uint32_t MillisCounter = 0;

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */


void Delay_Ms(__IO uint32_t nTime)
{ 
  TimingDelay= nTime;

//  while(TimingDelay != 0);
}

void Delay_us(__IO uint32_t nTime)
{
    nTime = nTime * 50;
    while (nTime > 0)
    {
        nTime--;
    }
}

void IncreaseMillis(void)
{
    MillisCounter++;
}

uint32_t Millis(void)
{
    return MillisCounter;
}
