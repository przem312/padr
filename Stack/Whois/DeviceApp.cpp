/*
 * DeviceApp.cpp
 *
 *  Created on: 25 sie 2014
 *      Author: przem312
 */
#include <stdint.h>
#include <cstring>
#include "DeviceApp.h"
#include "CommPortBase.h"
#include "Thread.h"
#include "WhoisEntry.h"
#include "MessageBase.h"
#include <Whois.hpp>
#include "../../WebService/Inc/Rest.hpp"
DeviceApp::DeviceApp(AppType type, Communication::CommunicationBase* pBase)
:AppBase(type,"DeviceId"),
 pCommport(pBase),
 isReady(false),
 pEntry(NULL),
 pWhois(new Whois(pBase, this)),
 rest(new Rest(*pWhois))

{
	// TODO Auto-generated constructor stub

}

DeviceApp::~DeviceApp() {
	// TODO Auto-generated destructor stub
}

void DeviceApp::FillTableEntry(WhoisEntry* entry)
{
	m_Mutex.Take();
	isReady=false;
	pEntry=entry;
	/*char GetNameRequest[2*sizeof(uint16_t) + sizeof(uint64_t)];
	std::memset(GetNameRequest,0,sizeof(GetNameRequest));
	static const uint16_t DEVICE_NAME = 0 | DEVICE_NAME_ID<<8;
	GetNameRequest[0]=GetNameRequest[2] = (uint8_t)(DEVICE_NAME&0xFF00>>8);
	GetNameRequest[1]=GetNameRequest[3] = (uint8_t)(DEVICE_NAME&0xFF);
	pCommport->SendCommand(&entry->getDeviceIpContainer(),
							GET_CMD,
							GetNameRequest,
							sizeof(GetNameRequest));
	*/
	GetMsg msg;
	msg.SetSenderAttributeId(DEVICE_NAME_ID);
	msg.SetSenderId(DEVICE_APP);
	msg.SetTargetAttributeId(DEVICE_NAME_ID);
	msg.SetTargetId(DEVICE_APP);
	pCommport->SendData(&entry->getDeviceIpContainer(),(char*)&msg,sizeof(msg));
	while(!isReady)
	{
		Thread::SleepMs(100);
	}
	m_Mutex.Release();
}

void DeviceApp::ProcessAppSpecificResponse(AttributeSet attr, char* pBuffer)
{
	switch(attr.m_AttrNo)
	{
	if(pEntry != NULL)
	{
	case DEVICE_NAME_ID:
		pEntry->setDeviceName(pBuffer);
		break;
	case DEVICE_IP_ID:
		pEntry->setDeviceIp(pBuffer);
		break;
	case DEVICE_STATUS_ID:
		pEntry->setStatus(*pBuffer);
		break;
	}
	case DEVICE_REQUEST_ID:
		DEBUG("RESPONSE");
		for (int var = 0; var < attr.sizeOfAttr; ++var) {
			DEBUG("%d",pBuffer[var]);
		}
		break;
	default:
		break;
	}
}

bool DeviceApp::GetAppSpecific(const AttributeSet attributeId,
                                            char* pBuffer)
{
	switch(attributeId.m_AttrNo)
	{
	case DEVICE_NAME_ID:
		std::strcpy(pBuffer,Device::GetDeviceName());
		break;
	case DEVICE_IP_ID:
		std::memcpy(pBuffer,Device::GetDeviceIp(),4);
		break;
	case DEVICE_STATUS_ID:
		*pBuffer=Device::IsMaster()?1:0;
		break;

	default:
		return false;
	}
	return true;
}
