/*
 * WhoisTable.hpp
 *
 *  Created on: Mar 10, 2015
 *      Author: PSokolo
 */

#ifndef STACK_WHOISTABLE_HPP_
#define STACK_WHOISTABLE_HPP_
#include <stdint.h>
#include <WhoisEntry.h>


/*
 *
 */
class WhoisTable
{
public:
    WhoisTable();
    virtual ~WhoisTable();
    bool AddEntry(const WhoisEntry&);
    void RemoveEntry(WhoisEntry&);
    WhoisEntry* GetEntry(uint8_t);
    bool isOnList(const WhoisEntry&);
    uint8_t GetNumberOfEntries() const
    {
        return numberOfEntries;
    }

private:
    struct DataSetStruct
    {
        DataSetStruct()
        :isUsed(false)
        {

        }
        WhoisEntry entry;
        bool isUsed;
    }DataSet[8];
    uint8_t numberOfEntries;
};

#endif /* STACK_WHOISTABLE_HPP_ */
