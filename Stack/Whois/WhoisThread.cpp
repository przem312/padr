/*
 * WhoisThread.cpp
 *
 *  Created on: Mar 19, 2015
 *      Author: PSokolo
 */
#include "MessageBase.h"
#include "CommunicationMsg.h"
#include "WhoisThread.h"
#include "ObjectPool.h"
#include "WhoisTable.hpp"
#include "WhoisMsg.h"
WhoisThread::WhoisThread(WhoisTable& rTable, DynamicObjectPool<WhoisEntry,8>& rEntry)
    :Thread("WhoisThread",0,0),
     table(rTable),entry(rEntry),
     msgToProcess(NULL)
    {

    }

    void WhoisThread::run()
    {
        while(true)
        {

            while(!msgToProcess)
            {
            	SleepMs(1);
            }
            WhoisMsg* pMsg = msgToProcess;
            if(pMsg == NULL)
            {
            	m_SynchBarrier.Release();
            	continue;
            }
            else
            {
				WhoisEntry* newEntry = entry.AcquireObject();
				if(newEntry)
				{
				newEntry->setDeviceIp(pMsg->getIp());
				newEntry->setDeviceName(pMsg->getdeviceName());
				newEntry->setPath((CommunicationPath)pMsg->getcommunicationPath());
				table.AddEntry(*newEntry);

				}
            }
			msgToProcess=NULL;
        	m_SynchBarrier.Release();


        }
    }
    void WhoisThread::SetMsgToProcess(WhoisMsg* pMsg)
    {
    	m_SynchBarrier.Take();
    	msgToProcess=pMsg;
    }

