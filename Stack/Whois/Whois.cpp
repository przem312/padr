
/*
 * Whois.cpp
 *
 *  Created on: Mar 19, 2015
 *      Author: PSokolo
 */

#include "Whois.hpp"
#include "WhoisMsg.h"
#include "CommPortBase.h"
#include "WhoisThread.h"
#include "CommunicationMsg.h"
#include "WebService/Src/OSAL/Linux/NamedPipe.h"
#include "../Application/AppMngr.h"

Whois* Whois::m_pInstance=NULL;
Whois::Whois(Communication::CommunicationBase* pCommport, DeviceApp* app)
:identity(app),
 commport(pCommport)/*,
 pipe(new NamedPipe(table))*/
{
	m_pInstance=this;

    pCommport->RegisterUser(&Whois::ReceiveWhois,MSG_WHOIS);

    me.setDeviceIp(Device::GetDeviceIp());
    me.setDeviceName(Device::GetDeviceName());
    me.setPath(ETHERNET);
    me.setStatus(1);
    char buffer[8]= {};
    std::memset(buffer,0,8);
    AppMngr::GetInstance()->ExecuteRequest(APP_LIST, 0, buffer,8 );
    me.setAppList(buffer,8);
	table.AddEntry(me);
}

Whois::~Whois()
{
    // TODO Auto-generated destructor stub
}

DeviceStatus::Status Whois::Receive(MessageBody* pMsg)
{
	WhoisMsg* whoisMsg = (WhoisMsg*)pMsg;
	if(whoisMsg->IsResponse())
	{
		WhoisEntry* newEntry = m_WhoisEntryPool.AcquireObject();
		if(newEntry)
		{
			newEntry->setDeviceIp(whoisMsg->getIp());
			newEntry->setDeviceName(whoisMsg->getdeviceName());
			newEntry->setPath((CommunicationPath)whoisMsg->getcommunicationPath());
			newEntry->setAppList(whoisMsg->getAppList(), 8);
			if(!table.AddEntry(*newEntry))
			{
				m_WhoisEntryPool.ReleaseObject(newEntry);
			}

		}
		Thread::SleepMs(1);
		return DeviceStatus::GEN_OK;
	}
	else
	{
		whoisMsg->setIp(Device::GetDeviceIp());
		whoisMsg->setDeviceName(Device::GetDeviceName());
		whoisMsg->setcommunicationPath(1);
		whoisMsg->SetResponseMarker();
		whoisMsg->SetMessageSize();
		whoisMsg->setAppList(me.getAppList(),me.getAppListSize());
		return DeviceStatus::COMM_RESP_RDY;

	}
	return DeviceStatus::GEN_BAD_DATA;
}

