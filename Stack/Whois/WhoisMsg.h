/*
 * WhoisMsg.h
 *
 *  Created on: 25 mar 2015
 *      Author: przem312
 */

#ifndef WHOISMSG_H_
#define WHOISMSG_H_
#include "MessageBase.h"

class WhoisMsg : public MessageBody
{
public:
	WhoisMsg()

	{
		SetMessageSize();
		SetMsgType(MSG_WHOIS);
	};

	void SetMessageSize()
	{
		m_msgSize = sizeof(WhoisMsg);
	};


	const char* getIp() const
	{
		return ip;
	}
	void setIp(const char* pIp)
	{
		ip[0]=pIp[0];
		ip[1]=pIp[1];
		ip[2]=pIp[2];
		ip[3]=pIp[3];
	}

	uint16_t getcommunicationPath() const {
		return communicationPath;
	}

	void setcommunicationPath(uint16_t communicationPath) {
		this->communicationPath = communicationPath;
	}

	const char* getdeviceName() const {
		return deviceName;
	}

	void setDeviceName(char* name)
	{
		std::strcpy(deviceName,name);
	}

	const char* getAppList() const {
		return appList;
	}
	void setAppList(const char* pList, unsigned int size ){
		std::memcpy(appList,pList,size);
	}
private:

	char ip[4];
	char deviceName[16];
	uint16_t communicationPath;
    char appList[8];

};




#endif /* WHOISMSG_H_ */
