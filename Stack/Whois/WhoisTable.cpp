/*
 * WhoisTable.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: PSokolo
 */

#include <WhoisTable.hpp>

WhoisTable::WhoisTable()
:numberOfEntries(0)
{
    // TODO Auto-generated constructor stub

}

WhoisTable::~WhoisTable()
{
    // TODO Auto-generated destructor stub
}

bool WhoisTable::AddEntry(const WhoisEntry& rEntry)
{
	if(isOnList(rEntry))
	{
		return false;
	}
    for (int index = 0; index < 8; ++index) {
        if(DataSet[index].isUsed == false)
        {
            DataSet[index].entry = rEntry;
            DataSet[index].isUsed = true;
			++numberOfEntries;
			return true;
        }

    }
    return false;
}
void WhoisTable::RemoveEntry(WhoisEntry& rEntry)
{
    for (int index = 0; index < 8; ++index) {
		if (DataSet[index].entry.Equals(rEntry))
            {
                DataSet[index].entry.Clear();
                DataSet[index].isUsed = false;
				--numberOfEntries;
                break;
            }
        }
}

bool WhoisTable::isOnList(const WhoisEntry& rEntry)
{
	for (int index = 0; index < 8; ++index)
	{
		if(DataSet[index].isUsed
		 &&DataSet[index].entry.Equals(rEntry))
		{
			return true;
		}
	}
	return false;
}

WhoisEntry* WhoisTable::GetEntry(uint8_t index)
{
    if(index >= 8)
    {
        return NULL;	
    }
    return &DataSet[index].entry;
}
