/*
 * WhoisEntry.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: PSokolo
 */

#include <string.h>
#include <WhoisEntry.h>

bool WhoisEntry::Equals(const WhoisEntry& rEntry)
{
    return(
         (std::strcmp(this->deviceName,rEntry.getDeviceName()) == 0) &&
         (deviceIp.GetAddressUint32() == rEntry.getDeviceIpContainer().GetAddressUint32()) &&
         (this->path == rEntry.getPath())
    );
}
