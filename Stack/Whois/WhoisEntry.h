/*
 * WhoisEntry.h
 *
 *  Created on: Mar 10, 2015
 *      Author: PSokolo
 */

#ifndef STACK_WHOISENTRY_H_
#define STACK_WHOISENTRY_H_

#include <string.h>
#include <TargetAddressContainer.h>
/*
 * TODO: PUT ELSEWHERE MAKE IT ELASTIC.
 */
enum CommunicationPath
{
    ETHERNET=1,
    RADIO=2
};

class WhoisEntry
{
public:
    WhoisEntry()
    {
        Clear();
        status = -1;
	};

    ~WhoisEntry()
    {

	};

    void Clear()
    {
        deviceIp.Reset();
        std::memset(deviceName,'\0',16);
        std::memset(appList,0,getAppListSize());
	};

    const char* getDeviceIp() const
    {
        return deviceIp.GetAddressByteArray();
	};

    const TargetAddressContainer& getDeviceIpContainer() const
    {
    	return deviceIp;
    };
    TargetAddressContainer& getDeviceIpContainer()
    {
        return deviceIp;
	};

    const char* getDeviceName() const
    {
        return deviceName;
	};

    void setDeviceIp(const char* ip)
    {
        deviceIp = TargetAddressContainer(ip);
    }

    void setDeviceName(const char* name)
    {
        std::strcpy(deviceName,name);
    }

    CommunicationPath getPath() const
    {
        return path;
    }
    const char* getPathAsString() const
    {
        return path == ETHERNET ? "ETHERNET" : "RADIO";
    }
    void setPath(CommunicationPath path)
    {
        this->path = path;
    }

    char getStatus() const
    {
        return status;
    }

    void setStatus(char status)
    {
        this->status = status;
    }

    bool Equals(const WhoisEntry& rEntry);

	const char* getAppList() const {
		return appList;
	}
	void setAppList(const char* pList, unsigned int size ){
		std::memcpy(appList,pList,(size>getAppListSize()) ? getAppListSize() : size);
	}
	const unsigned int getAppListSize() const {
		return sizeof(appList);
	}
private:
    char deviceName[16];
    TargetAddressContainer deviceIp;
    CommunicationPath path;
    char status;
    // TODO: Make this flexible.
    char appList[8];

};

#endif /* STACK_WHOISENTRY_H_ */
