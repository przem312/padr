/*
 * Whois.hpp
 *
 *  Created on: Mar 19, 2015
 *      Author: PSokolo
 */

#ifndef STACK_WHOIS_WHOIS_HPP_
#define STACK_WHOIS_WHOIS_HPP_
#include "WhoisEntry.h"
#include "WhoisTable.hpp"
#include "MessageBase.h"
#include "Device.h"
#include "ObjectPool.h"
#include "DeviceApp.h"
namespace Communication
{
class CommunicationBase;
}
class WhoisThread;
class NamedPipe;
/*
 *
 */
class Whois
{
public:
    Whois(Communication::CommunicationBase*,DeviceApp*);
    virtual ~Whois();

	Communication::CommunicationBase* getCommport() {
		return commport;
	}
    WhoisTable& getTable()
    {
    	return table;
    }
    static DeviceStatus::Status ReceiveWhois(MessageBody* pMsg)
    {
        return m_pInstance->Receive(pMsg);
    }
private:
    static Whois* m_pInstance;
    DynamicObjectPool<WhoisEntry,8> m_WhoisEntryPool;
    DeviceStatus::Status Receive(MessageBody* pMsg);
    WhoisTable table;
    DeviceApp* identity;
    WhoisEntry me;
    //NamedPipe* pipe;
    Communication::CommunicationBase* commport;
};

#endif /* STACK_WHOIS_WHOIS_HPP_ */
