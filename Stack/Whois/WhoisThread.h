/*
 * WhoisThread.h
 *
 *  Created on: Mar 19, 2015
 *      Author: PSokolo
 */

#ifndef STACK_WHOIS_WHOISTHREAD_H_
#define STACK_WHOIS_WHOISTHREAD_H_
#include "Thread.h"
#include "Mutex.h"
#include "ObjectPool.h"
#include "WhoisEntry.h"
/*
 *
 */
class WhoisTable;
class MessageBody;
class WhoisMsg;
class WhoisThread : public Thread
{
public:
    WhoisThread(WhoisTable& rTable, DynamicObjectPool<WhoisEntry,8>& rEntry);
    virtual ~WhoisThread(){};
    virtual void run();
    void SetMsgToProcess(WhoisMsg* pMsg);
private:
    WhoisMsg* msgToProcess;
    Mutex m_SynchBarrier;
    WhoisTable& table;
    DynamicObjectPool<WhoisEntry,8>& entry;
};

#endif /* STACK_WHOIS_WHOISTHREAD_H_ */
