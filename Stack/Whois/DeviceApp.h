/*
 * DeviceApp.h
 *
 *  Created on: 25 sie 2014
 *      Author: przem312
 */

#ifndef DEVICEAPP_H_
#define DEVICEAPP_H_
#include "AppBase.h"
#include "Mutex.h"
class WhoisEntry;
class Rest;
namespace Communication
{
class CommunicationBase;
}
class Whois;
class DeviceApp :public AppBase
{
public:
	DeviceApp(AppType Type, Communication::CommunicationBase* pBase);
	virtual ~DeviceApp();
	void FillTableEntry(WhoisEntry* entry);
protected:
    virtual void ProcessAppSpecificResponse(AttributeSet attr, char* pBuffer);
    virtual uint8_t GetArgumentNumber()
    {
    	return 3;
    }

    virtual bool GetAppSpecific(const AttributeSet attributeId,
                                                char* pBuffer);
private:
	static const uint8_t DEVICE_NAME_ID = 5;
	static const uint8_t DEVICE_IP_ID = 6;
	static const uint8_t DEVICE_STATUS_ID = 7;
	static const uint8_t DEVICE_REQUEST_ID = 8;
	Whois* pWhois;
	WhoisEntry* pEntry;
	volatile bool isReady;
	Rest* rest;
	Mutex m_Mutex;
	Communication::CommunicationBase* pCommport;
};

#endif /* DEVICEAPP_H_ */
