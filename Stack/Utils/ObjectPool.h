/*
 * ObjectPool.h
 *
 *  Created on: Dec 13, 2013
 *      Author: przem312
 */

#ifndef OBJECTPOOL_H_
#define OBJECTPOOL_H_
#include <Types.h>
#include <stddef.h>
#include "AssertMacro.h"
#include "Mutex.h"
/*
 *	Template object for creating object pool.
 */

template <class PooledClass, int ArraySize>class ObjectPool
{
public:
	/*
	 * Returns pointer to one object from pool.
	 * If no objects are available-NULL is returned.
	 */
	PooledClass* AcquireObject()
	{
		m_Mutex.Take();
		PooledClass* pObj=NULL;
		for(int objNo=0;objNo<NUMBER_OF_OBJECTS;objNo++)
		{
			if(!m_ObjectPool[objNo].inUse)
			{
				pObj = new(m_ObjectPool[objNo].m_pObj) PooledClass();
				m_ObjectPool[objNo].inUse = true;
				break;
			}
		}
		m_Mutex.Release();
		return pObj;
	};

	void RemoveObject()
	{
		for(int objNo=0;objNo<NUMBER_OF_OBJECTS;objNo++)
		{
			RemoveObject(this->m_ObjectPool[objNo].m_pObj);
		}
	};

	void ReleaseObject(PooledClass* pObj)
	{
		m_Mutex.Take();
		for(int objNo=0;objNo<NUMBER_OF_OBJECTS;objNo++)
		{
			if( (pObj == (PooledClass*)m_ObjectPool[objNo].m_pObj) && m_ObjectPool[objNo].inUse)
			{
				this->m_ObjectPool[objNo].inUse = false;
				pObj->~PooledClass();
				break;
			}
		}
		m_Mutex.Release();
		//     ASSERT(false);
	};

	static const size_t NUMBER_OF_OBJECTS=ArraySize;
protected:
	struct objectPool
	{
		objectPool()
		{
			memset(m_pObj,0,sizeof(PooledClass));
			inUse = false;
		};
		~objectPool()
		{
            memset(m_pObj,0,sizeof(PooledClass));
		};
		void DeleteObject()
		{
			//delete m_pObj;
		    memset(m_pObj,0,sizeof(PooledClass));
		};
		char m_pObj[sizeof(PooledClass)];
		bool inUse;
	}m_ObjectPool[NUMBER_OF_OBJECTS];
	Mutex m_Mutex;
};

template <class PooledClass, int ArraySize>class DynamicObjectPool :public ObjectPool<PooledClass,ArraySize>
{
	/*
	 * Creates objects by using object's standard ctor.
	 */
public:
	~DynamicObjectPool()
	{
		for(size_t objNo=0;objNo<ArraySize;objNo++)
		{
//			m_ObjectPool[objNo].DeleteObject();
		}
	}
	void AddObj()
	{
		for(size_t objNo=0;objNo<ArraySize;objNo++)
		{
			ASSERT(this->m_ObjectPool[objNo].m_pObj == NULL);

//			m_ObjectPool[objNo].m_pObj=new PooledClass();

		}
	};

	 static const size_t NUMBER_OF_OBJECTS=ArraySize;

};
#endif /* OBJECTPOOL_H_ */
