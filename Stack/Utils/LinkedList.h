/*
 * LinkedList.h
 *
 *  Created on: Dec 1, 2013
 *      Author: przem312
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_
#include <Types.h>
#include <stdint.h>
#include <stddef.h>
#include <AssertMacro.h>
/*
 *
 */
class ListNode;

class ListNode
{
public:
	ListNode():m_pPrev(NULL),
	m_pNext(NULL)
{
		//Nothing to do
};

	~ListNode()
	{
		if(m_pNext)
		{
			m_pNext->m_pPrev = m_pPrev;
			if(m_pPrev)
			{
				m_pPrev->m_pNext = m_pNext;
			}
		}
		// FIXME MANAGE THE OBJECT INITIALIZATION.
	};

	ListNode* m_pPrev;
	ListNode* m_pNext;
};

class LinkedList
{
public:
	uint32_t GetListLength()
	{
		return m_ListLength;
	}
	void AddNode(ListNode* pArg)
	{
		if (isNodeOnList(pArg))
		{
			ASSERT(false);
		}

		if (m_pHead == NULL)
		{
			m_pHead = pArg;
			
			m_pHead->m_pNext = NULL;
			m_pHead->m_pPrev = NULL;

		}
		else
		{
			
			ListNode* pPrev = m_pHead;
			m_pHead = pArg;
			pPrev->m_pNext = m_pHead;
			m_pHead->m_pPrev = pPrev;
			m_pHead->m_pNext = NULL;
		}
		m_ListLength += 1;
	};

	void RemoveNode(ListNode* pArg)
	{
		if (!isNodeOnList(pArg))
		{
			ASSERT(false);
		}
		ListNode* pPrevTempNode = pArg->m_pPrev;
		ListNode* pNextTempNode = pArg->m_pNext;
		if (pArg->m_pPrev)
		{
			pArg->m_pPrev->m_pNext = pNextTempNode;
		}
		if (pArg->m_pNext)
		{
			pArg->m_pNext->m_pPrev = pPrevTempNode;
		}
		if (m_pHead == pArg)
		{
			m_pHead = pPrevTempNode;
		}
		if (m_pCurrent == pArg)
		{
			m_pCurrent == m_pHead;
		}

		m_ListLength -= 1;
	};

	ListNode* GetFirst()
	{
		m_pCurrent = m_pHead;
		return m_pCurrent;
	};

	ListNode* GetNext()
	{
		return GetPrevious();
	};

	ListNode* GetPrevious()
	{
		if(m_pCurrent)
		{
			m_pCurrent = m_pCurrent->m_pPrev;
		}
		return m_pCurrent;
	};

	void CleanList()
	{
		if(m_pHead == NULL)
			return;
		ListNode* pPrevTempNode =NULL;
		do
		{
			pPrevTempNode= m_pHead->m_pPrev;
			RemoveNode(m_pHead);
		}
		while(pPrevTempNode);
		m_ListLength=0;
	};

	LinkedList()
	:m_pHead(NULL)
	,m_pCurrent(NULL)
	,m_ListLength(0)
	{
		// Nothing to do.
	};
	~LinkedList()
	{
		CleanList();
	};
private:

	bool isNodeOnList(ListNode* pArg)
	{
		ListNode* pListNode = GetFirst();
		while(pListNode)
		{
			if(pListNode == pArg)
			{
				return true;
			}
			pListNode = GetNext();
		}
		return false;
	}

	ListNode* m_pHead;
	ListNode* m_pCurrent;
	uint32_t m_ListLength;

};


#endif /* LINKEDLIST_H_ */
