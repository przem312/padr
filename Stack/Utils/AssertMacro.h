/*
 * Assert.h
 *
 *  Created on: Mar 29, 2014
 *      Author: przem312
 */

#ifndef ASSERT_H_
#define ASSERT_H_
#include "Console.h"
#define ASSERT_EXTENDED
#if defined(ASSERT_EXTENDED)
#define ASSERT_MSG()    ERROR_LOG("ASSERTION IN %s line %d",__FILE__, __LINE__)
#else
#define ASSERT_MSG() ERROR_LOG("ASSERT!")
#endif

#define ASSERT(x) if(x == 0)\
        {\
                ASSERT_MSG();\
                while(1);\
        }

#endif /* ASSERT_H_ */
