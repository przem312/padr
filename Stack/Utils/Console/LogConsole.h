/*
 * LogConsole.h
 *
 *  Created on: Oct 19, 2013
 *      Author: przem312
 */

#ifndef LOGCONSOLE_H_
#define LOGCONSOLE_H_

/*
 *
 */

#include <stddef.h>

#include "ConsoleInstance.h"

class LogConsole : public ConsoleInstance
{
public:
    static LogConsole* GetInstance();

private:
    virtual void WriteOutput(const char* pMessage, const size_t msgSize);
    LogConsole();
    virtual
    ~LogConsole();
};

#endif /* LOGCONSOLE_H_ */
