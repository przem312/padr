/*
 * ConsoleInstance.h
 *
 *  Created on: Oct 19, 2013
 *      Author: przem312
 */

#ifndef CONSOLEINSTANCE_H_
#define CONSOLEINSTANCE_H_
#include <cstring>
#include <stdint.h>
/*
 *
 */
class ConsoleInstance
{
public:
    // Defines input to Log.
    virtual void Write(const char* pMessage)
    {
        const size_t msgSize = GetMsgSize(pMessage);
        WriteOutput(pMessage, msgSize);
    }
    static const size_t MAX_SIZE_OF_MESSAGE = 32;
protected:
    virtual void WriteOutput(const char* pMessage, const size_t msgSize) = 0;

    ConsoleInstance(size_t maxMsgSize)
    :m_MsgSize(maxMsgSize)
    {};
    ConsoleInstance()
    :m_MsgSize(ConsoleInstance::MAX_SIZE_OF_MESSAGE)
    {};
    virtual
    ~ConsoleInstance(){};
private:
    size_t GetMsgSize(const char* pMessage)
    {
        size_t strLen = std::strlen(pMessage);
        if(strLen > m_MsgSize)
            {
                strLen = m_MsgSize;
            }
        return strLen;
    }
    const size_t m_MsgSize;
};

#endif /* CONSOLEINSTANCE_H_ */
