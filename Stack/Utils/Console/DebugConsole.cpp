/*
 * DebugConsole.cpp
 *
 *  Created on: Oct 19, 2013
 *      Author: przem312
 */

#include "DebugConsole.h"
#include <cstdio>
//#include "min_printf.h"

DebugConsole* DebugConsole::GetInstance()
{

	return Create(256);
}

DebugConsole* DebugConsole::Create(const uint32_t maxMsgSize)
{
    static DebugConsole instance(maxMsgSize);
    return &instance;
}


void DebugConsole::WriteOutput(const char* pMessage, size_t msgSize)
{
	//min_printf("[%d]DEBUG: %s \n" ,m_MsgNumber, pMessage);
	m_MsgNumber++;
}

DebugConsole::DebugConsole(const uint32_t maxMsgSize)
:ConsoleInstance(maxMsgSize),
 m_MsgNumber(0)
{
    // TODO Auto-generated constructor stub

}

DebugConsole::~DebugConsole()
{
    // TODO Auto-generated destructor stub
}
