/*
 * DebugConsole.h
 *
 *  Created on: Oct 19, 2013
 *      Author: przem312
 */

#ifndef DEBUGCONSOLE_H_
#define DEBUGCONSOLE_H_

/*
 *
 */

#include <stddef.h>

#include "ConsoleInstance.h"

class DebugConsole : protected ConsoleInstance
{
public:
	static DebugConsole* GetInstance();
    static DebugConsole* Create(const uint32_t maxMsgSize);
    virtual void WriteOutput(const char* pMessage, const size_t msgSize);
private:
    DebugConsole(const uint32_t maxMsgSize);
    virtual
    ~DebugConsole();
    uint32_t m_MsgNumber;
};

#endif /* DEBUGCONSOLE_H_ */
