/*
 * Console.h
 *
 * Declaration of Console object.
 *
 *  Created on: Oct 19, 2013
 *      Author: przem312
 */  


#ifndef CONSOLE_H_
#define CONSOLE_H_

#if defined(LOG_FILES)
   // FIXME
#include <cstdio>
#include <stdarg.h>
#define DEBUG 
#define LOG 
// FIXME
#define ERROR_LOG Console::GetConsole()->Debug

#include "DebugConsole.h" // DebugConsole instance.
#include "LogConsole.h"
//
   /*
 *
 */
class Console
{
public:
    // Returns instance of console log.
    static Console* GetConsole();

    void Init(bool isFsOn = false);

    // Gets instance of Error Console
//    ErrorConsole Error(char* errorLog);
    // Gets instance of Log Console
    void Log(char* logText, ...);
    // Gets instance of Debug Console
    void Debug(char* debugLog, ...);

private:
    Console();
    virtual ~Console();

    bool m_fsOn;

//    ErrorConsole* m_pErrorIntance;
    LogConsole* m_pLogInstance;
    DebugConsole* m_pDebugInstance;
};

#else 

//#if !defined NDEBUG
#include <stdio.h>
#if 1
#define DEBUG(msg, ...) \
    printf("\n[DEBUG:] ");\
    printf(msg, ##__VA_ARGS__)
#else 
#define DEBUG(msg, ...)
#endif
#define LOG(msg, ...) \
    printf("\n[LOG:] ");\
    printf(msg, ##__VA_ARGS__);
#define ERROR_LOG(msg, ...) \
	    printf("\n[ERROR:] ");\
	    printf(msg, ##__VA_ARGS__);
#endif
#endif /* CONSOLE_H_ */
