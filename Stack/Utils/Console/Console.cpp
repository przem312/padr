/*
 * Console.cpp
 *
 *  Created on: Oct 19, 2013
 *      Author: przem312
 */
#include <stdio.h>
#include <string.h>
#include "Console.h"
#include "Device.h"
// Returns instance of console log.
Console* Console::GetConsole()
{
    static Console instance;
    return &instance;
}

void Console::Init(bool isFsOn)
{
    char* deviceName = Device::GetDeviceName();
    DEBUG(deviceName);
    LOG(deviceName);
}

//// Gets instance of Error Console
//ErrorConsole Console::Error()
//{
//    return ErrorConsole::GetInstance();
//}

// Gets instance of Log Console
void Console::Log(char* logText,...)
{
    if(m_fsOn)
    {
        m_pLogInstance->Write(logText);
    }
    else
    {
        Debug(logText);
    }
}

// Gets instance of Debug Console
void Console::Debug(char* debugLog,...)
{
//    char dest[256];
//    va_list arguments;
//    va_start(arguments,debugLog);
//    vsprintf(dest,debugLog,arguments);
//    m_pDebugInstance->WriteOutput(dest, std::strlen(dest));
}

Console::Console()
{
    m_pDebugInstance = DebugConsole::GetInstance();
    m_pLogInstance = LogConsole::GetInstance();
}

Console::~Console()
{
    // TODO Auto-generated destructor stub
}
