/*
 * Notifier.h
 *
 *  Created on: Jan 13, 2014
 *      Author: przem312
 */

#ifndef NOTIFIER_H_
#define NOTIFIER_H_


#include "LinkedList.h"
#include "Listener.h"
/*
 * Notifier class template implementation. It includes LinkedList of Listener objects.
 */
template <class NotifierArg> class Notifier :public LinkedList
{
public:
	void RegisterListener(Listener<NotifierArg>* pListenerAddr)
	{
		AddNode(pListenerAddr);
	}

	void RemoveListener(Listener<NotifierArg>* pListenerAddr)
	{
		RemoveNode(pListenerAddr);
	}

	void Notify(NotifierArg& rArg)
	{
		Listener<NotifierArg>* pListener = (Listener<NotifierArg>*)GetFirst();
		while(pListener)
		{
			pListener->OnNotify(rArg);
			pListener = (Listener<NotifierArg>*)GetNext();
		}
	}

protected:
	Notifier()
{
		// Nothing to do.
}
	~Notifier()
	{

	}

};

#endif /* NOTIFIER_H_ */
