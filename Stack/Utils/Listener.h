/*
 * Listener.h
 *
 *  Created on: Jan 13, 2014
 *      Author: przem312
 */

#ifndef LISTENER_H_
#define LISTENER_H_
#include "LinkedList.h"
/*
 * TODO
 */
template <class NotifyArgument> class Listener :public ListNode
{
public:
    virtual void OnNotify(NotifyArgument& rArg) = 0;
    virtual ~Listener(){};
};

#endif /* LISTENER_H_ */
