/*
 * BufferPool.hpp
 *
 *  Created on: May 29, 2014
 *      Author: PSokolo
 */

#ifndef BUFFERPOOL_HPP_
#define BUFFERPOOL_HPP_
#include "ObjectPool.h"
#include "Buffer.hpp"
/*
 *
 */
template <size_t numberOfBuffers,size_t sizeOfBuffer> class BufferPool
	: public ObjectPool <BufferImpl<sizeOfBuffer>, numberOfBuffers>
{
public:
    BufferPool()
    {
        for (int i = 0; i<numberOfBuffers; i++)
        {
           
			//this->AddObj(&m_buffers[i]);
        }
    }
    ~BufferPool()
    {
        for (int i = 0; i<numberOfBuffers; i++)
        {
            //this->RemoveObject(&m_buffers[i]);
        }
    }

private:
    //BufferImpl<sizeOfBuffer> m_buffers[numberOfBuffers];
};



#endif /* BUFFERPOOL_HPP_ */
