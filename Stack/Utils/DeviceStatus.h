/*
 * DeviceStatus.h
 *
 * Contains general set of statuses used in stack.
 * All modules within stack shall have their own
 * for logging.
 *  Created on: Dec 7, 2013
 *      Author: przem312
 */

#ifndef DEVICESTATUS_H_
#define DEVICESTATUS_H_
#include "Types.h"
class DeviceStatus
{
public:
	typedef enum status
	    {
	        /* General statuses */
	        GEN_OK = 0,
	        GEN_IDDLE = 1,
	        GEN_FAIL = 2,
	        GEN_BAD_DATA = 3,
	        GEN_FATAL = 4,

	        COMM_RESP_RDY = 5,
	        COMM_TIMEOUT = 6,
	        COMM_MSG_PENDING = 7
	    }Status;

    DeviceStatus()
    {
        //Nothing to do
    };
    ~DeviceStatus()
    {
        //Nothing to do
    };

    Status GetStatus()
    {
        return m_Status;
    };
    void SetStatus(Status newStatus)
    {
      m_Status = newStatus;
    };
    Status m_Status;
private:
    void ProcessStatus(Status& rStatus){};
};
#endif /* DEVICESTATUS_H_ */
