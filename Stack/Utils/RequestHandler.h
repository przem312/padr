/*
 * RequestHandler.hpp
 *
 *  Created on: Aug 14, 2014
 *      Author: PSokolo
 */

#ifndef REQUESTHANDLER_HPP_
#define REQUESTHANDLER_HPP_

/*
 *
 */

#include "AppMngr.h"

class TargetAddressContainer;

typedef uint16_t RequestHandlerNo;

class RequestHandler
{
public:
    RequestHandlerNo PostRequest(AttributeNo number, TargetAddressContainer* pAddress);
    TargetAddressContainer* GetRequest(const RequestHandlerNo number, AttributeNo& Attrnumber);
    RequestHandler();
    virtual
    ~RequestHandler();
private:
    typedef struct RequestStruct
    {
    	RequestStruct()
    	{
            m_AttrNumber = (AttributeNo)0;
    		m_pContainer=NULL;
    	}
    	AttributeNo m_AttrNumber;
    	TargetAddressContainer* m_pContainer;
    }RequestStruct;
    // FIXME should this be configurable?
    RequestStruct m_buffer[8];
    RequestHandlerNo m_currentNo;
};

#endif /* REQUESTHANDLER_HPP_ */
