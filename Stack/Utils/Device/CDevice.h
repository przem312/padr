/*
 * CDevice.h
 *
 *  Created on: Jul 29, 2014
 *      Author: przem312
 */

#ifndef CDEVICE_H_
#define CDEVICE_H_
#ifdef __cplusplus
extern "C" {
#endif
char* GetDeviceMac(void);

char* GetDeviceIp(void);
#ifdef __cplusplus
}
#endif

#endif /* CDEVICE_H_ */
