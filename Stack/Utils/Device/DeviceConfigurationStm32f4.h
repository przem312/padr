/*
 * DeviceConfigurationStm32f4.h
 *
 *  Created on: Feb 3, 2014
 *      Author: przem312
 */

#ifndef DEVICECONFIGURATIONSTM32F4_H_
#define DEVICECONFIGURATIONSTM32F4_H_
#include <DeviceConfiguration.h>
#define STM32F4 1
static const DeviceConfig DEVICE_CONFIG=
{
    false,       // m_IsDisplayOn;
    true,       // m_IsLedOn;
    false,       // m_IsAdcOn;
    false,      // m_IsPwmOn;
    false,      // m_IsIoOn;
    false,       // m_IsWebserverOn;
    false,       // m_IsSdCardOn;
    false,       // m_IsUartOn;

};

#define NUMBER_OF_ADC 1

#define MAX_CONNECTIONS_COUNT 2

#define MAX_BUFFERS_FOR_MSG MAX_CONNECTIONS_COUNT+1

#define MAX_MSG_SIZE 64U

#define USE_RADIO_DRIVER 0

#define USE_ETHERNET_DRIVER 1

#define USE_UART_DRIVER 0

#endif /* DEVICECONFIGURATIONSTM32F4_H_ */
