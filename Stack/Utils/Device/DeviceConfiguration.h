/*
 * DeviceConfiguration.h
 *
 *  Created on: Feb 3, 2014
 *      Author: przem312
 */

#ifndef DEVICECONFIGURATION_H_
#define DEVICECONFIGURATION_H_

typedef struct deviceConfig
{
    bool m_IsUiOn;
    bool m_IsLedOn;
    bool m_IsAdcOn;
    bool m_IsPwmOn;
    bool m_IsIoOn;
    bool m_IsWebserverOn;
    bool m_IsSdCardOn;
    bool m_IsUartOn;
}DeviceConfig;

#endif /* DEVICECONFIGURATION_H_ */
