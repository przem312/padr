/*
 * DeviceConfigurationStm32f0.h
 *
 *  Created on: Feb 9, 2014
 *      Author: przem312
 */

#ifndef DEVICECONFIGURATIONPC_H_
#define DEVICECONFIGURATIONPC_H_
#include <DeviceConfiguration.h>


static DeviceConfig DEVICE_CONFIG ={
    false,       // m_IsDisplayOn;
    false,       // m_IsLedOn;
    false,       // m_IsAdcOn;
    false,      // m_IsPwmOn;
    false,      // m_IsIoOn;
    false,       // m_IsWebserverOn;
    false,
    false
};

#define MAX_CONNECTIONS_COUNT 16

#define MAX_BUFFERS_FOR_MSG MAX_CONNECTIONS_COUNT+1

#define MAX_MSG_SIZE 64

#define USE_RADIO_DRIVER 0

#define USE_ETHERNET_DRIVER 1

#define USE_UART_DRIVER 0

#endif /* DEVICECONFIGURATIONSTM32F0_H_ */
