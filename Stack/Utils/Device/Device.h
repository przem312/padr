/*
 * Device.h
 *
 *  Created on: Dec 8, 2013
 *      Author: przem312
 */

#ifndef DEVICE_H_
#define DEVICE_H_
#define PC 1

#ifdef STM32F4
#include "DeviceConfigurationStm32f4.h"
#elif STM32F0
#include "DeviceConfigurationStm32f0.h"
#elif PC
#include "DeviceConfigurationPC.h"
#endif
#include "Types.h"
/*
 *
 */
class Device;

class Device
{
public:
    typedef enum DeviceType_t
    {
    	END_NODE,
    	MASTER
    }DeviceType;

    static uint8_t GetDeviceId();
    static char* GetDeviceIp();
	static char* GetDeviceIpAsString();
	static char* GetMacAddress();
	static char* GetDeviceName();
	static char* GetBroadcastArray();
	static void SetDeviceIp(const char* ip);
	static bool SetDeviceName(char* string, uint32_t size);

	static const char* getUsedPort() {
		return m_UsedPort;
	}
    static DeviceType GetDeviceType()
    {
    	return IsMaster() ? MASTER : END_NODE;
    }
    static bool IsMaster()
    {
    	return m_DeviceId == 0x00;
    }


private:
    static uint8_t m_DeviceId;
    static char m_IpAddress[4];
    static char m_broadcastArray[4];
    static char m_MacAddress[6];
    static char m_DeviceName[16];
    static const char* m_UsedPort;
};

#endif /* DEVICE_H_ */
