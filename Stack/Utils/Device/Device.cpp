/*
 * Device.cpp
 *
 *  Created on: Dec 8, 2013
 *      Author: przem312
 */
#include <cstring>
#include "Device.h"
#include "BuildSpecific.h"
#include "TargetAddressContainer.h"

char Device::m_broadcastArray[] = {224,0,2,123};
uint8_t Device::GetDeviceId()
{
    return m_DeviceId;
}

char* Device::GetDeviceIp()
{
    return m_IpAddress;
}
char* Device::GetDeviceName()
{
    return m_DeviceName;
}
void Device::SetDeviceIp(const char* ip)
{
	std::memcpy(m_IpAddress, ip, 4);
}
bool Device::SetDeviceName(char* string, uint32_t size)
{
    if(size < 16)
    {
        return false;
    }
    std::memcpy(m_DeviceName,string,size);
    return true;
}
char* Device::GetDeviceIpAsString()
{
  TargetAddressContainer address(GetDeviceIp());
  return address.GetAddressAsString();
}
char* Device::GetMacAddress()
{
    return m_MacAddress;
}
char* Device::GetBroadcastArray()
{
    return m_broadcastArray;
}

#ifdef __cplusplus
extern "C" {
#endif
char* GetDeviceMac(void)
{
  return Device::GetMacAddress();
}

char* GetDeviceIp(void)
{
  return Device::GetDeviceIp();
}

#ifdef __cplusplus
}
#endif
