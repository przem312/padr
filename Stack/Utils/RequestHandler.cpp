/*
 * RequestHandler.cpp
 *
 *  Created on: Aug 14, 2014
 *      Author: PSokolo
 */

#include "RequestHandler.h"


RequestHandlerNo RequestHandler::PostRequest(AttributeNo number, TargetAddressContainer* pAddres)
{
	RequestHandlerNo retVal = ++m_currentNo;
	if(m_currentNo == 8)
	{
		retVal = m_currentNo = 0;
	}
	RequestStruct req;
	req.m_AttrNumber=number;
	req.m_pContainer=pAddres;
	m_buffer[retVal]=req;
	return retVal;
}
TargetAddressContainer* RequestHandler::GetRequest(const RequestHandlerNo number, AttributeNo& attrNumber)
{
	if(number >= 8)
	{
		return NULL;
	}
	RequestStruct retVal = m_buffer[number];
	attrNumber = retVal.m_AttrNumber;
	return retVal.m_pContainer;
}
RequestHandler::RequestHandler()
:m_currentNo(0)
{
	// TODO Auto-generated constructor stub

}

RequestHandler::~RequestHandler()
{
	// TODO Auto-generated destructor stub
}

