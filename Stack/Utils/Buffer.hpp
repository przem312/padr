/*
 * Buffer.hpp
 *
 *  Created on: May 29, 2014
 *      Author: PSokolo
 */

#ifndef BUFFER_HPP_
#define BUFFER_HPP_

/*
 *
 */
#include "Types.h"
#include "stddef.h"
#include <cstring>
template <int BufferSize> class BufferImpl
{
public:
    BufferImpl()
    {
        std::memset(m_buffer, 0,BufferSize);
    };
    char* GetBufferStart()
    {
      return m_buffer;
    };
    int GetBufferSize()
    {
        return BufferSize;
    };

private:
    char m_buffer[BufferSize];
};

#endif /* BUFFER_HPP_ */
