/*
 * Stack.h
 *
 *  Created on: Jan 30, 2014
 *      Author: przem312
 */

#ifndef STACK_H_
#define STACK_H_

#include "Console.h"
#include <DeviceStatus.h>
#include <Device.h>
#include <CommPortBase.h>
#include <AppMngr.h>
#include <Whois.hpp>
/*
 *
 */
class AppMngr;
class CommunicationThread;
class Stack
{
public:

    Stack();
    virtual ~Stack();
	/*
	 * Entry point for stack usage.
	 * Shall be called only once.
	 */
    DeviceStatus::Status Start();
    /*
     * Shutdown procedure.
     *
     */
    void Stop();
    Whois& GetWhois()
    {
        return *whois;
    }
    DeviceStatus& GetStatus()
    {
      return m_DeviceStatus;
    }
    
#if defined(LOG_FILES)
    static Console* GetConsole()
    {
        return m_pConsoleInstance;
    }
#endif

    Communication::CommunicationBase* m_CommPort;
private:
    DeviceStatus m_DeviceStatus;

#if defined(LOG_FILES)
    static Console* m_pConsoleInstance;
#endif

    // Initializing aggregated members.
    void Init();
    Whois* whois;
    CommunicationThread* m_pThread;
    // Reference to console instance.

};

#endif /* STACK_H_ */
