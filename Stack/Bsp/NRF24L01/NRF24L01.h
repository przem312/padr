/*
 * NRF24L01.h
 *
 *  Created on: May 31, 2014
 *      Author: przem312
 */

#ifndef NRF24L01_H_
#define NRF24L01_H_


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//NRF24L01 command
#define NRF_READ_REG        0x00  //¶ÁÅäÖÃ¼Ä´æÆ÷,µÍ5Î»Îª¼Ä´æÆ÷µØÖ·
#define NRF_WRITE_REG       0x20  //Ð´ÅäÖÃ¼Ä´æÆ÷,µÍ5Î»Îª¼Ä´æÆ÷µØÖ·
#define NRF_ACTIVATE        0x50  //
#define NRF_RD_RX_PL_WID    0x60  // read rx payload width
#define NRF_RD_RX_PLOAD     0x61  //¶ÁRXÓÐÐ§Êý¾Ý,1~32×Ö½Ú
#define NRF_WR_TX_PLOAD     0xA0  //Ð´TXÓÐÐ§Êý¾Ý,1~32×Ö½Ú
#define NRF_WR_ACK_PAYLOAD  0xA8  //
#define NRF_FLUSH_TX        0xE1  //Çå³ýTX FIFO¼Ä´æÆ÷.·¢ÉäÄ£Ê½ÏÂÓÃ
#define NRF_FLUSH_RX        0xE2  //Çå³ýRX FIFO¼Ä´æÆ÷.½ÓÊÕÄ£Ê½ÏÂÓÃ
#define NRF_REUSE_TX_PL     0xE3  //ÖØÐÂÊ¹ÓÃÉÏÒ»°üÊý¾Ý,CEÎª¸ß,Êý¾Ý°ü±»²»¶Ï·¢ËÍ.
#define NRF_NOP             0xFF  //¿Õ²Ù×÷,¿ÉÒÔÓÃÀ´¶Á×´Ì¬¼Ä´æÆ÷
//NRF24L01 address
#define NRF_CONFIG          0x00  //ÅäÖÃ¼Ä´æÆ÷µØÖ·;bit0:1½ÓÊÕÄ£Ê½,0·¢ÉäÄ£Ê½;bit1:µçÑ¡Ôñ;bit2:CRCÄ£Ê½;bit3:CRCÊ¹ÄÜ;
                              //bit4:ÖÐ¶ÏMAX_RT(´ïµ½×î´óÖØ·¢´ÎÊýÖÐ¶Ï)Ê¹ÄÜ;bit5:ÖÐ¶ÏTX_DSÊ¹ÄÜ;bit6:ÖÐ¶ÏRX_DRÊ¹ÄÜ
#define NRF_EN_AA           0x01  //Ê¹ÄÜ×Ô¶¯Ó¦´ð¹¦ÄÜ  bit0~5,¶ÔÓ¦Í¨µÀ0~5
#define NRF_EN_RXADDR       0x02  //½ÓÊÕµØÖ·ÔÊÐí,bit0~5,¶ÔÓ¦Í¨µÀ0~5
#define NRF_SETUP_AW        0x03  //ÉèÖÃµØÖ·¿í¶È(ËùÓÐÊý¾ÝÍ¨µÀ):bit1,0:00,3×Ö½Ú;01,4×Ö½Ú;02,5×Ö½Ú;
#define NRF_SETUP_RETR      0x04  //½¨Á¢×Ô¶¯ÖØ·¢;bit3:0,×Ô¶¯ÖØ·¢¼ÆÊýÆ÷;bit7:4,×Ô¶¯ÖØ·¢ÑÓÊ± 250*x+86us
#define NRF_RF_CH           0x05  //RFÍ¨µÀ,bit6:0,¹¤×÷Í¨µÀÆµÂÊ;
#define NRF_RF_SETUP        0x06  //RF¼Ä´æÆ÷;bit3:´«ÊäËÙÂÊ(0:1Mbps,1:2Mbps);bit2:1,·¢Éä¹¦ÂÊ;bit0:µÍÔëÉù·Å´óÆ÷ÔöÒæ
#define NRF_STATUS          0x07  //×´Ì¬¼Ä´æÆ÷;bit0:TX FIFOÂú±êÖ¾;bit3:1,½ÓÊÕÊý¾ÝÍ¨µÀºÅ(×î´ó:6);bit4,´ïµ½×î¶à´ÎÖØ·¢
                              //bit5:Êý¾Ý·¢ËÍÍê³ÉÖÐ¶Ï;bit6:½ÓÊÕÊý¾ÝÖÐ¶Ï;

#define NRF_OBSERVE_TX      0x08  //·¢ËÍ¼ì²â¼Ä´æÆ÷,bit7:4,Êý¾Ý°ü¶ªÊ§¼ÆÊýÆ÷;bit3:0,ÖØ·¢¼ÆÊýÆ÷
#define NRF_CD              0x09  //ÔØ²¨¼ì²â¼Ä´æÆ÷,bit0,ÔØ²¨¼ì²â;
#define NRF_RPD             0x09  //ÔØ²¨¼ì²â¼Ä´æÆ÷,bit0,ÔØ²¨¼ì²â;
#define NRF_RX_ADDR_P0      0x0A  //Êý¾ÝÍ¨µÀ0½ÓÊÕµØÖ·,×î´ó³¤¶È5¸ö×Ö½Ú,µÍ×Ö½ÚÔÚÇ°
#define NRF_RX_ADDR_P1      0x0B  //Êý¾ÝÍ¨µÀ1½ÓÊÕµØÖ·,×î´ó³¤¶È5¸ö×Ö½Ú,µÍ×Ö½ÚÔÚÇ°
#define NRF_RX_ADDR_P2      0x0C  //Êý¾ÝÍ¨µÀ2½ÓÊÕµØÖ·,×îµÍ×Ö½Ú¿ÉÉèÖÃ,¸ß×Ö½Ú,±ØÐëÍ¬RX_ADDR_P1[39:8]ÏàµÈ;
#define NRF_RX_ADDR_P3      0x0D  //Êý¾ÝÍ¨µÀ3½ÓÊÕµØÖ·,×îµÍ×Ö½Ú¿ÉÉèÖÃ,¸ß×Ö½Ú,±ØÐëÍ¬RX_ADDR_P1[39:8]ÏàµÈ;
#define NRF_RX_ADDR_P4      0x0E  //Êý¾ÝÍ¨µÀ4½ÓÊÕµØÖ·,×îµÍ×Ö½Ú¿ÉÉèÖÃ,¸ß×Ö½Ú,±ØÐëÍ¬RX_ADDR_P1[39:8]ÏàµÈ;
#define NRF_RX_ADDR_P5      0x0F  //Êý¾ÝÍ¨µÀ5½ÓÊÕµØÖ·,×îµÍ×Ö½Ú¿ÉÉèÖÃ,¸ß×Ö½Ú,±ØÐëÍ¬RX_ADDR_P1[39:8]ÏàµÈ;
#define NRF_TX_ADDR         0x10  //·¢ËÍµØÖ·(µÍ×Ö½ÚÔÚÇ°),ShockBurstTMÄ£Ê½ÏÂ,RX_ADDR_P0Óë´ËµØÖ·ÏàµÈ
#define NRF_RX_PW_P0        0x11  //½ÓÊÕÊý¾ÝÍ¨µÀ0ÓÐÐ§Êý¾Ý¿í¶È(1~32×Ö½Ú),ÉèÖÃÎª0Ôò·Ç·¨
#define NRF_RX_PW_P1        0x12  //½ÓÊÕÊý¾ÝÍ¨µÀ1ÓÐÐ§Êý¾Ý¿í¶È(1~32×Ö½Ú),ÉèÖÃÎª0Ôò·Ç·¨
#define NRF_RX_PW_P2        0x13  //½ÓÊÕÊý¾ÝÍ¨µÀ2ÓÐÐ§Êý¾Ý¿í¶È(1~32×Ö½Ú),ÉèÖÃÎª0Ôò·Ç·¨
#define NRF_RX_PW_P3        0x14  //½ÓÊÕÊý¾ÝÍ¨µÀ3ÓÐÐ§Êý¾Ý¿í¶È(1~32×Ö½Ú),ÉèÖÃÎª0Ôò·Ç·¨
#define NRF_RX_PW_P4        0x15  //½ÓÊÕÊý¾ÝÍ¨µÀ4ÓÐÐ§Êý¾Ý¿í¶È(1~32×Ö½Ú),ÉèÖÃÎª0Ôò·Ç·¨
#define NRF_RX_PW_P5        0x16  //½ÓÊÕÊý¾ÝÍ¨µÀ5ÓÐÐ§Êý¾Ý¿í¶È(1~32×Ö½Ú),ÉèÖÃÎª0Ôò·Ç·¨
#define NRF_FIFO_STATUS     0x17  //FIFO×´Ì¬¼Ä´æÆ÷;bit0,RX FIFO¼Ä´æÆ÷¿Õ±êÖ¾;bit1,RX FIFOÂú±êÖ¾;bit2,3,±£Áô
                              //bit4,TX FIFO¿Õ±êÖ¾;bit5,TX FIFOÂú±êÖ¾;bit6,1,Ñ­»··¢ËÍÉÏÒ»Êý¾Ý°ü.0,²»Ñ­»·;
#define NRF_DYNPD           0x1C
#define NRF_FEATURE         0x1D
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// NRF24L01 bit field
// config register
#define NRF_PRIM_RX         0x01
#define NRF_PRIM_TX         0x00
#define NRF_POWER_UP        0x02
#define NRF_CRC0            0x04
#define NRF_CRC_1B          0x00
#define NRF_CRC_2B          0x04
#define NRF_EN_CRC          0x08
#define NRF_DIS_CRC         0x00
#define NRF_MASK_MAX_RT     0x10
#define NRF_EN_MAX_RT_IRQ   0x00
#define NRF_DIS_MAX_RT_IRQ  0x10
#define NRF_MASK_TX_OK      0x20
#define NRF_EN_TX_OK_IRQ    0x00
#define NRF_DIS_TX_OK_IRQ   0x20
#define NRF_MASK_RX_OK      0x40
#define NRF_EN_RX_OK_IRQ    0x00
#define NRF_DIS_RX_OK_IRQ   0x40
#define NRF_EN_ALL_IRQ      0x00
#define NRF_DIS_ALL_IRQ     (NRF_DIS_MAX_RT_IRQ | NRF_DIS_TX_OK_IRQ | NRF_DIS_RX_OK_IRQ)
// EN_AA
#define NRF_ENAA_P0         0x01
#define NRF_ENAA_P1         0x02
#define NRF_ENAA_P2         0x04
#define NRF_ENAA_P3         0x08
#define NRF_ENAA_P4         0x10
#define NRF_ENAA_P5         0x20
// EN_RXADDR
#define NRF_ERX_P0          0x01
#define NRF_ERX_P1          0x02
#define NRF_ERX_P2          0x04
#define NRF_ERX_P3          0x08
#define NRF_ERX_P4          0x10
#define NRF_ERX_P5          0x20
// SETUP_AW
#define NRF_AW_3            0x01
#define NRF_AW_4            0x10
#define NRF_AW_5            0x11
// SETUP_RETR
#define NRF_ARD(us)      ( (( (us-1)/250  )&0x0f)<<4 )
#define NRF_ARDx( X_250_plus_250_us)     ( ((X_50_plus_250_us&)&0x0f)<<4 )
#define NRF_ARC( arc_count )            ((arc_count) & 0x0f)
// RF_CH
// RF_SETUP
#define NRF_PWR_18dBm         (0x0<<1)
#define NRF_PWR_12dBm         (0x1<<1)
#define NRF_PWR_6dBm          (0x2<<1)
#define NRF_PWR_0dBm          (0x3<<1)
#define NRF_DR_250Kbps        (1<<5)
#define NRF_DR_1Mbps          (0<<3)
#define NRF_DR_2Mbps          (1<<3)
#define NRF_PLL_LOCK          (1<<4)
#define NRF_CONT_WAVE            (1<<7)
// STATUS register
#define NRF_MAX_RT          0x10  //´ïµ½×î´ó·¢ËÍ´ÎÊýÖÐ¶Ï
#define NRF_TX_OK           0x20  //TX·¢ËÍÍê³ÉÖÐ¶Ï
#define NRF_RX_OK           0x40  //½ÓÊÕµ½Êý¾ÝÖÐ¶Ï
#define NRF_RX_P_NO_MASK    (0x07<<1)
#define NRF_TX_FULL         0x01
// OBSERVE_TX
#define  NRF_PLOS_CNT       0xF0
#define  NRF_ARC_CNT        0x0F
// FIFO_STATUS
#define  NRF_FIFO_RX_EMPTY  0x01
#define  NRF_FIFO_RX_FULL   0x02
#define  NRF_FIFO_TX_EMPTY  0x10
#define  NRF_FIFO_TX_FULL   0x20
#define  NRF_FIFO_TX_REUSE  0x40
// DYNPD
#define  NRF_DPL_P0         0x01
#define  NRF_DPL_P1         0x02
#define  NRF_DPL_P2         0x04
#define  NRF_DPL_P3         0x08
#define  NRF_DPL_P4         0x10
#define  NRF_DPL_P5         0x20
// FEATURE
#define  NRF_EN_DYN_ACK         0x01
#define  NRF_EN_ACK_PAYLOAD     0x02
#define  NRD_EN_DPL             0x04

#ifndef  NRF_CE_SET
#define  NRF_CE_SET
#endif
#ifndef  NRF_CE_RESET
#define  NRF_CE_RESET
#endif
#ifndef  NRF_CS_SET
#define  NRF_CS_SET
#endif
#ifndef  NRF_CS_RESET
#define  NRF_CS_RESET
#endif

#define  NRF_CE_ENABLE      NRF_CE_SET
#define  NRF_CE_DISABLE     NRF_CE_RESET
#define  NRF_CS_ENABLE      NRF_CS_RESET
#define  NRF_CS_DISABLE     NRF_CS_SET
#define  NRF_READY          (NRF_IRQ == 0)

#define  NRF_MAX_CHANNEL    127

void nrf_init(void);
uint8_t nrf_detect(void);
uint8_t nrf_write_reg(uint8_t reg,uint8_t value);
uint8_t nrf_read_reg(uint8_t reg);
uint8_t nrf_read_buf(uint8_t reg,uint8_t *pBuf,uint32_t len);
uint8_t nrf_write_buf(uint8_t reg, const uint8_t *pBuf, uint32_t len);

#endif
#endif /* NRF24L01_H_ */
