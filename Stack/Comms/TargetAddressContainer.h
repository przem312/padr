/*
 * TargetAddressContainer.h
 *
 *  Created on: Dec 19, 2013
 *      Author: PSokolo
 */

#ifndef TARGETADDRESSCONTAINER_H_
#define TARGETADDRESSCONTAINER_H_
#include <new>
#include <cstring>
#include "Types.h"
#include <stdio.h>
typedef union 
{
  uint32_t addressHolder;
  char pArrayHolder[sizeof(uint32_t)];
}Address;
/*
 * Basic address container
 * TODO Mechanism for proper handling addresses (this accepts only the ones written in 4 char array.
 */

class TargetAddressContainer {
public:
	TargetAddressContainer()
	{
		this->m_Address.addressHolder = 0;
	}

	TargetAddressContainer(uint32_t address)
	{
		this->m_Address.addressHolder = address;
	}

	TargetAddressContainer(const char* pAddrArray)
	{
          std::memcpy(m_Address.pArrayHolder,pAddrArray,4);
	}

	TargetAddressContainer(const TargetAddressContainer& rCopy)
	{
		this->m_Address.addressHolder = rCopy.GetAddressUint32();
	}
	TargetAddressContainer& operator=(const TargetAddressContainer& rCopy)
	{
		this->m_Address.addressHolder = rCopy.GetAddressUint32();
		return *this;
	}

	uint32_t GetAddressUint32() const
	{
		return m_Address.addressHolder;
	}

	const char* GetAddressByteArray() const
	{
		return m_Address.pArrayHolder;
	}
#if defined(DEBUG_VER) || 1
	char* GetAddressAsString() const
	{
        const char* addressArray = GetAddressByteArray();
	    static char returnString[sizeof("255.255.255.255")] = {0};
		sprintf(returnString, "%d.%d.%d.%d", (uint8_t)addressArray[0], (uint8_t)addressArray[1], (uint8_t)addressArray[2], (uint8_t)addressArray[3]);
	    return returnString;
	}
#endif
	void Reset()
	{
		m_Address.addressHolder=0;
	}
private:
	Address m_Address;
};

#endif /* TARGETADDRESSCONTAINER_H_ */
