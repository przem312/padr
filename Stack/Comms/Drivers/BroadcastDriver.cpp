/*
 * BroadcastDriver.cpp
 *
 *  Created on: Jan 14, 2014
 *      Author: przem312
 */

#include "BroadcastDriver.h"
#include "EthernetDriver.h"
#include "NRF24L01Driver.h"
#include "Device.h"
#include <cstdio>
namespace Communication {

    BroadcastDriver* BroadcastDriver::GetInstance()
    {
        static BroadcastDriver instance;
        return &instance;
    }

    void BroadcastDriver::Open()
    {
        // Nothing to do
    }

    void BroadcastDriver::Close()
    {
        // Nothing to do
    }

	char* BroadcastDriver::Read(DriverHandler& handler, int& retVal)
    {
        return NULL;
    }

    int BroadcastDriver::Write(DriverHandler handler,const char* pSource, size_t sourceSize)
    {
#if USE_ETHERNET_DRIVER==1
        int ethernetRetVal = EthernetDriver::GetInstance()->Write(handler, pSource, sourceSize);
        if(ethernetRetVal<0)
        {
        	return ethernetRetVal;
        }
#endif
#if USE_RADIO_DRIVER==1
        int NRF24L01RetVal = NRF24L01Driver::GetInstance()->Write(handler, pSource, sourceSize);
        if(NRF24L01RetVal<0)
        {
        	return NRF24L01RetVal;
        }
#endif
#if 0
//        size_t UartRetVal=UartDriver::GetInstance().Write(handler, pSource, sourceSize);
#endif
        return sourceSize;/*+ (sourceSize-UartRetVal);*/
    }

	int BroadcastDriver::IOCTL(DriverCmd cmd, char* pBuffer, size_t bufferSize)
	{
		return 0;
	}

    BroadcastDriver::BroadcastDriver()
      :CommDriver()
    {
        // TODO Auto-generated constructor stub

    }

    BroadcastDriver::~BroadcastDriver()
    {
        // TODO Auto-generated destructor stub
    }
}
