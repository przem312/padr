/*
 * BroadcastDriver.h
 *
 *  Created on: Jan 14, 2014
 *      Author: przem312
 */

#ifndef BROADCASTDRIVER_H_
#define BROADCASTDRIVER_H_

#include "CommDriver.h"

namespace Communication {
/*
 *
 */


    class BroadcastDriver : public CommDriver
    {
    public:
        static BroadcastDriver* GetInstance();
        virtual void Open();
        virtual void Close();
        virtual char* Read(DriverHandler& handler, int& retVal);
        virtual int Write(DriverHandler handler,const char* pSource, const size_t sourceSize);
		virtual int IOCTL(DriverCmd cmd, char* pBuffer, size_t bufferSize);
        virtual DriverHandler isReadAvailable()
        {
          return -1;
        };
        BroadcastDriver();
        virtual ~BroadcastDriver();
private:
        virtual void readImpl(){}
    };
}
#endif /* BROADCASTDRIVER_H_ */
