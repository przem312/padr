/*
 * DriverBufferPool.h
 *
 *  Created on: 20 gru 2014
 *      Author: przem312
 */

#ifndef DRIVERBUFFERPOOL_H_
#define DRIVERBUFFERPOOL_H_
#include "DriverBuffers.h"
#include "ObjectPool.h"
namespace Communication
{
template <size_t numberOfBuffers> class DriverBufferPool
	: public ObjectPool <DriverBuffer, numberOfBuffers>
{
public:
	DriverBufferPool()
    {
        for (int i = 0; i<numberOfBuffers; i++)
        {

		//	this->AddObj(&m_buffers[i]);
        }
    }
    virtual ~DriverBufferPool()
    {
        for (int i = 0; i<numberOfBuffers; i++)
        {
      //      this->RemoveObject(&m_buffers[i]);
        }
    }

private:
    //DriverBuffer m_buffers[numberOfBuffers];
};
}


#endif /* DRIVERBUFFERPOOL_H_ */
