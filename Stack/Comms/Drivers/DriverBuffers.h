/*
 * DriverBuffers.h
 *
 *  Created on: 18 gru 2014
 *      Author: przem312
 */

#ifndef DRIVERBUFFERS_H_
#define DRIVERBUFFERS_H_
#include "Buffer.hpp"
#include "Device.h"

namespace Communication {
extern DriverHandler handlerStatic;

class DriverBuffer :public ListNode{
public:
	DriverBuffer():
		m_Handler(handlerStatic++)
	{
		ClearBuffer();
	};
	~DriverBuffer()
	{
		handlerStatic--;
		ClearBuffer();
	}

	void ClearBuffer()
	{
		std::memset(m_Buffer.GetBufferStart(),'\0',m_Buffer.GetBufferSize());
		m_AddressHolder.Reset();
	}
	TargetAddressContainer* getAddressHolder() {
		return &m_AddressHolder;
	}

	void setAddressHolder(const TargetAddressContainer& addressHolder) {
		m_AddressHolder = addressHolder;
	}

	BufferImpl<MAX_MSG_SIZE>& getBuffer() {
		return m_Buffer;
	}

	DriverHandler getHandler() const {
		return m_Handler;
	}
	void setHandler(DriverHandler handler)
	{
		m_Handler = handler;
	}
private:
	DriverHandler m_Handler;
	TargetAddressContainer m_AddressHolder;
	BufferImpl<MAX_MSG_SIZE> m_Buffer;

};

} /* namespace Communication */

#endif /* DRIVERBUFFERS_H_ */
