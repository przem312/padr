/*
 * EthernetDriver.cpp
 *
 *  Created on: Dec 11, 2013
 *      Author: przem312
 */


#include "EthernetDriver.h"

#include "Console.h"
#include "Device.h"
#if LWIP_TODO
#include <lwip/sockets.h>

#include <lwip/ping.h>
#include <lwip/tcpip.h>
#include "stm32f4x7_eth.h"
#include "stm32f4x7_eth_bsp.h"
#include <lwip/init.h>
#include "lwip/memp.h"
#include "lwIP/tcp.h"
#include "lwIP/udp.h"
#include "lwIP/tcpip.h"
#include "lwIP/api.h"
#include "netif/etharp.h"
#include "lwIP/dhcp.h"
#include "ethernetif.h"

#include "arch/sys_arch.h"
#include <stdio.h>

namespace Communication
	{
	namespace
	{
	typedef int SOCKET;
	class LwipSocket
	{
	public:
		static const uint16_t STACK_PORT=1337;
		bool Init();
		bool SendData(const char* pAddress, char* pData, size_t size);
		bool ReceiveData(char* pData, int& rSize);
		LwipSocket(size_t bufferSize)
		{
			m_SocketName = new char[sizeof(m_DeviceAddress)];
			std::memset((void* )&m_DeviceAddress,'\0',sizeof(m_DeviceAddress));
			std::memset((void* )&m_SocketName,'\0',sizeof(m_DeviceAddress));
			m_Socket = -1;
			m_BufferSize = bufferSize;
		}
		~LwipSocket()
		{
			close(m_Socket);

		}



	private:
		SOCKET m_Socket;
		struct sockaddr_in m_DeviceAddress;
		char* m_SocketName;
		size_t m_BufferSize;
		static struct netif netif;
	};
	struct netif LwipSocket::netif;
	bool LwipSocket::Init()
	{
		/* configure Ethernet (GPIOs, clocks, MAC, DMA) */
		ETH_BSP_Config();
		struct ip_addr ipaddr;
		struct ip_addr multiIpAddr;
		struct ip_addr netmask;
		struct ip_addr gw;
		uint8_t* macaddress=(uint8_t*)Device::GetMacAddress();

		sys_sem_t sem;

		sys_init();

		/* Initializes the dynamic memory heap defined by MEM_SIZE.*/
		mem_init();

		/* Initializes the memory pools defined by MEMP_NUM_x.*/
		memp_init();

		pbuf_init();
		netif_init();
		/* Initilaize the LwIP stack */
		//                lwip_init();
		tcpip_init(NULL,NULL);
		char* pIp = Device::GetDeviceIp();
		char pMulcitcastIp = Device::GetBroadcastArray();
		IP4_ADDR(&ipaddr, pIp[0], pIp[1], pIp[2], pIp[3]);
		IP4_ADDR(&multiIpAddr, pMulcitcastIp[0], pMulcitcastIp[1], pMulcitcastIp[2], pMulcitcastIp[3]);
		IP4_ADDR(&netmask, 255, 255, 255, 0);
		IP4_ADDR(&gw, pIp[0], pIp[1], pIp[2], 1);

		netif_add(&netif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &tcpip_input);
		netconn_join_leave_group()
		ASSERT(0 < )
		netif_set_default(&netif);

	#if LWIP_DHCP
	dhcp_start(&netif);
	#endif
	netif_set_up(&netif);
	ping_init();
	m_Socket = socket(PF_INET, SOCK_DGRAM, 17);
	if( m_Socket == -1 )
	{
		return false;
	}
	char* pDeviceIp = Device::GetDeviceIp();


	m_DeviceAddress.sin_family = AF_INET;
	m_DeviceAddress.sin_port = htons(STACK_PORT);
	char* deviceAddress = Device::GetDeviceIp();
	m_DeviceAddress.sin_addr.s_addr = htonl(((u32_t)(deviceAddress[0] & 0xff) << 24) | ((u32_t)(deviceAddress[1] & 0xff) << 16) | \
			((u32_t)(deviceAddress[2] & 0xff) << 8) | (u32_t)(deviceAddress[3] & 0xff));
	//		m_DeviceAddress.sin_addr.S_un.S_un_b.s_b1 = htons(deviceAddress[0]);
	//		m_DeviceAddress.sin_addr.S_un.S_un_b.s_b2 = htons(deviceAddress[1]);
	//		m_DeviceAddress.sin_addr.S_un.S_un_b.s_b3 = htons(deviceAddress[2]);
	//		m_DeviceAddress.sin_addr.S_un.S_un_b.s_b4 = htons(deviceAddress[3]);
	//m_DeviceAddress.sin_addr.S_un.S_addr = INADDR_ANY;
	if(bind(m_Socket,
			(struct sockaddr *)&m_DeviceAddress,
			sizeof(m_DeviceAddress)) == -1)
	{
		printf("Bind failed with ");
		close(m_Socket);
		return false;
	}
	DEBUG("SOCKET INITIALIZED!");
	return true;
	}

	bool LwipSocket::ReceiveData(char* pData, int& rSize)
	{
		struct sockaddr_in receiveAddress;
		bool retVal = false;
		int value = sizeof(receiveAddress);
		std::memset(&receiveAddress,'\0',sizeof(receiveAddress));
		rSize = recvfrom(m_Socket,pData+4,m_BufferSize,0, (struct sockaddr*)&receiveAddress,(u32_t*)&value);
		if(rSize<0 || value<0)
		{
			//			printf("recvfrom failed with error code : %d" , WSAGetLastError());
			rSize=0;
			std::memset(pData, '\0',m_BufferSize);
		}
		else
		{
			//              TODO
			//			pData[0] = receiveAddress.sin_addr.S_un.S_un_b.s_b1;
			//			pData[1] = receiveAddress.sin_addr.S_un.S_un_b.s_b2;
			//			pData[2] = receiveAddress.sin_addr.S_un.S_un_b.s_b3;
			//			pData[3] = receiveAddress.sin_addr.S_un.S_un_b.s_b4;
		}
		return rSize != 0;
	}

	bool LwipSocket::SendData(const char* pAddress, char* pData, size_t size)
	{
		struct sockaddr_in sendAddress;
		std::memset(&sendAddress,'\0',sizeof(sendAddress));
		bool retVal = false;
		int value = sizeof(sendAddress);
		sendAddress.sin_addr.s_addr = htonl(((u32_t)(pAddress[0] & 0xff) << 24) | ((u32_t)(pAddress[1] & 0xff) << 16) | \
				((u32_t)(pAddress[2] & 0xff) << 8) | (u32_t)(1 & 0xff));

		sendAddress.sin_family = AF_INET;
		sendAddress.sin_port = htons(STACK_PORT);
		retVal = sendto(m_Socket,pData,size,0,(struct sockaddr*) &sendAddress,sizeof(sendAddress)) != -1;
		if(retVal == true)
		{
			//			DEBUG("WindowsSocket: successfully sent %d bytes to address: %d.%d.%d.%d",
			//					size,
			//					sendAddress.sin_addr.S_un.S_un_b.s_b1,
			//					sendAddress.sin_addr.S_un.S_un_b.s_b2,
			//					sendAddress.sin_addr.S_un.S_un_b.s_b3,
			//					sendAddress.sin_addr.S_un.S_un_b.s_b4);
		}
		else
		{
			//			printf("sendto failed with error code : %d" , WSAGetLastError());
		}
		return retVal;
	}

	};

	static LwipSocket* EthernetSocket;
#endif
	/*
	 * EthernetDriver.cpp
	 *
	 *  Created on: Dec 11, 2013
	 *      Author: przem312
	 */

	#include "SocketImpl/SocketImpl.hpp"
	#include "Device.h"
	namespace Communication
	{
	    EthernetDriver* EthernetDriver::GetInstance()
	    {
	        static EthernetDriver instance;
	        return &instance;
	    }
	    EthernetDriver::EthernetDriver()
	    :m_isInit(false),
	     m_pSocket(new SocketImpl(MAX_MESSAGE_SIZE,Device::GetBroadcastArray()))
	    {
	        // TODO Auto-generated constructor stub
	    }

	    EthernetDriver::~EthernetDriver()
	    {
	    	delete m_pSocket;
	        // TODO Auto-generated destructor stub
	    }


	    int EthernetDriver::Write(DriverHandler handler, const char* pSource, const size_t sourceSize)
	    {
			m_Mutex.Take();
			size_t retVal = -1;
			TargetAddressContainer* adress = GetAddress(handler);
			
			if (adress != NULL && m_pSocket->SendData(adress->GetAddressAsString(), pSource, sourceSize))
			{
				retVal = sourceSize;

			//	DEBUG("EthernetDriver: Written %d bytes out of %d to %s", retVal, sourceSize, GetAddress(handler)->GetAddressAsString());
			}
			m_Mutex.Release();
	        return retVal;
	    }

	    int EthernetDriver::IOCTL(DriverCmd cmd, char* pBuffer, size_t bufferSize)
	    {
	        size_t retVal = 0;
	        DEBUG("EthernetDriver: IOCTL %d with %s, size: %d", cmd, pBuffer, bufferSize);
	        return retVal;
	    }

	    void EthernetDriver::Open()
	    {
			ASSERT(m_pSocket->Init());
	    }
		void EthernetDriver::Close()
		{

		}
	    DriverHandler EthernetDriver::isReadAvailable()
	    {
	    	m_Mutex.Take();
	    	DriverBuffer* pBuffer = m_BufferPool.AcquireObject();
	    	m_Mutex.Release();
	    	if(NULL == pBuffer)
	    	{
	    		return -1;
	    	}
			DriverHandler retVal = pBuffer->getHandler();
			int size = 0;
			const bool readResult = m_pSocket->ReceiveData(pBuffer->getBuffer().GetBufferStart(),
				pBuffer->getBuffer().GetBufferSize(),
				pBuffer->getAddressHolder(),
				size);
			m_Mutex.Take();
			if (!readResult)
			{
				m_BufferPool.ReleaseObject(pBuffer);
				retVal = -1;
			}
			
			else
			{
				m_BufferList.AddNode(pBuffer);
			}
			m_Mutex.Release();
			return retVal;
	    }
	}
