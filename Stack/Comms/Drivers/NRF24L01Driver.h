/*
 * NRF24L01Driver.h
 *
 *  Created on: Dec 25, 2013
 *      Author: przem312
 */

#ifndef NRF24L01DRIVER_H_
#define NRF24L01DRIVER_H_

/*
 *
 */
#include "CommDriver.h"
#include <MessageBase.h>
namespace Communication
{
    class NRF24L01Driver : public CommDriver
    {
    public:
        static NRF24L01Driver* GetInstance();
        virtual void Open();
        virtual int Write(DriverHandler handler,const char* pSource, const size_t sourceSize);
        virtual int IOCTL(DriverCmd cmd, char* pBuffer, size_t bufferSize);
        NRF24L01Driver();
        virtual ~NRF24L01Driver();
        virtual DriverHandler isReadAvailable();
        virtual bool OnReceive()
        {
            readAvailable=true;
        }
    private:
        MessageHeader BuildHeader(DriverHandler handler);
        
        void sendAck();
        typedef union
        {
            struct ackMsg : public MessageBody
            {
            	ackMsg()
            	{
            		SetMsgType(ACK_MSG);
            		SetMessageSize(sizeof( MessageBody));
            	}

            };
            char m_buffer[32];
        } AckMessage;
        bool IsReceiver();
        bool readAvailable;
        uint32_t currentMsgId;
    };

}
#endif /* NRF24L01DRIVER_H_ */
