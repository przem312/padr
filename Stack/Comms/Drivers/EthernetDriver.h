/*
 * EthernetDriver.h
 *
 *  Created on: Dec 11, 2013
 *      Author: przem312
 */

#ifndef ETHERNETDRIVER_H_
#define ETHERNETDRIVER_H_

/*
 *
 */
#include "CommDriver.h"
namespace Communication
{
class SocketImpl;
    class EthernetDriver : public Communication::CommDriver
    {
    public:
        static EthernetDriver* GetInstance();
        virtual void Open();
        virtual void Close();
		// Read is Buffer-less. must only set pointer to buffer.
		// TODO: COMMDRIVER shall have this method only.
        virtual int Write(DriverHandler handler,const char* pSource, const size_t sourceSize);
        virtual int IOCTL(DriverCmd cmd, char* pBuffer, size_t bufferSize);
        virtual int isReadAvailable();
    private:
        EthernetDriver();
        virtual ~EthernetDriver();
        static const uint16_t PORT_NUMBER = 1337;
        static const uint8_t UPD_HEADER_SIZE = 42;
        static const char threadName[];
        SocketImpl* m_pSocket;
        bool m_isInit;
    };
}
#endif /* ETHERNETDRIVER_H_ */
