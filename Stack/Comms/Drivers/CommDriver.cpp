#include "CommDriver.h"

namespace Communication
{
	Mutex CommDriver::m_Mutex;
	LinkedList CommDriver::m_BufferList;
	DriverBufferPool<MAX_CONNECTIONS_COUNT+1> CommDriver::m_BufferPool;
	DriverHandler handlerStatic=0;

DriverHandler CommDriver::PushHandler(const TargetAddressContainer* pContainer)
{
	if (false == m_Mutex.Take())
	{
		return -1;
	}
	DriverBuffer* pBuffer = m_BufferPool.AcquireObject();
	if(NULL == pBuffer)
	{
		return -1;
	}
	pBuffer->setAddressHolder(*pContainer);
	m_BufferList.AddNode(pBuffer);
	m_Mutex.Release();
	return pBuffer->getHandler();
}

TargetAddressContainer* CommDriver::GetAddress(DriverHandler handlerNo)
{
	DriverBuffer* pBuffer = GetBufferObj(handlerNo);
	if(NULL == pBuffer)
	{
		return NULL;
	}
	return pBuffer->getAddressHolder();

}

char* CommDriver::GetBuffer(DriverHandler handlerNo, int& bufferSize)
{

	DriverBuffer* pBuffer = GetBufferObj(handlerNo);
	if(NULL == pBuffer)
	{
		return NULL;
	}

	bufferSize = pBuffer->getBuffer().GetBufferSize();
	return pBuffer->getBuffer().GetBufferStart();
}

DriverBuffer* CommDriver::GetBufferObj(DriverHandler handler)
{
	DriverBuffer* pBuffer = (DriverBuffer*)m_BufferList.GetFirst();
	while(pBuffer)
	{
		if(pBuffer->getHandler() == handler)
		{
			m_Mutex.Release();
			return pBuffer;
		}
		pBuffer = (DriverBuffer*)m_BufferList.GetNext();
	}
	return NULL;
}


char* CommDriver::Read(DriverHandler& handler, int& retVal)
{
//		DEBUG("EthernetDriver: Read %d bytes from %s", retVal, GetAddress(handler)->GetAddressAsString());

    return GetBuffer(handler,retVal);
}

}

