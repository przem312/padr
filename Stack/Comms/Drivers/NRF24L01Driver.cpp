/* NRF24L01Driver.cpp
  *
 *
 *  Created on: Dec 25, 2013
 *      Author: przem312
 */

#include <string.h>
#include "NRF24L01Driver.h"
//#include <nRF24L01P.h>
#include <Device.h>
#include "Console.h"
#include "MessageBase.h"
//#include "spi.h"
static char dumbBuffer[32] = {};
namespace Communication
{
    void NRF24L01Driver::Open()
    {
      
    }

    NRF24L01Driver* NRF24L01Driver::GetInstance()
    {
        static NRF24L01Driver instance;
        return &instance;
    }

    NRF24L01Driver::NRF24L01Driver()
    //:m_Semaphore(false),
      :readAvailable(false)
//       SpiPort(SPI1)
    {
        // TODO Auto-generated constructor stub

    }

    NRF24L01Driver::~NRF24L01Driver()
    {
        // TODO Auto-generated destructor stub
    }

    int NRF24L01Driver::Write(DriverHandler handler, const char* pSource, const size_t sourceSize)
    {
        MessageHeader header = BuildHeader(handler);
        MessageBase* pBase = (MessageBase*) pSource;
        Message message(header,*pBase);
        if(!message.Validate())
        {
            return -1;
        }
        std::memcpy(pSource,&message,sourceSize);
        readAvailable = true;
        DEBUG("NRF24L01Driver: Written %d bytes out of %d to %s", retVal, sourceSize, GetAddress(handler)->GetAddressAsString());
        return retVal;
    }

    DriverHandler NRF24L01Driver::isReadAvailable()
    {
        //return nRF24L01_isReadAvailable() == 1;
        return -1;
    }

}
