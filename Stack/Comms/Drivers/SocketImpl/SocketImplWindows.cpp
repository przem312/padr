/*
 * SocketImplWindows.cpp
 *
 *  Created on: 7 lut 2015
 *      Author: przem312
 */



#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>

#include "SocketImpl.hpp"
#include "Device.h"
#include "Console.h"
#include <string.h>
#include <Thread.h>
#include "TargetAddressContainer.h"

#include "AssertMacro.h"
#pragma comment(lib, "Ws2_32.lib")
namespace Communication {
	struct SocketInternals
	{
		SOCKET m_Socket;
		struct sockaddr_in m_DeviceAddress;
		struct sockaddr_in m_BroadcastAddr;
	};

		SocketImpl::SocketImpl(uint32_t bufferSize, char* broadCastIp)
		:m_BufferSize(bufferSize)
		{
			m_pInnerStuff = new SocketInternals;
			std::memset(&m_pInnerStuff->m_DeviceAddress,'\0',sizeof(sockaddr_in));
			std::memset(&m_pInnerStuff->m_BroadcastAddr,'\0',sizeof(sockaddr_in));
			m_pInnerStuff->m_Socket = INVALID_SOCKET;

			m_pInnerStuff->m_BroadcastAddr.sin_addr.S_un.S_un_b.s_b1 = broadCastIp[0];
			m_pInnerStuff->m_BroadcastAddr.sin_addr.S_un.S_un_b.s_b2 = broadCastIp[1];
			m_pInnerStuff->m_BroadcastAddr.sin_addr.S_un.S_un_b.s_b3 = broadCastIp[2];
			m_pInnerStuff->m_BroadcastAddr.sin_addr.S_un.S_un_b.s_b4 = broadCastIp[3];
			WSADATA wsa;
			ASSERT(WSAStartup(MAKEWORD(1,1),&wsa) == 0);
		}
		SocketImpl::~SocketImpl()
		{
			
			struct ip_mreq mreq;
setsockopt(
    m_pInnerStuff->m_Socket, 
    IPPROTO_IP, 
    IP_DROP_MEMBERSHIP,
    (char*)&mreq,
    sizeof(mreq));
  shutdown (m_pInnerStuff->m_Socket, 0x00);

closesocket(m_pInnerStuff->m_Socket);
			WSACleanup();
		}



	bool SocketImpl::Init()
	{
		std::memset(&m_pInnerStuff->m_Socket,'\0',sizeof(SOCKET));
		std::memset(&m_pInnerStuff->m_DeviceAddress,'\0',sizeof(sockaddr_in));
		m_pInnerStuff->m_Socket = socket(AF_INET, SOCK_DGRAM, 0);

		m_pInnerStuff->m_BroadcastAddr.sin_family = AF_INET;
		m_pInnerStuff->m_DeviceAddress.sin_port = htons(STACK_PORT);

		struct ip_mreq imr;
		imr.imr_multiaddr.S_un.S_addr = inet_addr("224.0.2.123");
		imr.imr_interface.s_addr = inet_addr("192.168.56.1");
		imr.imr_interface.s_addr = htonl(INADDR_ANY);

		if( m_pInnerStuff->m_Socket == INVALID_SOCKET )
		{
			return false;
		}

		char* deviceAddress = Device::GetDeviceIp();
		m_pInnerStuff->m_DeviceAddress.sin_family = AF_INET;
		m_pInnerStuff->m_DeviceAddress.sin_port = htons(STACK_PORT);
		m_pInnerStuff->m_DeviceAddress.sin_addr.s_addr = htonl(INADDR_ANY);
		/*BOOL bOptVal = TRUE;
		if(0 != setsockopt(m_pInnerStuff->m_Socket, SOL_SOCKET, SO_REUSEADDR, (char *)&bOptVal, sizeof(bOptVal)))
		{
			printf("\n SO_REUSEADDR failed with error code : %d", WSAGetLastError());
			closesocket(m_pInnerStuff->m_Socket);
			WSACleanup();
			return false;
		}*/
		//m_DeviceAddress.sin_addr.S_un.S_addr = INADDR_ANY;
		if(bind(m_pInnerStuff->m_Socket,
				(struct sockaddr *)&m_pInnerStuff->m_DeviceAddress,
				sizeof(m_pInnerStuff->m_DeviceAddress)) == -1)
		{
			printf("\n Bind failed with error code : %d" , WSAGetLastError());
			closesocket(m_pInnerStuff->m_Socket);
			WSACleanup();
			return false;
		}
		//int ttl = 3;
		/*if (0 != setsockopt(m_pInnerStuff->m_Socket, IPPROTO_IP, IP_MULTICAST_TTL, (char *)&ttl, sizeof(ttl)))
		{
			printf("\n setsockopt IP_MULTICAST_TTL failed with error code : %d", WSAGetLastError());
			closesocket(m_pInnerStuff->m_Socket);
			WSACleanup();
			return false;
		}*/
		/*unsigned long address = m_pInnerStuff->m_DeviceAddress.sin_addr.S_un.S_addr;
		if (0 != setsockopt(
			m_pInnerStuff->m_Socket,
			IPPROTO_IP,
			IP_MULTICAST_IF,
			(char *)&address,
			sizeof(address)))
		{
			printf("\n setsockopt IP_MULTICAST_IF failed with error code : %d", WSAGetLastError());
			closesocket(m_pInnerStuff->m_Socket);
			WSACleanup();
			return false;
		}*/

		if (0 != setsockopt(m_pInnerStuff->m_Socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char FAR	*)&imr, sizeof(imr)))
		{
			printf("\n setsockopt IP_ADD_MEMBERSHIP failed with error code : %d", WSAGetLastError());
			closesocket(m_pInnerStuff->m_Socket);
			WSACleanup();
			return false;
		}
		int optval = 8;
		
		if(0 != setsockopt(m_pInnerStuff->m_Socket, IPPROTO_IP, IP_MULTICAST_TTL, (char*)&optval, sizeof(int)))
		{
			printf("\n IP_MULTICAST_TTL failed with error code : %d", WSAGetLastError());
			closesocket(m_pInnerStuff->m_Socket);
			WSACleanup();
			return false;
		}

		int timeout = 1000; // 1 sec
		/*if( 0!= setsockopt(m_pInnerStuff->m_Socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)))
		{
			printf("\n SO_RCVTIMEO failed with error code : %d", WSAGetLastError());
			closesocket(m_pInnerStuff->m_Socket);
			WSACleanup();
			return false;
		}*/
		DEBUG("SOCKET INITIALIZED!");
		return true;
	}

	bool SocketImpl::ReceiveData(char* pData, uint32_t bufferSize,TargetAddressContainer* pContainer, int& rSize)
	{
		struct sockaddr_in receiveAddress;
		bool retVal = false;
		int value = sizeof(receiveAddress);
		std::memset(&receiveAddress,'\0',sizeof(receiveAddress));
		rSize = recvfrom(m_pInnerStuff->m_Socket, pData, bufferSize, 0, (struct sockaddr FAR*)&receiveAddress,&value);
		if(rSize<0 || value<0)
		{
		printf("recvfrom failed with error code : %d" , WSAGetLastError());
		rSize=0;
		std::memset(pData, '\0',m_BufferSize);
		}
		else
		{
			pContainer->Reset();
			*pContainer = TargetAddressContainer(receiveAddress.sin_addr.S_un.S_addr);
		}
		return rSize != 0;
	}

	bool SocketImpl::SendData(const char* pAddress, const char* pData, size_t size)
	{
		struct sockaddr_in sendAddress;
		std::memset(&sendAddress,'\0',sizeof(sendAddress));
		bool retVal = false;
		int value = sizeof(sendAddress);
#if 1
		sendAddress.sin_addr.s_addr = inet_addr(pAddress);
#elif 0
		sendAddress.sin_addr.S_un.S_un_b.s_b1 = 127;
		sendAddress.sin_addr.S_un.S_un_b.s_b2 = 0;
		sendAddress.sin_addr.S_un.S_un_b.s_b3 = 0;
		sendAddress.sin_addr.S_un.S_un_b.s_b4 = 1;
#else

#endif
		sendAddress.sin_family = AF_INET;
		sendAddress.sin_port = htons(STACK_PORT);
		retVal = sendto(m_pInnerStuff->m_Socket,pData,size,0,(struct sockaddr*) &sendAddress,sizeof(sendAddress)) != 0;
		if(retVal == true)
		{
			//DEBUG("WindowsSocket: successfully sent %d bytes to address: %d.%d.%d.%d",
			//		size,
			//		sendAddress.sin_addr.S_un.S_un_b.s_b1,
			//		sendAddress.sin_addr.S_un.S_un_b.s_b2,
			//		sendAddress.sin_addr.S_un.S_un_b.s_b3,
			//		sendAddress.sin_addr.S_un.S_un_b.s_b4);
		}
		else
		{
			printf("sendto failed with error code : %d" , WSAGetLastError());
		}
		return retVal;
	}

};


