#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include "SocketImpl.hpp"
#include "Device.h"
#include "Console.h"
#include <string.h>
#include <errno.h>
#include "TargetAddressContainer.h"

#include "AssertMacro.h"
#include "../../TargetAddressContainer.h"
#include "../../../Utils/Device/Device.h"

namespace Communication {

static const int INVALID_SOCKET = -1;
struct SocketInternals
{
	int m_Socket;
	struct sockaddr_in m_DeviceAddress;
	char* m_SocketName;
	in_addr GetInterfaceAddress(const char* iface)
	{
		int fd=socket(AF_INET, SOCK_DGRAM, 0);
		struct ifreq ifr;

		//Copy the interface name in the ifreq structure
		strncpy(ifr.ifr_name , iface , IFNAMSIZ-1);
		//Type of address to retrieve - IPv4 IP address
		ifr.ifr_addr.sa_family = AF_INET;
		ioctl(fd, SIOCGIFADDR, &ifr);
		close(fd);

		//display result
		return ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr;
	}
	bool SetUpMulticast(const char* pAddrString, const char* iface)
	{
		struct ip_mreq imr;
		unsigned char ttl = 3;
		imr.imr_multiaddr.s_addr = inet_addr(pAddrString);
		imr.imr_interface.s_addr =  inet_addr(inet_ntoa(GetInterfaceAddress(iface)));
		TargetAddressContainer container(imr.imr_interface.s_addr);
		Device::SetDeviceIp(container.GetAddressByteArray());
		setsockopt(m_Socket, IPPROTO_IP, IP_MULTICAST_TTL, &ttl,
				sizeof(unsigned char));

		u_char loop = 0;
		setsockopt(m_Socket, IPPROTO_IP, IP_MULTICAST_LOOP, &loop, sizeof(loop));

		if(-1 == setsockopt (m_Socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, &imr, sizeof(imr)))
		{
			DEBUG("IP_ADD_MEMBERSHIP FAILED: %s", strerror(errno));
			close(m_Socket);
			return false;
		}
		return true;

	}
};
SocketImpl::SocketImpl(uint32_t bufferSize, char* broadCastIp)
:m_BufferSize(bufferSize)
{
	m_pInnerStuff = new SocketInternals;
	m_pInnerStuff->m_SocketName = new char[sizeof(sockaddr_in)];
	std::memset(&m_pInnerStuff->m_DeviceAddress,'\0',sizeof(sockaddr_in));
	std::memset(&m_pInnerStuff->m_SocketName,'\0',sizeof(sockaddr_in));
	m_pInnerStuff->m_Socket = INVALID_SOCKET;

}

SocketImpl::~SocketImpl()
{
	close(m_pInnerStuff->m_Socket);
}

bool SocketImpl::Init()
{
	std::memset(&m_pInnerStuff->m_Socket,'\0',sizeof(m_pInnerStuff->m_Socket));
	std::memset(&m_pInnerStuff->m_DeviceAddress,'\0',sizeof(sockaddr_in));
	m_pInnerStuff->m_Socket = socket(AF_INET, SOCK_DGRAM, 0);


	if( m_pInnerStuff->m_Socket == -1 )
	{
		DEBUG("INVALID_SOCKET %d", m_pInnerStuff->m_Socket);
		return false;
	}

	const char* iface = Device::getUsedPort();

	struct ifreq ifr;
	setsockopt(m_pInnerStuff->m_Socket, SOL_SOCKET, SO_BINDTODEVICE, &ifr, sizeof(ifr));

	m_pInnerStuff->m_DeviceAddress.sin_family = AF_INET;
	m_pInnerStuff->m_DeviceAddress.sin_port = htons(STACK_PORT);
	m_pInnerStuff->m_DeviceAddress.sin_addr.s_addr = htonl(INADDR_ANY);

	int reuse = 1;

	if(setsockopt(m_pInnerStuff->m_Socket, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse)) < 0)
	{
		DEBUG("SO_REUSEADDR FAILED with: %s", strerror(errno));
		close(m_pInnerStuff->m_Socket);
		return false;
	}

	if(bind(m_pInnerStuff->m_Socket,
			(struct sockaddr *)&m_pInnerStuff->m_DeviceAddress,
			sizeof(m_pInnerStuff->m_DeviceAddress)) == -1)
	{
		DEBUG("BhahaIND FAILED: %d", errno);
		close(m_pInnerStuff->m_Socket);
		return false;
	}

	DEBUG("SOCKET INITIALIZED!");
	return m_pInnerStuff->SetUpMulticast("224.0.2.123",iface);
}

bool SocketImpl::ReceiveData(char* pData, uint32_t bufferSize,TargetAddressContainer* pContainer, int& rSize)
{

	struct sockaddr receiveAddress;
	bool retVal = false;
	socklen_t sendsize = sizeof(receiveAddress);
	std::memset(&receiveAddress,'\0',sizeof(receiveAddress));
	rSize = recvfrom(m_pInnerStuff->m_Socket, pData, bufferSize, 0, &receiveAddress,&sendsize);
	if(rSize<0)
	{
		DEBUG("RECV failed");
		rSize=0;
		std::memset(pData, '\0',m_BufferSize);
	}
	else
	{
		DEBUG("RECEIVED");
		pContainer->Reset();
		*pContainer = TargetAddressContainer(&receiveAddress.sa_data[2]);
	}
	return rSize != 0;
}

bool SocketImpl::SendData(const char* pAddress, const char* pData, size_t size)
{
	struct sockaddr_in sendAddress;
	std::memset(&sendAddress,'\0',sizeof(sendAddress));
	bool retVal = false;

#if 1
	sendAddress.sin_addr.s_addr = inet_addr(pAddress);
#else
	sendAddress.sin_addr.s_addr = htons(127)<<24 | htons(0)<<16| htons(0)<<8|htons(1);
#endif
	sendAddress.sin_family = AF_INET;
	sendAddress.sin_port = htons(STACK_PORT);
	retVal = sendto(m_pInnerStuff->m_Socket,pData,size,0,(struct sockaddr*) &sendAddress,sizeof(sendAddress)) != 0;
	if(retVal != true)
	{
		ERROR_LOG("sendto failed with error code %s \n", strerror(errno));
	}
	return retVal;
}

};

