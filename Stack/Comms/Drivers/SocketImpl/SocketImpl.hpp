/*
 * SocketImpl.hpp
 *
 *  Created on: 7 lut 2015
 *      Author: przem312
 */

#ifndef SOCKETIMPL_HPP_
#define SOCKETIMPL_HPP_
#include <stdint.h>
class TargetAddressContainer;
namespace Communication
{
	struct SocketInternals;
	class SocketImpl
	{
	public:
		SocketImpl(uint32_t bufferSize,char* broadCastIp);
		~SocketImpl();
		static const uint16_t STACK_PORT=1337;
		bool Init();
		bool SendData(const char* pAddress, const char* pData, size_t size);
		bool ReceiveData(char* pData,
									 uint32_t bufferSize,
									 TargetAddressContainer* pContainer,
									 int& rSize);
	private:
		bool isBroadcast(const char* Container) const;
		SocketInternals* m_pInnerStuff;
		uint32_t m_BufferSize;
	};
}


#endif /* SOCKETIMPL_HPP_ */
