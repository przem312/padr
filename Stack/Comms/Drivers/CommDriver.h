/*
 * CommDriver.h
 *
 *  Created on: Dec 29, 2013
 *      Author: przem312
 */

#ifndef COMMDRIVER_H_
#define COMMDRIVER_H_

#include "Driver.h"
#include "ObjectPool.h"
#include "MessageBase.h"
#include "Device.h"
#include "AssertMacro.h"
#include "TargetAddressContainerList.h"
#include "DriverBuffers.h"
#include "DriverBufferPool.h"
#include "Mutex.h"
namespace Communication
{
class CommDriver : public Driver
{
public:
	CommDriver()
:Driver(COMM_DRIVER)
{
};
virtual ~CommDriver()
{};
	static DriverHandler PushHandler(const TargetAddressContainer* pContainer);
	TargetAddressContainer* GetAddress(DriverHandler handlerNo);
	char* GetBuffer(DriverHandler handlerNo, int& bufferSize);
	static void ClearHandler(DriverHandler handlerNo)
	{
		m_Mutex.Take();
		DriverBuffer* pBuffer = GetBufferObj(handlerNo);
		if(NULL==pBuffer)
		{		
			m_Mutex.Release();
			return;
		}
		ASSERT( NULL!=pBuffer );
		m_BufferList.RemoveNode(pBuffer);
		m_BufferPool.ReleaseObject(pBuffer);
		m_Mutex.Release();
	}
	virtual char* Read(DriverHandler& handler, int& retVal);
	virtual DriverHandler isReadAvailable()=0;
protected:
	static LinkedList m_BufferList;
	static DriverBufferPool<MAX_CONNECTIONS_COUNT+1> m_BufferPool;
	static Mutex m_Mutex;
private:
	static DriverBuffer* GetBufferObj(DriverHandler handler);
	static DriverHandler m_handlerNumber;

};

}
#endif /* COMMDRIVER_H_ */
