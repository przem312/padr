/*
 * ConnectionInstance.hpp
 *
 *  Created on: Sep 29, 2014
 *      Author: PSokolo
 */

#ifndef CONNECTIONINSTANCE_HPP_
#define CONNECTIONINSTANCE_HPP_
#include "Types.h"
class TargetAddressContainer;

namespace Connection
{

    /*
     *
     */
    class ConnectionInstance
    {
    public:
        ConnectionInstance();
        virtual ~ConnectionInstance();
        bool Establish(TargetAddressContainer* pDestination, uint32_t timeOutValue);
        bool ShutDown();
    private:
        uint16_t m_connectionId;
        //Timer m_Timer;

    };

} /* namespace Connection */
#endif /* CONNECTIONINSTANCE_HPP_ */
