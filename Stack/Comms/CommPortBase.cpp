/*
 * CommPortBase.cpp
 *
 *  Created on: Dec 16, 2013
 *      Author: przem312
 */


#include "CommPortBase.h"
#include "CommandDispatcher.h"
#include "Console.h"
#include "AssertMacro.h"
#include "Device.h"
#include "Buffer.hpp"
#include "CommunicationMsg.h"
#include "WhoisMsg.h"
namespace Communication
{

CommunicationBase::CommunicationBase()
{
}

CommunicationBase::~CommunicationBase() {
}

void CommunicationBase::Init() {
	m_Status.SetStatus(DeviceStatus::GEN_IDDLE);
	for (int i = 0; i < 3; ++i) {
		m_pDriverList[i] = NULL;
	}

	m_pDriverList[ETHERNET_DRIVER] = EthernetDriver::GetInstance();
	m_pDriverList[ETHERNET_DRIVER]->Open();
	(void) ((CommandDispatcher::GetInstance()));
	TargetAddressContainer container(Device::GetBroadcastArray());
	WhoisHandler = CommDriver::PushHandler(&container);
	ASSERT(WhoisHandler >= 0);
}

DeviceStatus::Status CommunicationBase::SendData(const TargetAddressContainer* pAddr,
		char* pData, size_t dataSize) {
	DeviceStatus::Status retStatus = DeviceStatus::GEN_FAIL;
	CommDriver* pDrv = GetDriverByAddress(*pAddr);
	const DriverHandler handler = CommDriver::PushHandler(pAddr);
	if (handler == -1) {
		return retStatus;
	}
	const int bytesWritten = pDrv->Write(handler, pData, dataSize);
	pDrv->ClearHandler(handler);
	if (bytesWritten == dataSize) {
		retStatus = DeviceStatus::GEN_OK;
		DEBUG("Sent data %s on address: %s with size %d", pData,
				pAddr->GetAddressAsString(), dataSize);
	}
	return retStatus;
}

DeviceStatus::Status CommunicationBase::PollForResponse(uint32_t timeoutMs,
		MsgSrv* pSrv) {
	for (int var = 0; var < NUMBER_OF_SERVICES; ++var) {
		if (m_PendingRequest[var].pSrv == NULL) {
			m_PendingRequest[var].pSrv = pSrv;
			m_PendingRequest[var].timeout.StartCounting(timeoutMs);
			while (m_PendingRequest[var].isComplete == false
					&& m_PendingRequest[var].timeout.IsExpired() == false) {
#ifdef USE_OS
				Thread::SleepMs(0);
#endif
			}
			if (m_PendingRequest[var].isComplete) {
				m_PendingRequest[var].pSrv = NULL;
				m_PendingRequest[var].isComplete = false;
				m_PendingRequest[var].timeout.StopCounting();
				return DeviceStatus::COMM_MSG_PENDING;
			}
			break;
		}
	}
	return DeviceStatus::GEN_FAIL;
}

void CommunicationBase::ProcessMessage(MessageBody* const pMsg,
		DeviceStatus::Status& retStatus)
{
	if (pMsg->IsResponse() && pMsg->getRequestId() != 0) {
		for (int var = 0; var < NUMBER_OF_SERVICES; ++var) {
			if (m_PendingRequest[var].pSrv != NULL
					&& m_PendingRequest[var].pSrv->getCachedRequestId()
							== pMsg->getRequestId()) {
				MsgSrv* srv = m_PendingRequest[var].pSrv;
				std::memcpy(srv->GetBuffer(), (void*) pMsg, pMsg->GetMsgSize());
				m_PendingRequest[var].isComplete = true;
				retStatus = DeviceStatus::GEN_OK;
				break;
			}
		}
	} else {
		const MsgType msgType = pMsg->GetMsgType();
		for (int var = 0; var < NUMBER_OF_SERVICES; ++var) {
			if (m_RegisteredUsers[var].m_Handler == msgType) {
				retStatus = m_RegisteredUsers[var].m_Receive(pMsg);
				break;
			}
		}

	}

}

DeviceStatus::Status CommunicationBase::ReceiveData(CommDriver* pDrv,
		DriverHandler handler) {
	DeviceStatus::Status retStatus = DeviceStatus::GEN_BAD_DATA;
	static Mutex mutex;
	mutex.Take();
	int size = -1;
	MessageBody* const pMsg = (MessageBody*) ((pDrv->Read(handler, size)));
	if (size <= 0) {
		CommDriver::ClearHandler(handler);
		mutex.Release();
		return retStatus;
	}
	ProcessMessage(pMsg, retStatus);
	switch (retStatus) {
		case DeviceStatus::COMM_RESP_RDY: {
			const size_t MsgSize = pMsg->GetMsgSize();
			retStatus =
					(MsgSize == pDrv->Write(handler, (char*) (pMsg), MsgSize)) ?
							DeviceStatus::GEN_OK : DeviceStatus::GEN_BAD_DATA;
			break;
		}
		case DeviceStatus::GEN_OK:
			break;
		default: {
			SendErrorMsg(handler, retStatus, pDrv);
			break;
		}
		}
	CommDriver::ClearHandler(handler);
	mutex.Release();
	return retStatus;
}

void CommunicationBase::Close() {
}

DeviceStatus::Status CommunicationBase::run() {
	for (int i = 0; i < 3; i++) {
		CommDriver* pDrv = m_pDriverList[i];

		//ASSERT(m_Status.GetStatus() == DeviceStatus::GEN_OK);

		if (pDrv == NULL) {
			continue;
		}
		//ASSERT(pDrv->GetStatusObj()->GetStatus() != DeviceStatus::GEN_FATAL);
		// TODO ADD ERROR HANDLING
		const DriverHandler handler = pDrv->isReadAvailable();
		if (handler >= 0) {
			m_Status.SetStatus(ReceiveData(pDrv, handler));
		}
	}
	return DeviceStatus::GEN_OK;
}
DeviceStatus::Status RouteMsg(TargetAddressContainer& rAddr, MessageBody* const)
{

}
CommDriver* CommunicationBase::GetDriverByAddress(
		const TargetAddressContainer& rAddr) {
	const uint32_t addr = rAddr.GetAddressUint32();
	CommDriver* retVal = NULL;
	const uint32_t ETHERNET_ADDRESS_MASK = 0xFF000000;
	const uint32_t RADIO_ADDRESS_MASK = 0x00FF0000;
	const uint32_t UART_ADDRESS_MASK = 0x0000FF00;
	TargetAddressContainer broadCastAddress(Device::GetBroadcastArray());
	do {
		if (addr == broadCastAddress.GetAddressUint32()) {
			retVal = BroadcastDriver::GetInstance();
			break;
		}
		if (addr && ETHERNET_ADDRESS_MASK) {
			retVal = EthernetDriver::GetInstance();
			break;
		}
		if (addr && RADIO_ADDRESS_MASK) {
			retVal = m_pDriverList[RADIO_DRIVER];
			break;
		}
		if (addr && UART_ADDRESS_MASK) {
			break;
		}
	} while (0);
	return retVal;
}

DeviceStatus::Status CommunicationBase::SendCommand(const TargetAddressContainer* pAddr,
			const CommandId command, char* pAdditionalData,
			const size_t dataSize)
{
	char buffer[256];
	std::memset(buffer,0,256);
	CommandMsg* pMsg = (CommandMsg*)buffer;

	CommandDispatcher::GetInstance().GetCommandById(command)->PrepareSendCommand(pMsg,pAdditionalData,dataSize);
	SendData(pAddr,buffer,pMsg->GetMsgSize());
}
void CommunicationBase::ProcessErrorMsg(const TargetAddressContainer& rAddr,
		ErrorMsg* pMsg) {
	const DeviceStatus::Status errorStatus = pMsg->GetErrorType();
	LOG("ErrorMsg received from %s with: ErrorStatus %d",
			rAddr.GetAddressAsString(), errorStatus);
}

DeviceStatus::Status CommunicationBase::SendErrorMsg(DriverHandler handler,
		DeviceStatus::Status status, CommDriver* pDrv) {
	ErrorMsg msg(status);
	DeviceStatus tempstatus;
	msg.SetMessageSize();
	if (!pDrv) {
		return DeviceStatus::GEN_BAD_DATA;
	}
	const bool returnValue = pDrv->Write(handler, (char*) ((&msg)), sizeof(msg))
			== sizeof(msg);
	return returnValue ? DeviceStatus::GEN_OK : DeviceStatus::GEN_FATAL;
}

void CommunicationBase::TriggerWhoisMsg(CommDriver* pDrv) {
	WhoisMsg msg;
	msg.setIp(Device::GetDeviceIp());
	pDrv->Write(WhoisHandler, (char*) ((&msg)),sizeof(msg));
}
}
