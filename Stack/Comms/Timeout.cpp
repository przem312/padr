/*
 * Timeout.cpp
 *
 *  Created on: Oct 2, 2014
 *      Author: PSokolo
 */

#include "Timeout.hpp"

Timeout::Timeout(uint32_t time)
:m_State(STATE_STARTED)
{
   // SetCallBack(&TriggerTimeout);
}

void Timeout::Update()
{
    Reset();
}

Timeout::TimeoutState Timeout::GetState()
{
    return m_State;
}

Timeout::~Timeout()
{
    // TODO Auto-generated destructor stub
}

void Timeout::TriggerTimeout()
{
    m_State=STATE_TIMEOUT;
}
