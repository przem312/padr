/*
 * MsgSrv.h
 *
 *  Created on: Dec 13, 2013
 *      Author: przem312
 */

#ifndef MSGSRV_H_
#define MSGSRV_H_

#include "MessageReceiver.h"
#include "CommDriver.h"
#include "DeviceStatus.h"
class TargetAddressContainer;
namespace Communication
{
/*
 * Class MsgSrv
 * Message service. Abstraction for Dispatchers.
 * Allows to process messages through all registered types
 *
 * TODO: Shall be bridge?
 */

class CommunicationBase;
class MsgSrvPool;
class MsgSrv
{
public:
	friend class MsgSrvPool;
    /*
     * Basic implementation of message processing.
     * Reads message from driver and processes it to appropriate dispatcher.
     */
	void SetDestination(TargetAddressContainer*);
	void* GetBuffer()
	{
		return m_buffer.GetBufferStart();
	}
	int GetBufferSize()
	{
		return m_buffer.GetBufferSize();
	}
    DeviceStatus::Status Send(TargetAddressContainer* pContainer,MessageBody* pMsg);
    MsgSrv();
    virtual ~MsgSrv();

    bool OperationSucceeded();

	uint16_t getCachedRequestId() const {
		return cachedRequestId;
	}
private:
    void Init(CommunicationBase& rBase)
    {
    	m_pBase=&rBase;
    }
    void CheckRead(const bool givenValue);
    DriverHandler handler;
    uint16_t cachedRequestId;
    CommunicationBase* m_pBase;
    DeviceStatus* m_pStatus;
    BufferImpl<MAX_MSG_SIZE> m_buffer;
    static uint32_t requestId;

};
}

#endif /* MSGSRV_H_ */
