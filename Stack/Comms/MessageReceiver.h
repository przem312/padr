/*
 * MessageReceiver.h
 *
 * Object used as a bridge between command dispatcher and receive mechanism.
 *
 *
 *  Created on: Dec 1, 2013
 *      Author: przem312
 */

#ifndef MESSAGERECEIVER_H_
#define MESSAGERECEIVER_H_
#include "Types.h"
#include "CommDriver.h"
#include "DeviceStatus.h"
#include "TargetAddressContainer.h"
/*
 *
 */
namespace Communication
{
class MessageReceiver
{
public:
    /*
     * Passes msg to MsgDispatcher.
     * pBuffer is used for obtaining possible response
     */
	MessageBody* ReceiveMsg(DeviceStatus& rStatus, MessageBody* pBuffer, TargetAddressContainer& recipent, size_t& rSize);

    void AcquireDriver(CommDriver* pDriver)
    {
        m_pDriver = pDriver;
    }
    MessageReceiver(CommDriver* pDriver)
    :m_pDriver(pDriver)
    {
        //Nothing to do
    };
    virtual ~MessageReceiver();
private:
    CommDriver* m_pDriver;
    TargetAddressContainer* m_pResponseAddress;
};
}
#endif /* MESSAGERECEIVER_H_ */
