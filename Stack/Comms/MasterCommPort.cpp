/*
 * MasterCommPort.cpp
 *
 *  Created on: 10 sty 2015
 *      Author: przem312
 */

#include <MasterCommPort.hpp>

namespace Communication {
MasterCommPort* MasterCommPort::m_pThis = NULL;
MasterCommPort::MasterCommPort()
:m_timeout(WHOIS_INTERVAL_MS)
{

}

MasterCommPort::~MasterCommPort() {
	// TODO Auto-generated destructor stub
}

DeviceStatus::Status MasterCommPort::Main()
{
	while(true)
	{
		run();
	}
	return DeviceStatus::GEN_OK;
}
void MasterCommPort::Init()
{
	if (m_pThis == NULL)
	{
		m_pThis = this;
	}
	if (Device::IsMaster())
	{
		m_timeout.SetCallBack(&(MasterCommPort::TriggerWhois));
		m_timeout.StartCounting(WHOIS_INTERVAL_MS);
	}
}
} /* namespace Communication */
