/*
 * MessageReceiver.cpp
 *
 *  Created on: Dec 1, 2013
 *      Author: przem312
 */

#include "MessageReceiver.h"
#include "CommandDispatcher.h"
namespace Communication
{
 MessageBody* MessageReceiver::ReceiveMsg(DeviceStatus& rStatus, MessageBody* pBuffer, TargetAddressContainer& recipent, size_t& rSize)
{
    if(!m_pDriver)
        {
			rStatus.SetStatus( DeviceStatus::GEN_BAD_DATA);
            return NULL;
        }
    DriverHandler receiveHandler;
	char* tempBuffer = NULL;
    pBuffer=(MessageBody*)m_pDriver->Read(receiveHandler, (int&)rSize);
    recipent = *(m_pDriver->GetAddress(receiveHandler));
    return pBuffer;
}


MessageReceiver::~MessageReceiver()
{
    // TODO Auto-generated destructor stub
}
}