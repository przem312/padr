/*
 * MessageBase.h
 *
 *  Created on: Nov 24, 2013
 *      Author: przem312
 */

#ifndef MESSAGEBASE_H_
#define MESSAGEBASE_H_
#include <stdio.h>
#include <AssertMacro.h>
#include <Types.h>
#include <cstring>
#include <stdint.h>
#include <cstddef>
#include "CommandNumbers.h"
#include "DeviceStatus.h"

class MessageStruct;

const uint16_t IP_ARRAY_SIZE = 4;
static const uint16_t MAX_MESSAGE_SIZE = 16;

static uint32_t offset=0;

typedef enum msgType
{
	COMMAND = 0,
	MSG_WHOIS = 1,

	MSG_ERROR = 202,

	/*
	 * TODO ADD SUPPORT TO OTHER TYPES OF MESSAGES
	 */
	ACK_MSG = 0xFF
}MsgType;
class MessageHeader;
class MessageBody;
//class MessageFooter;

// TODO: EXTRACT IP TO THE RADIO DRIVER
class MessageHeader
{
public:

	/* static size_t GetAuthorIpOffset()
  {
    return (size_t)offsetof(MessageHeader,authorIpAddr);
  };

  void SetAuthorIp(const char* pAddr)
  {
    std::memcpy(authorIpAddr,pAddr,IP_ARRAY_SIZE);
  };
  void SetTargetIp(const char* pAddr)
  {
    std::memcpy(targetIpAddr,pAddr,IP_ARRAY_SIZE);
  };
	 */
	void SetDeviceId(uint16_t id)
	{
		deviceID=id;
	};

	void SetResponsReq(bool status)
	{
		isResponseRequested = status? 1 : 0;
	};

	/*  char* GetAuthorIp()
  {
    return authorIpAddr;
  };

  char* GetTargetIp()
  {
    return targetIpAddr;
  };*/

	uint16_t GetDeviceId()
	{
		return deviceID;
	};

	bool GetResponsReq()
	{
		return isResponseRequested == 1;
	};

	size_t GetHeaderSize()
	{
		return BASE_SIZE;
	}
	uint16_t getMessageId() const {
		return messageId;
	}

	void setMessageId(uint16_t messageId) {
		this->messageId = messageId;
	}

protected:

	static const size_t BASE_SIZE = 14;
	static const uint16_t MESSAGE_ID_UNDEFINED = 0xFFFF;
	/*  // Author IP address.
  char authorIpAddr[IP_ARRAY_SIZE];
  // Target IP address.
  char targetIpAddr[IP_ARRAY_SIZE];*/
	/*
	sizeof header->10
	 */
	/*
	 * Used for connected transactions. If not connected, shall be UINT16 MAX
	 */
	uint16_t messageId;
	/*
	 *  Author assigned device ID
	 *  Given by master.
	 *  Master ID = 0x01;
	 */
	uint16_t deviceID;

	uint16_t isResponseRequested;
	/*
	sizeof identifier = 6
	 */
};

class MessageBody
{
public:
	MessageBody()
	:messageType(0),m_msgSize(0),RequestId(0),response(0)
	{
	}
	void SetMsgType(MsgType type)
	{
		messageType = (type & 0xff);
	};
	uint16_t GetMsgSize() const
	{
		return m_msgSize;
	};
	void SetMessageSize(uint16_t msgSize = sizeof(MessageBody))
	{
		m_msgSize = msgSize;
	};
	MsgType GetMsgType() const
	{
		return (MsgType)(messageType & 0xff);
	};

	uint16_t getRequestId() const {
		return RequestId;
	};

	void setRequestId(uint16_t requestId) {
		RequestId = requestId;
	};
	void SetResponseMarker()
	{
		response = 1;
	}
	bool IsResponse()
	{
		return response == 1;
	}
protected:
	/*
	 * Message type.
	 */
	uint16_t messageType;
	uint16_t m_msgSize;
	uint16_t RequestId;
	uint16_t response;

};

class CommandMsg : public MessageBody
{
public:

	void SetCommandId(CommandId commId)
	{
		commandId=commId;
	};

	CommandId GetCommandId() const
	{
		return (CommandId)(commandId & 0xFF);
	};
	uint8_t GetTargetId() const
	{
		return targetpath&0xFF;
	};

	void SetTargetId(uint8_t id)
	{
		targetpath &=0xFF00;
		targetpath |= id;
	};

	uint8_t GetTargetAttributeId()
	{
		return targetpath&0xFF00>>8;
	};

	void SetTargetAttributeId(uint8_t id)
	{
		targetpath &=0x00FF;
		const uint16_t attribute = (id<<8 )&0x00FF;

		targetpath |= attribute;
	};

	uint8_t GetSenderId() const
	{
		return senderPath&0xFF;
	};

	void SetSenderId(uint8_t id)
	{
		senderPath &=0xFF00;
		senderPath |= id;
	};

	uint8_t GetSenderAttributeId()
	{
		return senderPath&0xFF00>>8;
	};

	void SetSenderAttributeId(uint8_t id)
	{
		senderPath &=0x00FF;
		const uint16_t attribute = (id<<8) &0x00FF;

		senderPath|= attribute;
	};
	char* getPayload()
	{
		return payLoad;
	}
protected:
	CommandMsg():senderPath(0),
	targetpath(0){};
private:
	uint16_t commandId;
	uint16_t targetpath;
	uint16_t senderPath;
protected:
	char* payLoad;

	/*
	 * Reply request.
	 */
};


class GetMsg : public CommandMsg
{
public:
	static size_t GetPayloadSize()
	{

		return sizeof(GetMsg);
	}

	GetMsg()
	{
		SetMessageSize();
		SetCommandId(GET_CMD);
		payLoad=(char*)&m_ResponseSize;
	};

	char* GetMsgBuffer()
	{
		return msg;
	}

	void SetMessageSize()
	{
		m_msgSize = sizeof(GetMsg) ;
	};


	bool AppendToMsgContent(char* pBuffer, uint32_t size)
	{
		if(size > 16)
		{
			return false;
		}
		std::memcpy(msg,pBuffer,size);

		m_ResponseSize +=size;

		return true;
	};

private:


	uint16_t m_ResponseSize;

	char msg[16];
};

class SetMsg : public CommandMsg
{
public:
	SetMsg()
	{
		SetMessageSize();
		SetCommandId(SET_CMD);
	}

	typedef struct
	{
		uint16_t m_TargetId;
		uint16_t m_AttributeId;
		uint16_t m_ResponseSize;
		char msg[sizeof(uint64_t)];
	}SetMsgContent;
private:
	SetMsgContent m_MsgContent;
};
/*
typedef union Message
{
  Message(const MessageHeader& rHeader,
		  const MessageBody& rBody)
  :MessageStruct(rHeader,rBody)
  {

  }
struct MessageStruct
{
	MessageStruct()
	{
	}

	bool Validate()
	{
		return messageChecksum == GetChecksum(m_Header,m_Body);
	}

	uint32_t GetChecksum(const MessageHeader& rHeader,
						 const MessageBody& rBody)
	{
		uint32_t checksum = 0;
		char* pHeader = (char*)&rHeader;
		char* pBody = (char*)&rBody;
		for(int i = 0; i<sizeof(MessageHeader); i++)
		{
			messageChecksum += pHeader[i];
		}
		for(int i = 0; i  <rBody.GetMsgSize(); i++)
		{
			messageChecksum += pBody[i];
		}
		return checksum;
	}

		MessageBody getBody() const {
			return m_Body;
		}

		void setBody(MessageBody body) {
			m_Body = body;
		}

		const MessageHeader& getHeader() const {
			return m_Header;
		}

		void setHeader(const MessageHeader& header) {
			m_Header = header;
		}

  MessageHeader m_Header;
  MessageBody m_Body;
  uint32_t messageChecksum;
  //MessageFooter m_Footer;
}messageStruct;
char m_MessageBuffer[32];
}Message;*/
#endif /* MESSAGEBASE_H_ */
