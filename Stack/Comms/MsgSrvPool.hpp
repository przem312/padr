/*
 * MsgSrvPool.hpp
 *
 *  Created on: 12-05-2015
 *      Author: psokolo
 */

#ifndef MSGSRVPOOL_HPP_
#define MSGSRVPOOL_HPP_
#include "MsgSrv.h"
#include "../Utils/ObjectPool.h"

namespace Communication {
class CommunicationBase;
class MsgSrvPool {
public:
	MsgSrvPool(CommunicationBase& rBase);
	~MsgSrvPool();
	MsgSrv* Get();
	void Release(MsgSrv*);
private:
	ObjectPool<MsgSrv,16> m_MsgSrvPool;
	CommunicationBase& m_rBase;
};
}
#endif /* MSGSRVPOOL_HPP_ */
