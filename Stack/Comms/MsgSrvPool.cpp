/*
 * MsgSrvPool.cpp
 *
 *  Created on: 12-05-2015
 *      Author: psokolo
 */

#include "MsgSrvPool.hpp"
namespace Communication
{
MsgSrvPool::MsgSrvPool(CommunicationBase& rBase)
:m_rBase(rBase)
{
	// TODO Auto-generated constructor stub
}

MsgSrvPool::~MsgSrvPool() {
	// TODO Auto-generated destructor stub
}

MsgSrv* MsgSrvPool::Get()
{
	MsgSrv* pObj = m_MsgSrvPool.AcquireObject();
	if(pObj != NULL)
	{
		pObj->Init(m_rBase);
	}
	return pObj;
}
void MsgSrvPool::Release(MsgSrv* pObj)
{
	m_MsgSrvPool.ReleaseObject(pObj);
}
}
