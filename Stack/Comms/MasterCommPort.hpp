/*
 * MasterCommPort.hpp
 *
 *  Created on: 10 sty 2015
 *      Author: przem312
 */

#ifndef MASTERCOMMPORT_HPP_
#define MASTERCOMMPORT_HPP_

#include <Stack.h>

#include "Timeout.hpp"
namespace Communication {

class MasterCommPort: public CommunicationBase {
public:
	MasterCommPort();
	MasterCommPort(CommunicationBase& Base)
	:CommunicationBase(Base)
	,m_timeout(WHOIS_INTERVAL_MS)
	{
		Init();
	}
	virtual ~MasterCommPort();

	DeviceStatus::Status Main();

private:
	void Init();
	static void TriggerWhois()
	{
		MasterCommPort::m_pThis->TriggerWhoisMsg();
	}
	static const uint32_t WHOIS_INTERVAL_MS = 10000;
	static MasterCommPort* m_pThis;
	Timeout m_timeout;
};

} /* namespace Communication */

#endif /* MASTERCOMMPORT_HPP_ */
