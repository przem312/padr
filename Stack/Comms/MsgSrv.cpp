/*
 * MsgSrv.cpp
 *
 *  Created on: Dec 13, 2013
 *      Author: przem312
 */

#include "MsgSrv.h"
#include "MessageBase.h"
#include "AssertMacro.h"
#include "CommandDispatcher.h"
#include "Drivers/CommDriver.h"
#include "CommPortBase.h"
namespace Communication
{
uint32_t MsgSrv::requestId = 1;
DeviceStatus::Status MsgSrv::Send(TargetAddressContainer* pContainer,MessageBody* pMsg)
{
	DEBUG("SEND");

	const uint16_t messageRequestId = 0xFFFF & requestId++;
	pMsg->setRequestId(messageRequestId);
	size_t msgSize = pMsg->GetMsgSize();
	m_pBase->SendData(pContainer,(char*)pMsg, msgSize);
	cachedRequestId=pMsg->getRequestId();
	return m_pBase->PollForResponse(1000,this);
}


bool MsgSrv::OperationSucceeded()
{
	return m_pStatus->GetStatus() == DeviceStatus::COMM_MSG_PENDING || m_pStatus->GetStatus() == DeviceStatus::COMM_RESP_RDY;
}
void MsgSrv::SetDestination(TargetAddressContainer* pContainer)
{
	handler = CommDriver::PushHandler(pContainer);
}

MsgSrv::MsgSrv()
:m_pBase(NULL),
 m_pStatus(NULL)
{
	// TODO Auto-generated constructor stub
}

MsgSrv::~MsgSrv()
{
	// TODO Auto-generated destructor stub
}

}
