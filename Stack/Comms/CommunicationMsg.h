/*
 * CommunicationMsg.h
 *
 *  Created on: 16 gru 2014
 *      Author: przem312
 */

#ifndef COMMUNICATIONMSG_H_
#define COMMUNICATIONMSG_H_
#include "MessageBase.h"
#include <string.h>

class ErrorMsg : public MessageBody
{
public:
	ErrorMsg(DeviceStatus::Status status)
{
		SetMsgType(MSG_ERROR);
		SetMessageSize();
		SetErrorType(status);
};

	void SetMessageSize()
	{
		m_msgSize = sizeof(uint16_t);
	};

	void SetErrorType(DeviceStatus::Status status)
	{
		m_ErrorType = (uint16_t)(status & 0xff);
	}

	DeviceStatus::Status GetErrorType()
	{
		return (DeviceStatus::Status)m_ErrorType;
	}

private:
	uint16_t m_ErrorType;
};



#endif /* COMMUNICATIONMSG_H_ */
