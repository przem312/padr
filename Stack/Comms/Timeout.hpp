/*
 * Timeout.hpp
 *
 *  Created on: Oct 2, 2014
 *      Author: PSokolo
 */

#ifndef TIMEOUT_HPP_
#define TIMEOUT_HPP_
#include "Timer/SoftwareTimer.hpp"
/*
 *
 */
class Timeout : public SoftwareTimer
{
public:
    typedef enum
    {
        STATE_IDDLE,
        STATE_STARTED,
        STATE_TIMEOUT
    }TimeoutState;

    Timeout(uint32_t time = DEFAULT_TIMEOUT);
    void Update();
    TimeoutState GetState();
    virtual ~Timeout();
private:
    void TriggerTimeout();
    TimeoutState m_State;
    static const uint32_t DEFAULT_TIMEOUT = 10;
};

#endif /* TIMEOUT_HPP_ */
