/*
 * CommPortBase.h
 *
 *  Created on: Dec 16, 2013
 *      Author: przem312
 */

#ifndef COMMPORTBASE_H_
#define COMMPORTBASE_H_
#include "Types.h"
#ifdef USE_OS
#include <Thread.h>
#include <Mutex.h>
#endif
#include "ObjectPool.h"
#include "LinkedList.h"
#include "MsgSrv.h"
#include "DeviceStatus.h"
#include "TargetAddressContainer.h"
#include "CommandNumbers.h"

#include "Driver.h"
#include "EthernetDriver.h"
#include "NRF24L01Driver.h"
#include "BroadcastDriver.h"
#include "BufferPool.hpp"
#include "MessageBase.h"
#include "Device.h"
#include "Timeout.hpp"

class ErrorMsg;
class WhoisMsg;

namespace Communication
{
	class MsgSrv;
	class CommunicationBase;
/*
 * Base object used for distributing data via comm devices.
 * Provides asynchronous mechanism for receiving/sending messages through known medias.
 * Receiving mechanism is provided within thread.
 *
 */
typedef DeviceStatus::Status (*ReceiveHandler)(MessageBody* pMsg);
class CommunicationBase
{
public:
	void Init();
	void Close();
	DeviceStatus::Status SendData(const TargetAddressContainer* pAddr, char* pData,
			size_t dataSize);
	DeviceStatus::Status SendCommand(const TargetAddressContainer* pAddr,
			const CommandId command, char* pAdditionalData,
			const size_t dataSize);

	DeviceStatus::Status ReceiveData(CommDriver* pDrv, DriverHandler handler);
	DeviceStatus::Status PollForResponse(uint32_t timeoutMs, MsgSrv*);
	DeviceStatus::Status run();

	void RegisterUser(ReceiveHandler pHandler, MsgType type) {
		for (int index = 0; index < NUMBER_OF_SERVICES; ++index) {
			if (m_RegisteredUsers[index].m_Receive == NULL) {
				m_RegisteredUsers[index].m_Receive = pHandler;
				m_RegisteredUsers[index].m_Handler = (uint32_t)(type);
				return;
			}
		}

		ASSERT(false);
	}

	void TriggerWhoisMsg(CommDriver* pDrv = BroadcastDriver::GetInstance());
	DeviceStatus::Status ReceiveWhoisResp(CommDriver* pDrv,
			DriverHandler handler);
	CommunicationBase();
	~CommunicationBase();

private:
	CommDriver* GetDriverByAddress(const TargetAddressContainer& rAddr);
	void ProcessErrorMsg(const TargetAddressContainer& rAddr, ErrorMsg* pMsg);
	DeviceStatus::Status SendErrorMsg(DriverHandler handler,
			DeviceStatus::Status status,CommDriver* pDrv);
	void ProcessMessage(MessageBody* const pMsg,
			DeviceStatus::Status& retStatus);

	DeviceStatus::Status RouteMsg(TargetAddressContainer& rAddr, MessageBody* const);
	static const int NUMBER_OF_SERVICES = 3;
	static const size_t NUMBER_OF_DRIVERS = NUMBER_OF_SERVICES;
	struct RegisteredUsers {
		RegisteredUsers() {
			m_Receive = NULL;
			m_Handler = (uint32_t)(ACK_MSG);
		}

		ReceiveHandler m_Receive;
		uint32_t m_Handler;
	} m_RegisteredUsers[NUMBER_OF_SERVICES];
	struct PendingServices {
		PendingServices() :
				timeout(100) {
			isComplete = false;
			pSrv = NULL;
		}

		bool isComplete;
		MsgSrv* pSrv;
		Timeout timeout;
	} m_PendingRequest[NUMBER_OF_SERVICES];
	static const unsigned long MAX_TIMEOUT = 0xFFFFFFFF;
	CommDriver* m_pDriverList[3];
	static const int RADIO_DRIVER = 0;
	static const int ETHERNET_DRIVER = 1;
	static const int UART_DRIVER = 2;
	DeviceStatus m_Status;
	DriverHandler WhoisHandler;
};
}
#endif /* COMMPORTBASE_H_ */
