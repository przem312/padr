/*
 * TargetAddressContainerList.cpp
 *
 *  Created on: Jan 11, 2014
 *      Author: przem312
 */
#include <cstring>

#include "TargetAddressContainerList.h"
#if 0
TargetAddressContainerList::TargetAddressContainerList()
:numberCount(1),
m_linkState(LIST_OK)
{
   //Nothing to do
}

TargetAddressContainerList::~TargetAddressContainerList()
{
    // TODO Auto-generated destructor stub
}


bool TargetAddressContainerList::IsNodeOnList(TargetAddressContainer* pAddress)
{
    bool retVal = false;
    if(m_linkState == LIST_UPDATE_IN_PROGRESS)
    {
    	return retVal;
    }
    const uint32_t addressValue = pAddress->GetAddressUint32();
    AddressArrayNode* pAddressNo = GetFirst();
        while(pAddressNo!=NULL)
            {
              if(pAddressNo->m_pAddress == NULL)
                break;
                if(addressValue == pAddressNo->m_pAddress->GetAddressUint32())
                    {
                        retVal = true;
                        break;
                    }
                pAddressNo = GetNext();
            }
    return retVal;
}

DriverHandler TargetAddressContainerList::AddNodeToList(TargetAddressContainer* pAddress, const char* pNodeName, const size_t nameSize)
{
    DriverHandler retHandler = -1;

    if(IsNodeOnList(pAddress))
        {
            retHandler = GetHandler(pAddress);
        }
    else
    {
        AddressArrayNode* pAddr = new AddressArrayNode(pAddress, pNodeName, nameSize);
        pAddr->m_handler = GetNextHandler();
        AddNode(pAddr);
        retHandler = pAddr->m_handler;
    }
    m_linkState = LIST_OK;
    return retHandler;
}

TargetAddressContainer* TargetAddressContainerList::GetNode(const DriverHandler handler)
{
    AddressArrayNode* pRetVal = NULL;
    if(m_linkState != LIST_UPDATE_IN_PROGRESS)
    {
        pRetVal = GetObjFromList(handler);
    }
    return pRetVal ? pRetVal->m_pAddress : NULL;
}

bool TargetAddressContainerList::GetNode(DriverHandler handler, TargetAddressContainer* pContainer)
{
    TargetAddressContainer* pRetVal = NULL;
    pRetVal = GetNode(handler);
    return pRetVal != NULL;
}

size_t TargetAddressContainerList::GetNodeNameString(const DriverHandler handler, char* pBuffer, const size_t bufferLength)
{
    AddressArrayNode* pObj = GetObjFromList(handler);
    size_t stringLength = std::strlen(pObj->m_TargetNameString);
    std::memset(pBuffer,'/0',bufferLength);
    if(bufferLength > stringLength)
    {
        std::memcpy(pObj->m_TargetNameString, pBuffer, stringLength);
    }
    else
    {
        std::memcpy(pObj->m_TargetNameString, pBuffer, bufferLength);
        stringLength = bufferLength;
    }
    return stringLength;
}

void TargetAddressContainerList::ClearList()
{
    AddressArrayNode* pStored = GetFirst();
    while(pStored)
    {
        delete pStored;
        pStored = GetNext();
    }
    CleanList();
    numberCount=0;
    m_linkState = LIST_UPDATE_IN_PROGRESS;
}

AddressArrayNode* TargetAddressContainerList::GetObjFromList(const DriverHandler handler)
{
    AddressArrayNode* pRetVal = GetFirst();
    while(pRetVal)
    {
        if(handler == pRetVal->m_handler)
        {
            break;
        }
        pRetVal = GetNext();
    }
    return pRetVal;
}

DriverHandler TargetAddressContainerList::GetHandler(TargetAddressContainer* pAddress)
{
    AddressArrayNode* pRetVal = GetFirst();
        while(pRetVal)
        {
            if(pAddress == pRetVal->m_pAddress)
            {
                break;
            }
            pRetVal = GetNext();
        }
		ASSERT(pRetVal!=NULL);
        return pRetVal->m_handler;
}
#endif