/*
 * TargetAddressContainerList.h
 *
 *  Created on: Jan 11, 2014
 *      Author: przem312
 */

#ifndef TARGETADDRESSCONTAINERLIST_H_
#define TARGETADDRESSCONTAINERLIST_H_

#include <cstring>

#include "Driver.h"
#include "LinkedList.h"
#include "TargetAddressContainer.h"

struct AddressArrayNode
{
    AddressArrayNode()
    :m_pAddress((uint32_t)0),
     m_handler(-1)
    {
    	    }
    DriverHandler m_handler;
    TargetAddressContainer m_pAddress;
    DeviceStatus m_status;
};



/*
 * Contains addresses of nodes in the net.
 * TODO Provide list clean up mechanism.
 */
class TargetAddressContainerList : public LinkedList
{
public:
    void Init();
    bool IsNodeOnList(TargetAddressContainer* pAddress);
    DriverHandler AddNodeToList(TargetAddressContainer* pAddress, const char* pNodeName, const size_t nameSize);

    TargetAddressContainer* GetNode(const DriverHandler handler);
    bool GetNode(const DriverHandler handler, TargetAddressContainer* pContainer);

    size_t GetNodeNameString(const DriverHandler handler, char* pBuffer, const size_t bufferLength);
    void ClearList();
protected:
    DriverHandler GetHandler(TargetAddressContainer* pAddress);
    size_t numberCount;
private:
    enum ListState
    {
        LIST_OK,
        LIST_UPDATE_IN_PROGRESS,
    }m_linkState;

    AddressArrayNode* GetObjFromList(const DriverHandler handler);
    DriverHandler GetNextHandler()
    {
        size_t numberCopy = numberCount;
        numberCount++;
        return (DriverHandler)numberCopy;
    }
};



#endif /* TARGETADDRESSCONTAINERLIST_H_ */
