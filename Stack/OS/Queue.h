/*
 * Queue.h
 *
 *  Created on: Mar 6, 2015
 *      Author: PSokolo
 */

#ifndef STACK_OS_QUEUE_H_
#define STACK_OS_QUEUE_H_
#include <stdint.h>


struct QueueImpl;

class Queue
{
public:
    Queue(uint32_t capacity)
    {
        m_pQueueImpl = new QueueImpl(capacity);
    }
    ~Queue()
    {
        delete m_pQueueImpl;
    }

    void Put(void* data);
    void* Get();

private:
    QueueImpl* m_pQueueImpl;
};


#endif /* STACK_OS_QUEUE_H_ */
