/*
 * SystemTick.hpp
 *
 *  Created on: 13 sty 2015
 *      Author: przem312
 */

#ifndef SYSTEMTICK_HPP_
#define SYSTEMTICK_HPP_

#include <Thread.h>

#include "Timer/GlobalTimer.hpp"

class SystemTick: public Thread {
public:
	SystemTick();
	virtual ~SystemTick();
	virtual void run()
	{
		while(true)
		{
			GlobalTimer::Instance().Tick();
			SleepMs(1);
		}
	}
};

#endif /* SYSTEMTICK_HPP_ */
