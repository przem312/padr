/*
 * MutexWindows.cpp
 *
 *  Created on: Aug 19, 2014
 *      Author: PSokolo
 */

#include "Mutex.h"
#include <Windows.h>
class MutexOsSpecificData
{
public:
    HANDLE m_handle;
};

Mutex::Mutex()
{
    m_pData = new MutexOsSpecificData();

	m_pData->m_handle = CreateMutex(NULL, FALSE, NULL);

}

Mutex::~Mutex()
{
    CloseHandle(m_pData->m_handle);
    delete m_pData;
}


bool Mutex::Take()
{
	bool retVal = (WAIT_OBJECT_0 == WaitForSingleObject(m_pData->m_handle, INFINITE));
	return retVal;
}

void Mutex::Release()
{
	ReleaseMutex(m_pData->m_handle);
}
