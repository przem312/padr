#include "Thread.h"

#include <Windows.h>
#include <tchar.h>


#include "assertmacro.h"

class OsSpecificHandles
{
public:
	HANDLE m_Handle;
	DWORD m_ThreadId;
};

DWORD WINAPI pvTaskCode( LPVOID lpParam )
{
	static_cast<Thread*>(lpParam)->SleepMs(50);
    static_cast<Thread*>(lpParam)->run();
	return 1;
}

Thread::Thread(const char *name, unsigned short stackDepth, char priority) {
	m_Handles = new OsSpecificHandles;
	m_Handles->m_Handle = CreateThread(NULL,
									0,
									pvTaskCode,
									this,
									0,
									&m_Handles->m_ThreadId);
	
	ASSERT(m_Handles->m_Handle != NULL);
}

void Thread::SleepMs(uint32_t ms)
{
    Sleep(ms);
}

Thread::~Thread()
{
	delete m_Handles;
}
