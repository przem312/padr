/*
 * QueueWindows.cpp
 *
 *  Created on: Mar 6, 2015
 *      Author: PSokolo
 */


#include <Queue.h>
#include <windows.h>

struct QueueImpl
{
    int m_nCapacity;
    CPtrList m_list;

    CRITICAL_SECTION m_lock;
    HANDLE m_queue_not_empty;
    HANDLE m_queue_not_full;


};
