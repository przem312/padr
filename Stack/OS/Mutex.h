/*
 * Mutex.h
 *
 *  Created on: Aug 19, 2014
 *      Author: PSokolo
 */

#ifndef MUTEX_H_
#define MUTEX_H_

/*
 *
 */
class MutexOsSpecificData;

class Mutex
{
public:
    Mutex();
    virtual ~Mutex();
    void Init();
    bool Take();
    void Release();
private:
    MutexOsSpecificData* m_pData;
};

#endif /* MUTEX_H_ */
