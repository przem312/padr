#include "Thread.h"

#include <pthread.h>
#include <unistd.h>

#include "AssertMacro.h"

class OsSpecificHandles
{
public:
	pthread_t m_ThreadId;
};

void* pvTaskCode( void* lpParam )
{
	static_cast<Thread*>(lpParam)->SleepMs(0);
    static_cast<Thread*>(lpParam)->run();
    pthread_exit(NULL);
    while(1) Thread::SleepMs(1);
}

Thread::Thread(const char *name, unsigned short stackDepth, char priority) {
	m_Handles = new OsSpecificHandles;
	DEBUG("Created thread: %s",name);
	ASSERT(0 == pthread_create (&m_Handles->m_ThreadId, NULL, &pvTaskCode, this));
	pthread_detach(m_Handles->m_ThreadId);
}

void Thread::SleepMs(uint32_t ms)
{
usleep(1000 * ms);
}

Thread::~Thread()
{
	delete m_Handles;
}
