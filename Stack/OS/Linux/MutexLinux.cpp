/*
 * MutexWindows.cpp
 *
 *  Created on: Aug 19, 2014
 *      Author: PSokolo
 */

#include "Mutex.h"
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>

pthread_t tid[2];
class MutexOsSpecificData
{
public:
	pthread_mutex_t m_handle;
};

Mutex::Mutex()
{
	m_pData = new MutexOsSpecificData();

	pthread_mutex_init(&m_pData->m_handle, NULL);
}

Mutex::~Mutex()
{
	pthread_mutex_destroy(&m_pData->m_handle);
	delete m_pData;
}


bool Mutex::Take()
{
	bool retVal = (0 == pthread_mutex_lock(&m_pData->m_handle));
	return retVal;
}

void Mutex::Release()
{
	pthread_mutex_unlock(&m_pData->m_handle);
}
