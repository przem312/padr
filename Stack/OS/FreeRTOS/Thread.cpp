/* This file has been prepared for Doxygen automatic documentation generation.*/
/*
 * Copyright (C) 2012 Yuriy Kulikov
 *      Universitaet Erlangen-Nuernberg
 *      LS Informationstechnik (Kommunikationselektronik)
 *      Support email: Yuriy.Kulikov.87@googlemail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* Scheduler include files. */
#ifdef USE_OS
#include "Thread.h"

#include "FreeRTOS.h"
#include "task.h"


struct OsSpecificHandles
{
	xTaskHandle m_Handle;
	portTickType m_TickType;
};

extern "C" void pvTaskCode(void *pvParameters) {
    (static_cast<Thread*>(pvParameters))->run();
}

Thread::Thread(const char *name, unsigned short stackDepth, char priority) {
	m_Handles = new OsSpecificHandles;
    xTaskCreate(pvTaskCode, name, stackDepth, (void*) this, priority, &m_Handles->m_Handle);
    m_Handles->m_TickType = xTaskGetTickCount();
}

void Thread::SleepMs(uint32_t ms)
{
    vTaskDelayUntil(&m_Handles->m_TickType, ms/portTICK_RATE_MS);
}

Thread::~Thread()
{
	delete m_Handles;
}
#endif
