/*
 * Mutex.cpp
 *
 *  Created on: Aug 19, 2014
 *      Author: PSokolo
 */

#include "Mutex.h"
#include "FreeRtos.h"
#include "AssertMacro.h"
#include "semphr.h"
class MutexOsSpecificData
{
public:
    xSemaphoreHandle m_handle;
};

Mutex::Mutex()
{
    m_pData = new MutexOsSpecificData();
    ASSERT(m_pData);
    vSemaphoreCreateBinary(m_pData->m_handle);
    ASSERT(m_pData->m_handle);
}

Mutex::~Mutex()
{
    vSemaphoreDelete(m_pData->m_handle);
    delete m_pData;
}


bool Mutex::Take()
{
    return xSemaphoreTake( m_pData->m_handle, ( TickType_t ) 10 ) == pdTRUE;
}
void Mutex::Release()
{
    xSemaphoreGive( m_pData->m_handle );
}
