PROJECT (Stack)

CMAKE_MINIMUM_REQUIRED(VERSION 2.6.0)

SET (STACK_BASE 
	Stack.h 
	Stack.cpp
)

SET (APPLICATION_SRC	
	Application/AppAttributeNumbers.h
	Application/AppBase.cpp
	Application/AppBase.h
	Application/AppInitializer.cpp
	Application/AppInitializer.h
	Application/AppMngr.cpp
	Application/AppMngr.h

	Application/AdcApp/AdcApp.h
	Application/AdcApp/AdcApp.cpp
	Application/AdcApp/AdcController.hpp
	Application/AdcApp/AdcController.cpp

	Application/DeviceApp.cpp
	Application/DeviceApp.h
	
	Application/LedApp/LedApp.h
	Application/LedApp/LedApp.cpp
	Application/Bsp/BspAdc.h
	Application/Bsp/BspLed.h
	Application/Bsp/Driver.h
)

SET (COMMAND_SRC
	Command/Command.cpp
	Command/Command.h
	Command/CommandDispatcher.cpp
	Command/CommandDispatcher.h
	Command/CommandFactory.cpp
	Command/CommandFactory.hpp
	Command/CommandNumbers.h
	Command/GetCommand.cpp
	Command/GetCommand.h
	Command/GetResp.cpp
	Command/GetResp.h
	Command/SetCommand.cpp
	Command/SetCommand.h
)

SET (COMM_SRC
	Comms/CommPortBase.cpp
	Comms/CommPortBase.h

	Comms/CommunicationMsg.h
	Comms/MessageBase.h

	Comms/MessageReceiver.cpp
	Comms/MessageReceiver.h
	Comms/MsgSrv.cpp
	Comms/MsgSrv.h

	#Comms/Connection
	Comms/Drivers/BufferList.hpp
	Comms/Drivers/CommDriver.cpp
	Comms/Drivers/CommDriver.h
	Comms/Drivers/DriverBufferPool.h
	Comms/Drivers/DriverBuffers.h
)

SET (UTILS_SRC
	Utils/AssertMacro.h
	Utils/Buffer.hpp
	Utils/BufferPool.hpp
	Utils/DeviceStatus.h
	Utils/LinkedList.h
	Utils/Listener.cpp
	Utils/Listener.h
	Utils/Notifier.h
	Utils/ObjectPool.cpp
	Utils/ObjectPool.h
	Utils/RequestHandler.cpp
	Utils/RequestHandler.h
	Utils/Types.h
)

SET (CONSOLE_FULL_SRC	
	Utils/Console/Console.cpp

	Utils/Console/DebugConsole.cpp
	Utils/Console/DebugConsole.h
	Utils/Console/LogConsole.cpp
	Utils/Console/LogConsole.h
)
		
SET (CONSOLE_STUB_SRC
	Utils/Console/ConsoleStub.cpp
	Utils/Console/Console.h
	Utils/Console/ConsoleInstance.h
	)
	
INCLUDE_DIRECTORIES (
	${CMAKE_CURRENT_SOURCE_DIR}
	Application/
	Application/AdcApp/
	Application/LedApp/
	Application/Bsp/
	Command/
	Comms/
	Comms/Communication/
	Comms/Drivers/
	OS/
	Utils/
	Utils/Console/
	Utils/Device/
)

ADD_EXECUTABLE (
	${STACK_BASE}
	${APPLICATION_SRC}
	${COMMAND_SRC}
	${COMM_SRC}
	${UTILS_SRC}
	${CONSOLE_STUB_SRC}
)