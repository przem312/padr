/*
 * CommunicationThread.hpp
 *
 *  Created on: Mar 10, 2015
 *      Author: PSokolo
 */

#ifndef STACK_COMMUNICATIONTHREAD_HPP_
#define STACK_COMMUNICATIONTHREAD_HPP_

#include <Thread.h>
class Stack;
/*
 *
 */
class CommunicationThread : public Thread
{
public:
    CommunicationThread(Stack& pStack);
    virtual ~CommunicationThread();
    virtual void run();
private:
    Stack& m_Stack;
};

#endif /* STACK_COMMUNICATIONTHREAD_HPP_ */
