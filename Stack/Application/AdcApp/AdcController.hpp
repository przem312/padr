/*
 * AdcController.hpp
 *
 *  Created on: Aug 18, 2014
 *      Author: PSokolo
 *      TODO: ADD DIFFERENT ADCs, THIS IS PROOF OF CONCEPT
 */

#ifndef ADCCONTROLLER_HPP_
#define ADCCONTROLLER_HPP_
#include "Driver.h"
/*
 *
 */
class AdcController : public Driver
{
public:
    AdcController();
    virtual ~AdcController();
    virtual char* Read(DriverHandler& handler, int& retVal);
    /*
     * Returns number of written bytes.
     */
    virtual int Write(DriverHandler handler, char* pSource,const size_t sourceSize)
    {
        return -1;
    }

    virtual void Open();
    virtual void Close(){}

    virtual int IOCTL(DriverCmd cmd, char* pBuffer = NULL, size_t bufferSize =0)
    {
        // TODO;
        return -1;
    };

    virtual int isReadAvailable();
    typedef union {
    struct responseformatstruct
    {
        uint16_t m_decimalPart;
        uint16_t m_floatPart;
    } m_ResponseFormat;
    char buffer[sizeof(responseformatstruct)];
    }ResponseFormat;
private:
    static const uint32_t ADC_BUFFER_SIZE = 8;

    uint8_t currentBufferPos;
    ResponseFormat m_Buffer[ADC_BUFFER_SIZE];
//    const DriverHandler m_myHandlerId;

};

#endif /* ADCCONTROLLER_HPP_ */
