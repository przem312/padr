/*
 * AdcApp.h
 *
 *  Created on: Aug 18, 2014
 *      Author: PSokolo
 */

#ifndef ADCAPP_H_
#define ADCAPP_H_

#include "AppBase.h"
#include "AdcController.hpp"
#include "Device.h"
/*
 *
 */
#define NUMBER_OF_ADC 2
class AdcApp : public AppBase
{
public:
    AdcApp(AppType type);
    virtual ~AdcApp();
    static AppBase* Init();

    virtual void ProcessAppSpecificResponse(AttributeSet attributeId, char* pBuffer);
    virtual void run();
    protected:

    virtual uint8_t GetArgumentNumber()
    {
    	return NUMBER_OF_ADC;
    }

    private:
//        AdcController m_Controller;
        static const uint8_t ADC0_VALUE = 4;
        static const uint8_t ADC1_VALUE = 5;
        static const uint8_t ADC2_VALUE = 6;
        static const uint8_t ADC3_VALUE = 7;

        virtual bool GetAppSpecific(const AttributeSet attributeId,
                                                    char* pBuffer);
        DeviceStatus::Status m_Status;
};

#endif /* ADCAPP_H_ */
