/*
 * AdcController.cpp
 *
 *  Created on: Aug 18, 2014
 *      Author: PSokolo
 */

#include "AdcController.hpp"

#include "BspAdc.h"

AdcController::AdcController()
:Driver(I_O_DRIVER)//m_myHandlerid(0)
{
    currentBufferPos = 0;
}

AdcController::~AdcController()
{
    // TODO Auto-generated destructor stub
}
void AdcController::Open()
{
    InitADC(0);
}
char* AdcController::Read(DriverHandler& handler, int& retVal)
{
    uint32_t decimalValue = 0;
    uint32_t floatValue = 0;
    if(handler == 0)
    {
        for(int i=0;i<ADC_BUFFER_SIZE; ++i)
        {
            decimalValue += m_Buffer[i].m_ResponseFormat.m_decimalPart;
            floatValue +=  m_Buffer[i].m_ResponseFormat.m_floatPart;
            m_Buffer[i].m_ResponseFormat.m_decimalPart = 0;
            m_Buffer[i].m_ResponseFormat.m_floatPart = 0;
        }
        m_Buffer[0].m_ResponseFormat.m_decimalPart= decimalValue / ADC_BUFFER_SIZE;
        m_Buffer[0].m_ResponseFormat.m_floatPart = floatValue / ADC_BUFFER_SIZE;
        currentBufferPos = 1;
        retVal = sizeof(m_Buffer[0]);
        return m_Buffer[0].buffer;
    }
    return NULL;
}

int AdcController::isReadAvailable()
{

#if 1
    uint32_t readValue = 0;
   uint16_t decimalPart = 0;
    uint16_t floatPart = 0;
    uint8_t retVal = ReadADC(0,&decimalPart,&floatPart);
    if(retVal != 0)
    {
        m_Buffer[currentBufferPos%ADC_BUFFER_SIZE].m_ResponseFormat.m_decimalPart = decimalPart;
        m_Buffer[currentBufferPos%ADC_BUFFER_SIZE].m_ResponseFormat.m_floatPart = floatPart;
        currentBufferPos++;
    }
#endif
   return retVal;
}
