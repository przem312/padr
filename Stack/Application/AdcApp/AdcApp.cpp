/*
 * AdcApp.cpp
 *
 *  Created on: Aug 18, 2014
 *      Author: PSokolo
 */

#include "AdcApp.h"
#include "AppMngr.h"
AdcApp::AdcApp(AppType type)
:AppBase(type,"AdcApp"),
 m_Status(DeviceStatus::GEN_IDDLE)
{
    // TODO Auto-generated constructor stub

}

AdcApp::~AdcApp()
{
    // TODO Auto-generated destructor stub
}

AppBase* AdcApp::Init()
{
	return NULL;
}

void AdcApp::ProcessAppSpecificResponse(AttributeSet attributeId, char* pBuffer)
{
	if(attributeId.m_AttrNo > 4+GetArgumentNumber()  ||
	   	  attributeId.sizeOfAttr < sizeof(AdcController::ResponseFormat))
	   {
		   return;
	   }
	AdcController::ResponseFormat resp;
	std::memcpy(resp.buffer,pBuffer,sizeof(resp));
	LOG("Value read from ADC %d: %d,%d",
		attributeId.m_AttrNo,
		resp.m_ResponseFormat.m_decimalPart,
		resp.m_ResponseFormat.m_floatPart);
}
void AdcApp::run()
{
	//m_Controller.Open();
#ifdef USE_OS
        //SleepMs(1000);
#endif
	while(true)
	{
		for(int i = 0; i<NUMBER_OF_ADC; ++i)
		{
			//m_Controller.isReadAvailable();
		}
		#ifdef USE_OS
//        SleepMs(100);
#endif
	}
}

bool AdcApp::GetAppSpecific(const AttributeSet attributeId,
                                                    char* pBuffer)
{
	if(attributeId.m_AttrNo > 4+GetArgumentNumber() ||
	   attributeId.m_AttrNo == 0 ||
   	  attributeId.sizeOfAttr < sizeof(AdcController::ResponseFormat))
   {
	   return false;
   }
   AdcController::ResponseFormat resp;
#if 0
   resp.m_ResponseFormat.m_decimalPart = 13;
   resp.m_ResponseFormat.m_floatPart = 37;
   std::memcpy(pBuffer,resp.buffer,sizeof(AdcController::ResponseFormat) );
   return true;
#else
   DriverHandler handler = attributeId.m_AttrNo-5;
   int retVal = 0;


	//std::memcpy(pBuffer,m_Controller.Read(handler, retVal),retVal);
	return (handler != -1) && (retVal > 0);
#endif   
}
