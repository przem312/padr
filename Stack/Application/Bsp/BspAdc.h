/*
 * BspAdc.h
 *
 *  Created on: Aug 25, 2014
 *      Author: PSokolo
 */

#ifndef BSPADC_H_
#define BSPADC_H_
#include "Types.h"
#if defined( __cplusplus )
extern "C"
{
#endif
    void InitADC(uint32_t adcNo);
    uint8_t ReadADC(uint32_t adcNo, uint16_t* pDecimal, uint16_t* pFloat);
#if defined( __cplusplus )
}
#endif

#endif /* BSPADC_H_ */
