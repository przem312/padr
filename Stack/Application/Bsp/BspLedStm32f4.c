/*
 * BspLedStm32f4.c
 *
 *  Created on: Aug 26, 2014
 *      Author: PSokolo
 */
#if defined( __cplusplus )
extern "C"
{
#endif
#include "BspLed.h"
#include "Stm32f4xx.h"
static uint8_t isInited[4] = {0};
static uint32_t getPinForLed(uint8_t ledNo);
void InitLed(uint8_t ledNo)
{
	/* This TypeDef is a structure defined in the
	 * ST's library and it contains all the properties
	 * the corresponding peripheral has, such as output mode,
	 * pullup / pulldown resistors etc.
	 *
	 * These structures are defined for every peripheral so
	 * every peripheral has it's own TypeDef. The good news is
	 * they always work the same so once you've got a hang
	 * of it you can initialize any peripheral.
	 *
	 * The properties of the periperals can be found in the corresponding
	 * header file e.g. stm32f4xx_gpio.h and the source file stm32f4xx_gpio.c
	 */
	GPIO_InitTypeDef GPIO_InitStruct;

	/* This enables the peripheral clock to the GPIOD IO module
	 * Every peripheral's clock has to be enabled
	 *
	 * The STM32F4 Discovery's User Manual and the STM32F407VGT6's
	 * datasheet contain the information which peripheral clock has to be used.
	 *
	 * It is also mentioned at the beginning of the peripheral library's
	 * source file, e.g. stm32f4xx_gpio.c
	 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);

	/* In this block of instructions all the properties
	 * of the peripheral, the GPIO port in this case,
	 * are filled with actual information and then
	 * given to the Init function which takes care of
	 * the low level stuff (setting the correct bits in the
	 * peripheral's control register)
	 *
	 *
	 * The LEDs on the STM324F Discovery are connected to the
	 * pins PD12 thru PD15
	 */
	GPIO_InitStruct.GPIO_Pin = getPinForLed(ledNo);

	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOF, &GPIO_InitStruct);             // this finally passes all the values to the GPIO_Init function which takes care of setting the corresponding bits.
	isInited[ledNo]=1;
}
uint8_t GetLedState(uint8_t ledNo)
{
	if(ledNo > 3)
	{
		return 0;
	}
	if(isInited[ledNo] == 0)
	{
		InitLed(ledNo);
	}

	uint32_t gpioPin = getPinForLed(ledNo);
	return GPIO_ReadOutputDataBit(GPIOF,gpioPin);
}
void SetLedState(uint8_t ledNo, uint8_t state)
{
	uint32_t gpioPin = getPinForLed(ledNo);
	if(ledNo > 3)
	{
		return;
	}
	if(isInited[ledNo] == 0)
	{
		InitLed(ledNo);
	}

	BitAction action;
	if(state != 0)
	{
		action = 0;
	}
	else
	{
		action = 1;
	}
	GPIO_WriteBit(GPIOF, gpioPin, action);

}

uint32_t getPinForLed(uint8_t ledNo)
{
	uint32_t gpioPin = 0;
	if(ledNo > 3)
	{
		return gpioPin;
	}

	switch(ledNo)
	{
	case 0:
	{
		gpioPin = GPIO_Pin_6;
		break;
	}
	case 1:
	{
		gpioPin = GPIO_Pin_7;
		break;
	}
	case 2:
	{
		gpioPin = GPIO_Pin_8;
		break;
	}
	case 3:
	{
		gpioPin = GPIO_Pin_9;
		break;
	}
	default: break;
	}
	return gpioPin;
}
#if defined( __cplusplus )
}
#endif
