/*
 * GlobalTimer.cpp
 *
 *  Created on: Oct 1, 2014
 *      Author: PSokolo
 */

#include "Timer/GlobalTimer.hpp"


GlobalTimer::GlobalTimer()
:Notifier()
{
    // TODO Auto-generated constructor stub

}

GlobalTimer::~GlobalTimer()
{
    // TODO Auto-generated destructor stub
}


void Timer_Tick(void)
{
    GlobalTimer::Instance().Tick();
}

uint32_t Timer_GetTicks(void)
{
    return GlobalTimer::Instance().GetTicks();
}
