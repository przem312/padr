/*
 * GlobalTimer.h
 *
 *  Created on: Oct 2, 2014
 *      Author: PSokolo
 */

#ifndef GLOBALTIMER_H_
#define GLOBALTIMER_H_
#include "Types.h"
#if defined( __cplusplus )
extern "C"
{
#endif

void Timer_Tick(void);
uint32_t Timer_GetTicks(void);

#if defined( __cplusplus )
}
#endif
#endif /* GLOBALTIMER_H_ */
