/*
 * GlobalTimer.hpp
 *
 *  Created on: Oct 1, 2014
 *      Author: PSokolo
 */

#ifndef GLOBALTIMER_HPP_
#define GLOBALTIMER_HPP_

#include <stdint.h>
#include <Types.h>
#include "Notifier.h"
/*
 *
 */
struct TimeTicks
{
	uint64_t m_TickCount;
};

class GlobalTimer : public Notifier<TimeTicks>
{
public:
    static GlobalTimer& Instance()
    {
        static GlobalTimer timer;
        return timer;
    }

    void Tick()
    {
    	m_TickCount.m_TickCount+=1;
    	Notify(m_TickCount);
    }

    uint64_t GetTicks()
    {
        return m_TickCount.m_TickCount;
    }

private:
    GlobalTimer();
    virtual ~GlobalTimer();

    TimeTicks m_TickCount;
};

#endif /* GLOBALTIMER_HPP_ */
