/*
 * SoftwareTimer.hpp
 *
 *  Created on: Oct 2, 2014
 *      Author: PSokolo
 */

#ifndef SOFTWARETIMER_HPP_
#define SOFTWARETIMER_HPP_

#include "Listener.h"
#include "Types.h"

#include "Timer/GlobalTimer.hpp"
/*
 *
 */
typedef void (*TimerExpiredCallback)();

class SoftwareTimer : public Listener<TimeTicks>
{
public:
    SoftwareTimer();
    virtual ~SoftwareTimer();

    void SetCallBack(TimerExpiredCallback callback);

    void StartCounting(uint32_t timeMs);

    void Reset();

    void StopCounting();

    bool IsExpired()
    {
        return m_CurrentValue.m_TickCount > m_EndValue.m_TickCount;
    }
    virtual void OnNotify(TimeTicks& rArg);

private:
    TimerExpiredCallback m_CallBack;
    TimeTicks m_CurrentValue;
    TimeTicks m_StartValue;
    TimeTicks m_EndValue;
};

#endif /* SOFTWARETIMER_HPP_ */
