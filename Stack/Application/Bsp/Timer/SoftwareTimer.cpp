/*
 * SoftwareTimer.cpp
 *
 *  Created on: Oct 2, 2014
 *      Author: PSokolo
 */

#include "Timer/SoftwareTimer.hpp"
#include "Timer/GlobalTimer.hpp"
#include <stdint.h>
#include <stddef.h>
SoftwareTimer::SoftwareTimer()
:m_CallBack(NULL)
{

}

SoftwareTimer::~SoftwareTimer()
{

}

void SoftwareTimer::SetCallBack(TimerExpiredCallback callback)
{
    m_CallBack=callback;
}

void SoftwareTimer::StartCounting(uint32_t timeMs)
{
	m_CurrentValue.m_TickCount=0;
	m_EndValue.m_TickCount=0;
	m_StartValue.m_TickCount=0;
    // TODO Auto-generated constructor stub
    GlobalTimer::Instance().RegisterListener(this);
    m_StartValue.m_TickCount = GlobalTimer::Instance().GetTicks();
    m_EndValue.m_TickCount = m_StartValue.m_TickCount+timeMs;
}

void SoftwareTimer::Reset()
{
    const uint32_t resetOffset = GlobalTimer::Instance().GetTicks() - m_StartValue.m_TickCount;
    m_StartValue.m_TickCount += resetOffset;
    m_EndValue.m_TickCount += resetOffset;
}

void SoftwareTimer::StopCounting()
{
    m_StartValue.m_TickCount=0;
    m_EndValue.m_TickCount=0;
	GlobalTimer::Instance().RemoveListener(this);
}

void SoftwareTimer::OnNotify(TimeTicks& rArg)
{
    m_CurrentValue = rArg;
    if(IsExpired())
    {
        if(m_CallBack)
            m_CallBack();
		Reset();
    }
}
