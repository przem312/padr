/*
 * BspLed.h
 *
 *  Created on: Aug 26, 2014
 *      Author: PSokolo
 */

#ifndef BSPLED_H_
#define BSPLED_H_

#include "Types.h"

#if defined( __cplusplus )
extern "C"
{
#endif
void InitLed(uint8_t ledNo);
uint8_t GetLedState(uint8_t ledNo);
void SetLedState(uint8_t ledNo, uint8_t state);
#if defined( __cplusplus )
}
#endif
#endif /* BSPLED_H_ */
