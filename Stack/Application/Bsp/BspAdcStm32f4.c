/*
 * BspAdc.c
 *
 *  Created on: Aug 25, 2014
 *      Author: PSokolo
 */

#include "BspAdc.h"
#include "stm32f4xx.h"

#if defined( __cplusplus )
extern "C"
{
#endif
  void initInternalAdc(void);
void initInternalAdc(void)
{
        ADC_DeInit();
        ADC_InitTypeDef ADC_InitStruct;
        ADC_CommonInitTypeDef ADC_CommonInitStruct;

        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

        ADC_CommonInitStruct.ADC_Mode = ADC_Mode_Independent;
        ADC_CommonInitStruct.ADC_Prescaler = ADC_Prescaler_Div8;
        ADC_CommonInitStruct.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
        ADC_CommonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
        ADC_CommonInit(&ADC_CommonInitStruct);

        ADC_InitStruct.ADC_Resolution = ADC_Resolution_12b;
        ADC_InitStruct.ADC_ScanConvMode = DISABLE;
        ADC_InitStruct.ADC_ContinuousConvMode = ENABLE;
        ADC_InitStruct.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
        ADC_InitStruct.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
        ADC_InitStruct.ADC_DataAlign = ADC_DataAlign_Right;
        ADC_InitStruct.ADC_NbrOfConversion = 1;
        ADC_Init(ADC1, &ADC_InitStruct);
        ADC_Cmd(ADC1,ENABLE);
        // ADC1 Configuration, ADC_Channel_TempSensor is actual channel 16
        ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 1,
                        ADC_SampleTime_144Cycles);

        // Enable internal temperature sensor
        ADC_TempSensorVrefintCmd(ENABLE);

          // Enable ADC conversion
    }
static uint8_t readInternalAdc(uint16_t* pDecimal, uint16_t* pFloat);

    void InitADC(uint32_t adcNo)
    {
        switch(adcNo)
        {
        case 0:
        {
            initInternalAdc();
            break;
        }
        default:
            break;
        }
    }

    uint8_t ReadADC(uint32_t adcNo, uint16_t* pDecimal, uint16_t* pFloat)
    {
        uint8_t retVal = 0;
        switch(adcNo)
        {
            case 0:
            {
                retVal = readInternalAdc(pDecimal,pFloat);
                break;
            }
            default:
            {
                *pDecimal = 0;
                *pFloat = 0;
                break;
            }
        }
        return retVal;
    }

    
    
    uint8_t readInternalAdc(uint16_t* pDecimal, uint16_t* pFloat)
    {
        uint8_t retVal = 0;
        ADC_SoftwareStartConv(ADC1); //Start the conversion
        while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET)
        {
          
        }//Processing the conversion
        uint32_t TemperatureValue = ADC_GetConversionValue(ADC1); //Return the converted data
        retVal = (TemperatureValue != 0);
        if(retVal != 0)
        {
            TemperatureValue *= 10000; // change to 0.xmV
            TemperatureValue -= 7600;  // substract 25 ref value
            TemperatureValue /= 25;   // Divide by 2.5 mV slope
            TemperatureValue -= 25; // Add the 25�C
            *pDecimal = (TemperatureValue / 10000) - 16; //Reading in Volts
            *pFloat = (TemperatureValue - (*pDecimal*10000)) % 1000;
        }
        return retVal;

    }
#if defined( __cplusplus )
}
#endif