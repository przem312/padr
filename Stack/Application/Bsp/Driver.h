/*
 * Driver.h
 *
 *  Created on: Dec 9, 2013
 *      Author: przem312
 */

#ifndef DRIVER_H_
#define DRIVER_H_

#include "DeviceStatus.h"
#include "Types.h"
#include "TargetAddressContainer.h"
#include "AssertMacro.h"
/*
 *
 */

    typedef enum BspDriverType
    {
        I_O_DRIVER,
        HMI_DRIVER,
        COMM_DRIVER
    }BspDriverType;

    typedef char DriverCmd;

    typedef int DriverHandler;
    class Driver;


    class Driver
    {
    public:
        /*
         * Returns number of read bytes.
         */
    	virtual char* Read(DriverHandler& handler, int& retVal)=0;
        /*
         * Returns number of written bytes.
         */
        virtual int Write(DriverHandler handler, const char* pSource, const size_t sourceSize)=0;

        virtual void Open() = 0;
        virtual void Close()
        {

        };

        virtual int IOCTL(DriverCmd cmd, char* pBuffer = NULL, size_t bufferSize =0)=0;

        DeviceStatus* GetStatusObj()
        {
            return &m_Status;
        };

        BspDriverType GetBspDriverType()
        {
            return m_Type;
        };

    protected:
        Driver(BspDriverType type)
        :m_Type(type)
        {
            m_Status.SetStatus(DeviceStatus::GEN_IDDLE);
        };
        virtual ~Driver()
        {
        	Close();
        };
        BspDriverType m_Type;
        DeviceStatus m_Status;

    };



#endif /* DRIVER_H_ */
