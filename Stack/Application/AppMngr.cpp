/*
 * AppMngr.cpp
 *
 *  Created on: Jan 31, 2014
 *      Author: PSokolo
 */

#include "AppMngr.h"
#include "Device.h"

#include <cstring>

#include "AdcApp/AdcApp.h"
#include "LedApp/LedApp.h"
#include "DeviceApp.h"
//#include "UiApp/UiApp.h"


const char* AppMngr::AppList::m_ApplicationName = "APP_LIST";

AppMngr::AppMngr()
 :m_pInitialized(false),
  m_List(NULL)
{
}

AppMngr::~AppMngr()
{
	delete m_List;
}

AppMngr* AppMngr::GetInstance()
{
	static AppMngr theInstance;
	return &theInstance;
}


    DeviceStatus::Status AppMngr::ExecuteRequest(uint8_t appId,
    									uint8_t attributeId,
    									char* const pBuffer,
    									size_t bufferSize )
	{
		AppBase* pApp = GetApp((AppType)appId);
		if(pApp==NULL)
		{
			return DeviceStatus::GEN_BAD_DATA;
		}
		AttributeSet attrSet;
		attrSet.m_AttrNo = (AttributeNo)attributeId;
		attrSet.sizeOfAttr=bufferSize;

		return      pApp->Get(attrSet,pBuffer) ? DeviceStatus::GEN_OK : DeviceStatus::GEN_BAD_DATA;
	}

AppBase* AppMngr::GetApp(AppType app)
{
	AppBase* pRetVal = (AppBase*)(AppLinkedList.GetFirst());
	while(pRetVal != NULL)
	{
		if(pRetVal->GetAppType() == app)
		{
			break;
		}
		pRetVal = (AppBase*)(AppLinkedList.GetNext());
	}
	return pRetVal;
}

void AppMngr::RegisterApp(AppBase* pApp)
{
	AppLinkedList.AddNode(pApp);
}

//TODO:Remove this
DeviceStatus::Status AppMngr::InitializeApp()
{

	m_List = new AppList(&this->AppLinkedList);
	DeviceStatus::Status retStatus = DeviceStatus::GEN_OK;
	do
	{
		if(DEVICE_CONFIG.m_IsUiOn)
		{
			retStatus = InitializeApp(UI_APP);
			if(retStatus != DeviceStatus::GEN_OK )
			{
				break;
			}
		}
		if(DEVICE_CONFIG.m_IsLedOn)
		{
			retStatus = InitializeApp(LED_APP);
			if(retStatus != DeviceStatus::GEN_OK )
			{
				break;
			}
		}

		if(DEVICE_CONFIG.m_IsAdcOn)
		{
			retStatus = InitializeApp(ADC_APP);
			if(retStatus != DeviceStatus::GEN_OK )
			{
				break;
			}

		}
		if(DEVICE_CONFIG.m_IsPwmOn)
		{
			retStatus = InitializeApp(PWM_APP);
			if(retStatus != DeviceStatus::GEN_OK )
			{
				break;
			}
		}

		if(DEVICE_CONFIG.m_IsIoOn)
		{
			retStatus = InitializeApp(IO_APP);
			if(retStatus != DeviceStatus::GEN_OK )
			{
				break;
			}
		}

		m_pInitialized = true;

	}
	while(0);
#if 0
        m_pInitialized = true;
	return retStatus;
#else
// TODO!
        m_pInitialized = true;
        return DeviceStatus::GEN_OK;
#endif
}

DeviceStatus::Status ExecuteRequest(uint8_t appId,
		uint8_t attributeId,
		char* const pBuffer,
		size_t bufferSize = sizeof(uint64_t))
{
	return DeviceStatus::GEN_BAD_DATA;
}

DeviceStatus::Status AppMngr::InitializeApp(AppType app)
{
	DeviceStatus::Status retStatus = DeviceStatus::GEN_OK;
	switch( app )
	{
	case ADC_APP:
		CREATE_APP(AdcApp, ADC_APP);
		break;
	case UI_APP:
//	    m_pAppList[UI_APP] = UiApp::Init();
		break;
#if 0
	case AppType::IO_APP:
		CREATE_APP(IoApp, UI_APP);
		break;
#endif
	case LED_APP:
		CREATE_APP(LedApp, LED_APP);
		break;
#if 0
	case AppType::PWM_APP:
		CREATE_APP(PwmApp,PWM_APP);
		break;
#endif

	default:
		break;
	}
	return retStatus;
}
