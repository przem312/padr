/*
 * AppAttributeNumbers.h
 *
 *  Created on: Mar 29, 2014
 *      Author: przem312
 */

#ifndef APPATTRIBUTENUMBERS_H_
#define APPATTRIBUTENUMBERS_H_
#include "Types.h"
typedef enum
{
    A_STATUS = 1,
    A_NAME,
    A_ARG_CNT,
    A_ID
}AttributeNo;
enum AppType
{
    DEVICE_APP = 0,
    UI_APP = 1,
    LED_APP = 2,
    ADC_APP = 3,
    PWM_APP = 4,
    IO_APP = 5,
    WEBSERVER_APP = 6,
    APP_LIST = 255,
    ENUM_SIZE = 8
};

typedef struct attrSet
{
    AttributeNo m_AttrNo;
    uint32_t sizeOfAttr;
}AttributeSet;
/*
 *
 */
//static const AttributeSet APP_STATUS = {
//                                          A_STATUS,
//                                         sizeof(uint8_t)
//                                        };
//static const AttributeSet APP_NAME      ={
//                                         A_NAME,
//                                         16
//                                         };
//static const AttributeSet APP_ARGUMENT_COUNT   ={
//                                                A_ARG_CNT,
//                                                sizeof(uint8_t)
//                                                };
//static const AttributeSet App_ID               ={
//                                    A_ID,
//                                    sizeof(uint8_t)
//                                                };
//

#endif /* APPATTRIBUTENUMBERS_H_ */
