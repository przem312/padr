/*
 * AppMngr.h
 *
 *  Created on: Jan 31, 2014
 *      Author: PSokolo
 */

#ifndef APPMNGR_H_
#define APPMNGR_H_

#include <DeviceStatus.h>
#include "AppBase.h"
#include "Console.h"
#include <string.h>
#define CREATE_APP(AppClass, AppId) new AppClass(AppId)
#define CREATE_APP_ARGUMENT(AppClass, AppId, Argument)\
		new AppClass(AppId, Argument)


class AppMngr {
public:

    static AppMngr* GetInstance();

    AppBase* GetApp(AppType app);

    DeviceStatus::Status InitializeApp();

    DeviceStatus::Status ExecuteRequest(uint8_t appId,
    									uint8_t attributeId,
    									char* const pBuffer,
    									size_t bufferSize = sizeof(uint64_t));
    void RegisterApp(AppBase*);
private:
	DeviceStatus::Status InitializeApp(AppType app);

	AppMngr();
	virtual ~AppMngr();

	class AppList : public AppBase
	{

	public:
	    AppList(LinkedList* pArray)
	    :AppBase(APP_LIST, m_ApplicationName),
		m_AppList(pArray)
	    {

	    };
		virtual ~AppList()
		{
		}

		virtual AppBase* Init()
		{
			return this;
		}
		
	    virtual uint8_t GetArgumentNumber()
	    {
	    	return 1;
	    };
	    virtual bool GetAppSpecific(const AttributeSet attributeId,
                                        char* pBuffer)
	    {
	    	bool status = false;

	    	if(attributeId.sizeOfAttr < ENUM_SIZE)
	    	{
	    		return false;
	    	}
	    	if(attributeId.m_AttrNo==APPLIST_CONTENT)
	    	{
	    		memset(pBuffer,0,attributeId.sizeOfAttr);
	    		AppBase* pBase = (AppBase*)m_AppList->GetFirst();
	    		char i = 0;
	    		while(pBase != NULL)
	    		{
	    			*(pBuffer+i) = (uint8_t)pBase->GetAppType();
	    			++i;
	    			pBase = (AppBase*)m_AppList->GetNext();
	    		}
	    		status = true;
	    	}

	    	return status;
	    }

	    virtual void ProcessAppSpecificResponse(AttributeSet attr, char* pBuffer)
	    {
#if 0
	    	if(attr.m_AttrNo!=0)
	    	{
	    		return;
	    	}
	    	for(int i=0; i<attr.sizeOfAttr; ++i)
	    	{
	    		if(*(pBuffer+i)==1)
	    		{
	    			AppBase* pApp = m_pArray[i];
	    			if(pApp)
	    			{
	    				LOG("%d    %s", i,pApp->GetAppName());
	    			}
	    		}
	    	}
#endif
	    }
	private:
	    static const uint8_t APPLIST_CONTENT=0;
	    LinkedList* m_AppList;
       		static const char* m_ApplicationName;
	};
	AppList* m_List;
	LinkedList AppLinkedList;
	bool m_pInitialized;
};

#endif /* APPMNGR_H_ */
