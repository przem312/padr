/*
 * AppBase.cpp
 *
 *  Created on: Mar 29, 2014
 *      Author: przem312
 */

#include <iostream>
#include <cstring>
#include "AssertMacro.h"
#include "AppBase.h"
#include "AppMngr.h"


bool AppBase::Get(const AttributeSet attributeSet, char* pBuffer)
{
	bool resp = true;
    switch(attributeSet.m_AttrNo)
    {
    case A_ID:
        *pBuffer = m_AppId;
        break;
    case A_NAME:
        std::strncpy(pBuffer, m_pAppName, attributeSet.sizeOfAttr);
        break;
    case A_STATUS:
        *pBuffer = m_Status;
        break;
    case A_ARG_CNT:
        *pBuffer = 4+GetArgumentNumber();
        break;
    default:
        resp = GetAppSpecific(attributeSet,pBuffer);
        break;
    }
    return resp;
}

void AppBase::ProcessResponse(AttributeSet attributeSet, char* pBuffer)
{
	switch(attributeSet.m_AttrNo)
	    {
	    case A_ID:
	        LOG("Application ID: %d",*pBuffer);
	        break;
	    case A_NAME:
	        LOG("Application name: %s", pBuffer);
	        break;
	    case A_STATUS:
	    	LOG("Application status: %d",*pBuffer);
	        break;
	    case A_ARG_CNT:
	        LOG("Application argument number: %d",*pBuffer);
	        break;
	    default:
	        ProcessAppSpecificResponse(attributeSet,pBuffer);
	        break;
	    }
}
AppBase::AppBase(AppType appId, const char* appName, uint32_t timeOutMs)
:m_AppId(static_cast<uint8_t> (appId)),
 m_TimeoutObj(timeOutMs),
 m_pAppName(appName),
 m_Status(DeviceStatus::GEN_IDDLE)
{
	AppMngr::GetInstance()->RegisterApp(this);
    m_AppNameSize = std::strlen(m_pAppName);
	//m_TimeoutObj.SetCallBack(&run);
}
