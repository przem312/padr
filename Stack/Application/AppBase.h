/*
 * AppBase.h
 *
 *  Created on: Mar 29, 2014
 *      Author: przem312
 */

#ifndef APPBASE_H_
#define APPBASE_H_
#include <stdint.h>
#include "Types.h"
#include "AppAttributeNumbers.h"
#include <Thread.h>
#include "DeviceStatus.h"

#include "Timeout.hpp"
/*
 *
 */
class AppBase :public ListNode

{
public:
    bool Get(const AttributeSet attributeId, char* pBuffer);
    virtual bool Set(const AttributeSet attributeId, char* pBuffer)
    {
        return false;
    }
    void ProcessResponse(AttributeSet attributeSet, char* pBuffer);
    const char* GetAppName()
    {
    	return m_pAppName;
    }
    AppType GetAppType()
    {
    	return static_cast<AppType>(m_AppId);
    }
protected:
    virtual void ProcessAppSpecificResponse(AttributeSet attr, char* pBuffer)=0;
    AppBase(AppType appId,const char* m_pAppName, uint32_t timeOutMs = 1000);

    virtual uint8_t GetArgumentNumber() = 0;
	virtual ~AppBase()
	{}
private:

    virtual bool GetAppSpecific(const AttributeSet attributeId,
                                                char* pBuffer) = 0;
    const char* m_pAppName;
    DeviceStatus::Status m_Status;
    const uint8_t m_AppId;
    uint32_t m_AppNameSize;
    Timeout m_TimeoutObj;

};

#endif /* APPBASE_H_ */
