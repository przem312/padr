/*
 * LedApp.h
 *
 *  Created on: Aug 19, 2014
 *      Author: PSokolo
 */

#ifndef LEDAPP_H_
#define LEDAPP_H_

#include "AppBase.h"

/*
 *
 */
class LedApp : public AppBase
{
public:
    LedApp(AppType type);
    virtual ~LedApp();

    static AppBase* Init();

    virtual void ProcessAppSpecificResponse(AttributeSet attributeId, char* pBuffer);
    virtual void run();
    protected:

    virtual uint8_t GetArgumentNumber()
    {
        return 4;
    }

    private:
        static const uint8_t LED0_VALUE = 5;
        static const uint8_t LED1_VALUE = 6;
        static const uint8_t LED2_VALUE = 7;
        static const uint8_t LED3_VALUE = 8;
        uint8_t m_LedState[4];
        typedef union
        {
            typedef struct
            {
                uint8_t m_LedNo;
                uint8_t m_State;
            }m_LedStruct;
            char pBuffer[sizeof(m_LedStruct)];
        }LedResponse;
        virtual bool GetAppSpecific(const AttributeSet attributeId,
                                                    char* pBuffer);
        DeviceStatus::Status m_Status;

};

#endif /* LEDAPP_H_ */
