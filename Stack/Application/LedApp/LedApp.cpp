/*
 * LedApp.cpp
 *
 *  Created on: Aug 19, 2014
 *      Author: PSokolo
 */

#include "LedApp/LedApp.h"
#include "AppMngr.h"
#include <cstring>
#include "BspLed.h"
LedApp::LedApp(AppType type)
:AppBase(type, "LedApp")
{
    // TODO Auto-generated constructor stub
    std::memset(&m_LedState,0,sizeof m_LedState);
}

LedApp::~LedApp()
{
    // TODO Auto-generated destructor stub
}

AppBase* LedApp::Init()
{
	return NULL;
}

void LedApp::ProcessAppSpecificResponse(AttributeSet attrSet,char* pBuffer)
{
  static char bitSetLiteral[] = "ON";
  static char bitResetLiteral[] = "OFF";
  const uint16_t ledNo = attrSet.m_AttrNo;
  char* state = (*pBuffer == 0) ? bitResetLiteral : bitSetLiteral;
  LOG("Led no %d: %s",ledNo,state);
}

void LedApp::run()
{
#ifdef USE_OS
    while(true)
    {
//    SleepMs(1000);
        SetLedState(1,1 == GetLedState(1));
    }
#endif
}

bool LedApp::GetAppSpecific(const AttributeSet attributeId,
                            char* pBuffer)
{
    bool retVal = false;
    switch(attributeId.m_AttrNo)
    {
        case LED0_VALUE:
        {
            *pBuffer=GetLedState(0);
            retVal = true;
            break;
        }
        case LED1_VALUE:
        {
            *pBuffer=GetLedState(1);
            retVal = true;
            break;
        }
        case LED2_VALUE:
        {
            *pBuffer=GetLedState(2);
            retVal = true;
            break;
        }

        case LED3_VALUE:
        {
            *pBuffer=GetLedState(3);
            retVal = true;
            break;
        }
        default:
        {
            break;
        }
    }
    return retVal;
}
