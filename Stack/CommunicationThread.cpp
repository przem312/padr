/*
 * CommunicationThread.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: PSokolo
 */

#include <CommunicationThread.hpp>
#include <StackState/InitState.hpp>
#include <Device.h>
#include <Console.h>
#include <Stack.h>
#include "DeviceStatus.h"
CommunicationThread::CommunicationThread(Stack& rStack)
	:m_Stack(rStack), Thread("CommunicationThread",1024,2)
{
    // TODO Auto-generated constructor stub

}

CommunicationThread::~CommunicationThread()
{
    // TODO Auto-generated destructor stub
}

void CommunicationThread::run()
{
    InitState initState(&m_Stack);
    //  m_DeviceStatus.m_Status = DeviceStatus::GEN_IDDLE;

        initState.Execute();
        DEBUG("Device %d successfully started with address: %s and status: %d",
              Device::GetDeviceId(),
              Device::GetDeviceIpAsString(),
              0);
}
