/*
 * Stack.cpp
 *
 *  Created on: Jan 30, 2014
 *      Author: przem312
 */

#include <CommPortBase.h>
#include <AppMngr.h>
#include <StackState/InitState.hpp>
#include "AssertMacro.h"
#include "Stack.h"
#include "CommunicationThread.hpp"
//#include "AppMngr.h"
#include "Device.h"

#include "CommandDispatcher.h"
#include "Comms/MsgSrvPool.hpp"
#if defined(LOG_FILES)
Console* Stack::m_pConsoleInstance=NULL;
#endif



DeviceStatus::Status Stack::Start()
{

	m_CommPort = new Communication::CommunicationBase;
	Communication::MsgSrvPool* pool = new Communication::MsgSrvPool(*m_CommPort);
	CommandDispatcher::GetInstance().Init(m_CommPort,*pool);
	m_pThread = new CommunicationThread(*this);
	//whois = new Whois(m_CommPort);
	return DeviceStatus::GEN_OK;
}

void Stack::Stop()
{
	delete m_pThread;
//    Console::GetConsole().ShutDown();
//    CommPortBase::GetInstance().Stop();
//    AppList::GetInstance().ShutDown();
    m_DeviceStatus.m_Status = DeviceStatus::GEN_FATAL;
}

void Stack::Init()
{

}

Stack::Stack()
:whois(NULL)
 //m_rAppMngr(AppMngr::Create()),
{
}

Stack::~Stack()
{
    // nothing to do
}
