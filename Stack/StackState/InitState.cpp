/*
 * InitState.cpp
 *
 *  Created on: 6 sty 2015
 *      Author: przem312
 */

#include <StackState/InitState.hpp>
#include <Stack.h>
#include <DeviceStatus.h>
#include <Device.h>
#include <CommPortBase.h>
#ifdef USE_OS
#include <SystemTick.hpp>
#endif
#include "StackState/MasterState.hpp"
#include "StackState/NodeState.hpp"
InitState::InitState(Stack* instance)
:IStackState(instance){
	// TODO Auto-generated constructor stub

}

InitState::~InitState() {
	// TODO Auto-generated destructor stub
}

DeviceStatus::Status InitState::Execute()
{
    m_pStack->m_CommPort->Init();
    DEBUG("Device %d successfully started with address: %s and status: %d",
          Device::GetDeviceId(),
          Device::GetDeviceIpAsString(),
          0);
	AppMngr::GetInstance()->InitializeApp();
#ifdef USE_OS
		SystemTick tick;
#endif
	MasterState masterState(m_pStack);
		SetNextState(&masterState);
	switch(Device::GetDeviceType())
	{
	case Device::MASTER:
		return m_StackState->Execute();

	case Device::END_NODE:
	{
//		NodeState nodeState(m_pStack);
//		SetNextState(&nodestate);
		return m_StackState->Execute();
	}
	}
	return DeviceStatus::GEN_FAIL;
}
