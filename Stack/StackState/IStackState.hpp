/*
 * IStackState.hpp
 *
 *  Created on: 6 sty 2015
 *      Author: przem312
 */

#ifndef ISTACKSTATE_HPP_
#define ISTACKSTATE_HPP_

#include "DeviceStatus.h"
#include <stddef.h>
#include <stdint.h>
class Stack;

class IStackState
{
public:
	IStackState(Stack* pStack)
	:m_pStack(pStack),m_StackState(NULL)
	{}

	virtual DeviceStatus::Status Execute() = 0;
	void SetNextState(IStackState* pState)
	{
		m_StackState=pState;
	}
protected:
	virtual ~IStackState(){};
	IStackState* m_StackState;
	Stack* m_pStack;
};



#endif /* ISTACKSTATE_HPP_ */
