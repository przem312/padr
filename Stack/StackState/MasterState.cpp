/*
 * MasterState.cpp
 *
 *  Created on: 10 sty 2015
 *      Author: przem312
 */

#include <StackState/MasterState.hpp>
#include <MasterCommPort.hpp>
#include "AppMngr.h"
#include "DeviceApp.h"
MasterState::MasterState(Stack* pStack) :IStackState(pStack){
	// TODO Auto-generated constructor stub

}

MasterState::~MasterState() {
	// TODO Auto-generated destructor stub
}

DeviceStatus::Status MasterState::Execute()
{
	Communication::MasterCommPort masterCommport(*m_pStack->m_CommPort);
	CREATE_APP_ARGUMENT(DeviceApp, WEBSERVER_APP, &masterCommport);
	return masterCommport.Main();
}
