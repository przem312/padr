/*
 * InitState.hpp
 *
 *  Created on: 6 sty 2015
 *      Author: przem312
 */

#ifndef INITSTATE_HPP_
#define INITSTATE_HPP_

#include <StackState/IStackState.hpp>

class Stack;

class InitState: public IStackState {
public:
	InitState(Stack* item);
	virtual ~InitState();

	virtual DeviceStatus::Status Execute();
};

#endif /* INITSTATE_HPP_ */
