/*
 * MasterState.hpp
 *
 *  Created on: 10 sty 2015
 *      Author: przem312
 */

#ifndef MASTERSTATE_HPP_
#define MASTERSTATE_HPP_

#include <StackState/IStackState.hpp>

class MasterState: public IStackState {
public:
	virtual DeviceStatus::Status Execute();
	MasterState(Stack* pStack);
	virtual ~MasterState();
};

#endif /* MASTERSTATE_HPP_ */
