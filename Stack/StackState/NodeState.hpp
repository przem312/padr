/*
 * NodeState.hpp
 *
 *  Created on: 10 sty 2015
 *      Author: przem312
 */

#ifndef NODESTATE_HPP_
#define NODESTATE_HPP_

#include <StackState/IStackState.hpp>
class NodeState: public IStackState {
public:
	NodeState(Stack* pStack)
:IStackState(pStack)
{}
	virtual DeviceStatus::Status Execute(){return DeviceStatus::GEN_OK;};
	virtual ~NodeState();
};

#endif /* NODESTATE_HPP_ */
