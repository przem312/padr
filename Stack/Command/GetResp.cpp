/*
 * GetResp.cpp
 *
 *  Created on: Aug 14, 2014
 *      Author: PSokolo
 */

#include "GetResp.h"
#include "Stack.h"
#include "AppMngr.h"
#include "../Application/AppAttributeNumbers.h"
#include "../Application/AppMngr.h"
GetResp::GetResp()
:Command(GET_RESP),
 m_AppId(0),
 m_number(0)
{
	// TODO Auto-generated constructor stub

}

GetResp::~GetResp()
{
	// TODO Auto-generated destructor stub
}
bool GetResp::PrepareCommand(CommandMsg*,char*,size_t)
{
	return false;
}

CommandMsg* GetResp::PrepareResp()
{
    char address[4];
//    std::memcpy(address,GetCmdPtr()->GetAuthorIp(),4);
    ClearMsg();

   // GetCmdPtr()->SetTargetIp(address);
	GetMsg* pMsg = (GetMsg*)GetCmdPtr();
//	pMsg->SetAuthorIp(Device::GetDeviceIp());
	pMsg->SetMessageSize();
	//pMsg->SetResponsReq(false);
	pMsg->SetTargetAttributeId(pMsg->GetSenderAttributeId());
	pMsg->SetTargetId(pMsg->GetSenderId());
	pMsg->SetCommandId(GET_RESP);
	pMsg->SetSenderAttributeId(m_number);
	pMsg->SetSenderId(m_AppId);
	AppBase* pApp = AppMngr::GetInstance()->GetApp((AppType)pMsg->GetTargetId());
	if(pApp == NULL)//TODO: ADD APP MNGR here.
	{
		return NULL;
	}
	AttributeSet set;
	set.m_AttrNo=(AttributeNo)pMsg->GetTargetAttributeId();
	set.sizeOfAttr = pMsg->GetPayloadSize();
	bool retStatus = pApp->Get(set, pMsg->GetMsgBuffer());
	if(!retStatus)
	{
		return NULL;
	}

	return pMsg;

}
DeviceStatus::Status GetResp::Execute()
{
//	GetMsg* pMsg = (GetMsg*)GetCmdPtr();
//	AppBase* pApp = Stack::GetAppMngr()->GetApp((AppMngr::AppType)pMsg->GetTargetId());
//	if(pApp == NULL)
//	{
		return DeviceStatus::GEN_BAD_DATA;
//	}
//	AttributeSet set;
//	set.m_AttrNo=(AttributeNo)pMsg->GetTargetAttributeId();
//	set.sizeOfAttr = pMsg->GetPayloadSize();
//	pApp->ProcessResponse(set,pMsg->GetMsgBuffer());
//	return DeviceStatus::GEN_OK;

}
