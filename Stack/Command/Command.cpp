/*
 * Command.cpp
 *
 *  Created on: Nov 25, 2013
 *      Author: przem312
 */

#include <cstring>
#include "Command.h"
#include "Device.h"
#include "AssertMacro.h"
#include <stddef.h>

    DeviceStatus::Status Command::ExecuteCommand(CommandMsg* pMsg)
    {
        ASSERT(pMsg != NULL);
        // TODO Mutex
        SetCmdPtr(pMsg);
        DeviceStatus::Status status = Execute();
        if(status != DeviceStatus::GEN_OK)
        {
        	return status;
        }
        else
        {
            
            	return PrepareResp() ? DeviceStatus::COMM_RESP_RDY : DeviceStatus::GEN_BAD_DATA;
            
        }
    }

    bool Command::PrepareSendCommand(CommandMsg* msgBuffer, char* pExtraContent, size_t size)
    {


        ASSERT(msgBuffer != NULL);
        msgBuffer->SetMsgType(COMMAND);
        msgBuffer->SetCommandId(GetCommandId());

		return PrepareCommand(msgBuffer,pExtraContent,size);

    }

    DeviceStatus::Status Command::Execute()
    {
        return DeviceStatus::GEN_BAD_DATA;
    }

    bool Command::PrepareCommand(CommandMsg* msgBuffer,char* pExtraContent, size_t bufferSize)
    {
	      return false;
    }

    void Command::ClearMsg()
    {
        ClearMsg(m_pCommand);
    }
    void Command::ClearMsg(CommandMsg* pMsg)
    {
//        pMsg->ClearMsg();
    }

    char* Command::GetDeviceIp()
    {
        return Device::GetDeviceIp();
    }

    uint8_t Command::GetDeviceId()
    {
        return Device::GetDeviceId();
    }

	void Command::SetMessageData(char* pData, size_t dataLen)
	{
		ASSERT(pData == NULL);
	}
