/*
 * CommandNumbers.h
 *
 *  Created on: Nov 25, 2013
 *      Author: przem312
 */

#ifndef COMMANDNUMBERS_H_
#define COMMANDNUMBERS_H_

#define RESP_OFFSET 100

    typedef enum commandId
    {
        GET_CMD,   // Get command.
        SET_CMD,   // Set command.
        SUBSCRIBE_CMD,    // Id command.
        UPDATE, 	// Update Request
        TRANSFER,
        GET_RESP = GET_CMD+RESP_OFFSET,
        SET_RESP = SET_CMD+RESP_OFFSET,
        SUBSCRIBE_RESP = SUBSCRIBE_CMD + RESP_OFFSET,
        UPDATE_RESP = UPDATE+RESP_OFFSET,
        TRANSFER_RESP = TRANSFER+RESP_OFFSET

    }CommandId;

#endif /* COMMANDNUMBERS_H_ */
