/*
 * SubscribeCommand.h
 *
 *  Created on: 12-05-2015
 *      Author: psokolo
 */

#ifndef SUBSCRIBECOMMAND_H_
#define SUBSCRIBECOMMAND_H_

#include "Command.h"

class SubscribeCommand: public Command {
public:
	SubscribeCommand();
	virtual ~SubscribeCommand();
	virtual CommandMsg* PrepareResp(){return NULL;};
};

#endif /* SUBSCRIBECOMMAND_H_ */
