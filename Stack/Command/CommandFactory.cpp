/*
 * CommandFactory.cpp
 *
 *  Created on: Apr 25, 2014
 *      Author: PSokolo
 */

#include "CommandFactory.hpp"
#include "Command.h"
#include "SetCommand.h"
#include "GetCommand.h"
#include "GetResp.h"
#include "CommandNumbers.h"
#include "CommandDispatcher.h"
#include "SubscribeCommand.h"


CommandFactory::CommandFactory()
{
	// TODO Auto-generated constructor stub

}

CommandFactory::~CommandFactory()
{
	// TODO Auto-generated destructor stub
}

void CommandFactory::CreateCommand()
{
	CreateCommand(GET_CMD);
	CreateCommand(SUBSCRIBE_CMD);
}

void CommandFactory::CreateCommand(CommandId id)
{
	Command* voidPtr;

	switch(id)
	{
	case GET_CMD:
		voidPtr = new GetCommand();
		break;

	case SUBSCRIBE_CMD:
		voidPtr = new SubscribeCommand();
		break;
		/*  case SET:
            void new SetCommand();
            break;*/
            /*    case SUBSCRIBE:
            pRetVal = &m_pSubscribeCommand;
            break;*/
	default:
		break;
	}
	if(voidPtr != NULL)
		CommandDispatcher::GetInstance().RegisterCommand(voidPtr);

}
/* namespace Command */
