/*
 * CommandFactory.hpp
 *
 *  Created on: Apr 25, 2014
 *      Author: PSokolo
 */

#ifndef COMMANDFACTORY_HPP_
#define COMMANDFACTORY_HPP_
#include "CommandNumbers.h"
    /*
     *
     */
    class CommandFactory
    {
    public:
        void CreateCommand();
        void CreateCommand(CommandId id);
        CommandFactory();
        virtual ~CommandFactory();
    };

 /* namespace Command */
#endif /* COMMANDFACTORY_HPP_ */
