/*
 * SetCommand.h
 *
 *  Created on: Dec 1, 2013
 *      Author: przem312
 */

#ifndef SETCOMMAND_H_
#define SETCOMMAND_H_

/*
 *
 */
#include "Command.h"

class SetCommand : public Command
{
public:
    SetCommand();
    virtual ~SetCommand(){};
    virtual CommandMsg* PrepareResp()
    {return NULL;}
    // Shall be overridden, this returns GEN_BAD_DATA
    virtual DeviceStatus::Status Execute();

};

#endif /* SETCOMMAND_H_ */
