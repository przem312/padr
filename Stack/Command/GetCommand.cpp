/*
 * GetCommand.cpp
 *
 *  Created on: Dec 1, 2013
 *      Author: przem312
 */

#include "GetCommand.h"
#include "Stack.h"
#include <new>
#include "CommandDispatcher.h"
#include "GetResp.h"
GetCommand::GetCommand()
:Command(GET_CMD)
{
    m_pRespCmd=new GetResp();
    CommandDispatcher::GetInstance().RegisterCommand(m_pRespCmd);
}
DeviceStatus::Status GetCommand::Execute()
{
    GetMsg* pMsg = (GetMsg*)GetCmdPtr();
    m_pRespCmd->SetCmdPtr(GetCmdPtr());
    m_pRespCmd->PostResponseData(pMsg->GetTargetId(), pMsg->GetTargetAttributeId());
    return DeviceStatus::GEN_OK;
}

bool GetCommand::PrepareCommand(CommandMsg* msgBuffer, char* pExtraContent, size_t bufferSize)
{

    GetMsg* pMsg = new (msgBuffer)GetMsg;
    pMsg->SetMessageSize();
    DEBUG("PREPARE");
    if(bufferSize>pMsg->GetMsgSize() || bufferSize==0)
    {
    	DEBUG("TOO BIG");
        return false;
    }
    pMsg->SetSenderId((uint8_t)*pExtraContent);
    pExtraContent+=sizeof(uint8_t);
    bufferSize--;
    pMsg->SetSenderAttributeId((uint8_t)*pExtraContent);
    pExtraContent+=sizeof(uint8_t);
    bufferSize--;
    pMsg->SetTargetId((uint8_t)*pExtraContent);
    pExtraContent+=sizeof(uint8_t);
    bufferSize--;
    pMsg->SetTargetAttributeId((uint8_t)*pExtraContent);
    pExtraContent+=sizeof(uint8_t);
    bufferSize--;
    pMsg->AppendToMsgContent(pExtraContent,bufferSize);
    return true;
}

/*bool GetCommand::PrepareSendCommand( CommandMsg* msgBuffer,char* pExtraContent, size_t size )
{
    if(size>GetMsg::GetPayloadSize() || size==0)
    {
        return false;
    }
    GetMsg* pMsg = (GetMsg*)msgBuffer;
    pMsg->SetTargetId((uint16_t)*pExtraContent);
    pExtraContent+=sizeof(uint16_t);
    size--;
    pMsg->SetTargetAttributeId((uint16_t)*pExtraContent);
    pExtraContent+=sizeof(uint16_t);
    size--;
    pMsg->AppendToMsgContent(pExtraContent,size);
    return true;
}*/

CommandMsg* GetCommand::PrepareResp()
{
    return m_pRespCmd->PrepareResp();
}

