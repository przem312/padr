/*
 * Command.h
 *
 *  Created on: Nov 25, 2013
 *      Author: przem312
 */

#ifndef COMMAND_H_
#define COMMAND_H_
#include <stddef.h>
#include <stdint.h>
#include "MessageBase.h"
#include "DeviceStatus.h"
#include "Types.h"
#include "LinkedList.h"
    /*
     * Class name: Command
     *
     */
   class Command :public ListNode
    {
    public:
        DeviceStatus::Status ExecuteCommand(CommandMsg* pMsg);

        /*
         * Prepares command to send
         * Can get content that will be passed to MsgBody
         */
        bool PrepareSendCommand(CommandMsg* msgBuffer, char* pExtraContent, size_t size = 0);

        CommandId GetCommandId()
        {
            return m_commandId;
        };
        virtual CommandMsg* PrepareResp()=0;

    protected:

        Command(const CommandId commandId)
        :m_commandId(commandId),
         m_pCommand(NULL)
        {

        };

        virtual ~Command()
        {

        }
        virtual bool PrepareCommand(CommandMsg* msgBuffer,char* pExtraContent, size_t bufferSize);

		virtual void SetMessageData(char* pData, size_t dataLen);
        // Shall be overridden, this returns GEN_BAD_DATA
        virtual DeviceStatus::Status Execute();

        void ClearMsg();
        void ClearMsg(CommandMsg* pMsg);

        char* GetDeviceIp();
        uint8_t GetDeviceId();

        virtual CommandMsg* GetCmdPtr()
        {
            return m_pCommand;
        };

        virtual void SetCmdPtr(CommandMsg* pMsg)
        {
            m_pCommand = pMsg;
        };
    private:

        void ResetCmdPtr()
        {
            m_pCommand = NULL;
        };

        CommandMsg* m_pCommand;
        const CommandId m_commandId;
    };

#endif /* COMMAND_H_ */
