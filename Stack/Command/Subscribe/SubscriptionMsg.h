/*
 * SubscriptionMsg.h
 *
 *  Created on: 12-05-2015
 *      Author: psokolo
 */

#ifndef SUBSCRIPTIONMSG_H_
#define SUBSCRIPTIONMSG_H_

#include "MessageBase.h"

namespace Subscription
{
typedef enum subscriptCommandType
{
	START = 1,
	STOP,
	DATA_PROVIDE
}SubscriptCommandType;

class SubscriptionMsg: public CommandMsg {
public:
	SubscriptionMsg();
	~SubscriptionMsg();

	uint16_t getCommandType() const {
		return commandType & 0xff;
	}

	void setCommandType(SubscriptCommandType commandType) {
		this->commandType = static_cast<uint16_t>(commandType);
	}

private:
	uint16_t commandType;
};

class SubscriptionStartMsg : public SubscriptionMsg
{

};

class SubscriptionStopMsg : public SubscriptionMsg
{

};


class SubscriptionDataMsg : public SubscriptionMsg
{

};


}
#endif /* SUBSCRIPTIONMSG_H_ */
