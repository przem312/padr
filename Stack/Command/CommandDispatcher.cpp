/*
 * CommandDispatcher.cpp
 *
 *  Created on: Dec 5, 2013
 *      Author: przem312
 */

#include <CommPortBase.h>
#include "CommandDispatcher.h"
#include "DeviceStatus.h"
#include "CommandFactory.hpp"
#include "MsgSrvPool.hpp"
#include "MessageBase.h"
    CommandDispatcher& CommandDispatcher::GetInstance()
    {
        static CommandDispatcher theInstance;
        return theInstance;
    }

    void CommandDispatcher::Init(Communication::CommunicationBase* pBase, Communication::MsgSrvPool& pPool)
    {
    	pBase->RegisterUser(CommandDispatcher::Receive, COMMAND);
    	CommandFactory factory;
    	factory.CreateCommand();
    	m_pPool=&pPool;
    }

    DeviceStatus::Status CommandDispatcher::ReceiveCommand(CommandMsg* pMsg)
    {

        const CommandId CommandToDispatch = pMsg->GetCommandId();
        Command* pCommand = GetCommandById(CommandToDispatch);
        if(pCommand == NULL)
        {
          return DeviceStatus::GEN_FAIL;
        }
        return pCommand->ExecuteCommand(pMsg);

    }

    DeviceStatus::Status CommandDispatcher::SendCommand(const CommandId id,
            						 DeviceStatus& rStatus,
            						 TargetAddressContainer* pContainer,
            						 char* pAdditionalData,
            						 size_t dataSize,
            						 ResponseRcvCallback pCallback)
    {
		// TODO prepare other buffer...
    	Communication::MsgSrv service;
    	if(dataSize +sizeof(CommandMsg)>= service.GetBufferSize())
    	{
//    		m_pPool->Release(service);
    		rStatus.SetStatus(DeviceStatus::GEN_BAD_DATA);
    		return DeviceStatus::GEN_BAD_DATA;
    	}

        CommandMsg* msgBuffer = (CommandMsg*)service.GetBuffer();

        if( GetCommandById(id)->PrepareSendCommand(msgBuffer, pAdditionalData, dataSize))
		{
        	DEBUG("PrepareSendCommand");
			rStatus.SetStatus(service.Send(pContainer,msgBuffer));
		   // DEBUG("Sent command with ID: %d on address: %s. Additional data size %d",(int)id,address.GetAddressAsString() ,dataSize);
		   // rStatus.SetStatus(SendCommand(msgBuffer, pSendSrv));
		}
    	if(rStatus.GetStatus() == DeviceStatus::COMM_MSG_PENDING)
    	{
    		if(pCallback != NULL)
    		{
    		pCallback((void*)msgBuffer->getPayload());
    		}
    		else
    		{
    			std::memcpy(pAdditionalData,msgBuffer->getPayload(),dataSize);
    		}
    	}
//		m_pPool->Release(service);
        return rStatus.GetStatus();

    }


    DeviceStatus::Status CommandDispatcher::SendCommand(CommandMsg* pMsg,
            Communication::MsgSrv* pSendSrv)
    {

		return DeviceStatus::GEN_BAD_DATA;
    }

    Command* CommandDispatcher::GetCommandRespById(const CommandId id)
    {
    	if(id>100)
    		return NULL;
    	const CommandId respId = (CommandId)(id+100);
    	return GetCommandById(respId);
    }

    Command* CommandDispatcher::GetCommandById(const CommandId id)
    {
    	Command* pCommand=(Command*)(m_commandList.GetFirst());
        while(pCommand!=NULL)
        {
            if(pCommand->GetCommandId() == id)
            {
                break;
            }
            pCommand=(Command*)(m_commandList.GetNext());
        }

        return pCommand;
    }

    void CommandDispatcher::RegisterCommand(Command* pCommand)
    {
        m_commandList.AddNode(pCommand);
    }

    CommandDispatcher::CommandDispatcher()
    {
       // factory.CreateCommand();
    }

    CommandDispatcher::~CommandDispatcher()
    {
        m_commandList.CleanList();
    }
