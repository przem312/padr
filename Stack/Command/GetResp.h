/*
 * GetResp.h
 *
 *  Created on: Aug 14, 2014
 *      Author: PSokolo
 */

#ifndef GETRESP_H_
#define GETRESP_H_

#include "Command.h"
/*
 *	FIXME: GET OTHER WAY OF PASSING DATA;
 */
class GetResp :public Command
{
	friend class GetCommand;
public:
    GetResp();
    virtual ~GetResp();
    virtual CommandMsg* PrepareResp();

    virtual bool PrepareCommand(CommandMsg* msgBuffer, char* pExtraContent, size_t bufferSize);
    virtual DeviceStatus::Status Execute();
    void PostResponseData(uint8_t AppId, uint8_t number)
    {
    	m_number=number;
    	m_AppId=AppId;
    };
private:
    uint8_t m_number;
    uint8_t m_AppId;
};

#endif /* GETRESP_H_ */
