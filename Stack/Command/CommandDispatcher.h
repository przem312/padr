/*
 * CommandDispatcher.h
 *
 *  Created on: Dec 5, 2013
 *      Author: przem312
 */

#ifndef COMMANDDISPATCHER_H_
#define COMMANDDISPATCHER_H_

#include "Command.h"
#include "CommandNumbers.h"
#include "LinkedList.h"
#include "../Utils/ObjectPool.h"
    /*
     *  Entry point for all commands.
     *  All commands requests shall be brought by using this object.
     *  TODO Synchronization mechanisms
     */
class TargetAddressContainer;
namespace Communication
{
    class MsgSrv;
    class MsgSrvPool;
    class CommunicationBase;

}

    class CommandDispatcher
    {
    public:
    	typedef void (ResponseRcvCallback)(void* pBody);
        static CommandDispatcher& GetInstance();
        static DeviceStatus::Status Receive(MessageBody* pBody)
        {
        	return GetInstance().ReceiveCommand((CommandMsg*)pBody);
        }
        // Returns pointer to address of sender device if response is required,
        // if not, returns NULL
        // TODO: PARAMETRIZE THIS
        void Init(Communication::CommunicationBase* pBase,
        			Communication::MsgSrvPool& pPool);
        DeviceStatus::Status ReceiveCommand(CommandMsg* pMsg);

        DeviceStatus::Status SendCommand(const CommandId id,
                						 DeviceStatus& rStatus,
                						 TargetAddressContainer* pContainer,
                						 char* pAdditionalData,
                						 size_t dataSize,
                						 ResponseRcvCallback pCallback = NULL);

        void RegisterCommand(Command* pCommand);
        Command* GetCommandRespById(const CommandId id);
        Command* GetCommandById(CommandId id);
    private:
        DeviceStatus::Status SendCommand( CommandMsg* pMsg,
                					      Communication::MsgSrv* pSendSrv);
        CommandDispatcher();

        /*
         *TODO Add linked list of commands
         *TODO void AddCommand(CommandId newCommand);
         */
        virtual ~CommandDispatcher();
        LinkedList m_commandList;
        Communication::MsgSrvPool* m_pPool;
        //TODO future
        //  static SubcribeCommand m_pSubscribeCommand;
    };

#endif /* COMMANDDISPATCHER_H_ */
