/*
 * GetCommand.h
 *
 *  Created on: Dec 1, 2013
 *      Author: przem312
 */

#ifndef GETCOMMAND_H_
#define GETCOMMAND_H_

/*
 *
 */
#include "Command.h"
#include "AppAttributeNumbers.h"
#include "GetResp.h"
    class GetCommand : public Command
    {
    public:

    	virtual bool PrepareCommand(CommandMsg* msgBuffer, char* pExtraContent, size_t bufferSize);

        GetCommand();
        virtual ~GetCommand()
        {
            delete m_pRespCmd;
        };

        virtual CommandMsg* PrepareResp();
		        // Shall be overridden, this returns GEN_BAD_DATA
        virtual DeviceStatus::Status Execute();
    private:
        GetResp* m_pRespCmd;

    };
#endif /* GETCOMMAND_H_ */
